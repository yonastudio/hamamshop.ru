<?php

/**
 * Description of GPU_Class_ImageFilter
 *
 * @author makedon
 */
class My_Image_Filter
{

    private $_error = 0;
    private $src = '';
    private $width = 0;
    private $height = 0;
    public $type = 'gallery';

    public function __construct($type = null)
    {
        if ($type) {
            $this->type = $type;
        }
    }

    public function process($id, $imageId, $params)
    {
        if (!$id) {
            $this->_error = 1;
        }

        if (!$imageId) {
            $imageId = 0;
        }

        $src = $this->ImageFilepath($id, $imageId, $params);

        if (!$src) {
            $this->_error = 2;
        }

        if (file_exists(DOCUMENT_ROOT . $src)) {
            $this->src = $src;
            $this->width = $params['width'];
        } else {
            $original = $this->ImageFilepathOriginal($id, $imageId);

            if ($params['width'] && file_exists(DOCUMENT_ROOT . $original)) {

                $paramsNewImage = $this->CreateImage($original, $src, $params);

                $this->src = $src;
                $this->width = $params['width']; //$paramsNewImage['width'];
            } else {
                $this->_error = 3;
            }
        }
    }

    public function getErrorText($codeError)
    {
        switch ($codeError) {
            case 0:
                $textError = '';
                break;
            case 1:
                $textError = 'not found Id';
                break;
            case 2:
                $textError = 'not filepath - error ImageFilepath';
                break;
            case 3:
                $textError = 'not found original image';
                break;
            case 4:
                $textError = 'no correct format image';
                break;
            case 5:
                $textError = 'no create image - is lock';
                break;
            case 6:
                $textError = 'error image not type JPEG';
                break;
            case 7:
                $textError = 'error reserv';
                break;

            default:
                $textError = 'no undefined error';
                break;
        }

        return $textError;
    }

    public function getSrc()
    {
        return $this->src;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function Error()
    {
        return $this->_error;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function ImageFilepath($id, $imageId, $params)
    {
        if ($this->Error()) {
            return '';
        }

        $isOriginal = !$params['width'];

        $ImageFolder = $this->ImageFolder($id, $imageId, $isOriginal);

        $ImageFilename = $this->ImageFilename($id, $imageId, $params['width']);

        return $ImageFolder . $ImageFilename;
    }

    public function ImageFilepathOriginal($id, $imageId)
    {
        return $this->ImageFilepath($id, $imageId, array());
    }

    public function ShowImageHtmlView($id, $imageId, $params)
    {
        $this->process($id, $imageId, $params);

        if ($this->Error()) {
            return '';
        }

        if ($params['hash']) {
            $hash = '?' . time();
        }

        //$result = '<img src="' . $this->getSrc() . $hash . '" width="' . $this->getWidth() . '"';
        $result = '<img src="' . $this->getSrc() . $hash . '"';

        if ($params['height'] > 0) {
            $result .= ' height="' . $this->getHeight() . '"';
        }

        if ($params['height_html'] > 0) {
            $result .= ' height="' . $params['height_html'] . '"';
        }

        $alt = ($params['alt']) ? $params['alt'] : '';

        $title = ($params['title']) ? $params['title'] : $alt;

        $class = ($params['class']) ? $params['class'] : '';

        $param_id = ($params['id']) ? $params['id'] : '';

        $result .= ' alt="' . $alt . '"';

        if ($title) {
            $result .= ' title="' . $title . '"';
        }

        if ($class) {
            $result .= ' class="' . $class . '"';
        }

        if ($param_id) {
            $result .= ' id="' . $param_id . '"';
        }

        if ($params['style']) {
            $result .= ' style="' . $params['style'] . '"';
        }

        $result .= '/>';

        return $result;
    }

    public function countImagesInternalFromId($id)
    {
        $ImageFolderOriginal = $this->ImageFolder($id, 1, true);

        $dir = DOCUMENT_ROOT . $ImageFolderOriginal;

        $countImages = 0;

        if (is_dir($dir)) {
            $dh = opendir($dir);
            if ($dh) {
                while (($file = readdir($dh)) !== false) {
                    if (preg_match('|' . $id . '|', $file)) {
                        $countImages++;
                    }
                }
                closedir($dh);
            }
        }

        return $countImages;
    }

    public function ClearImagePreview($id, $imageId, $SpecifyPattern = false)
    {
        $ImageFolder = $this->ImageFolder($id, $imageId, false); //true

        $dir = DOCUMENT_ROOT . $ImageFolder;

        if ($imageId == 0 || $SpecifyPattern) {
            $pattern = '|' . $id . '|';
        } else {
            $pattern = '|' . $id . '_' . $imageId . '\.' . '|';
        }

        $this->DeleteFileInDirectoryPattern($dir, $pattern);
    }

    /**
     * семафор для блокировки оригинала картинки
     * @param <type> $original
     * @return <type>
     */
    private function SetLock($original)
    {

        $lockFile = preg_replace('/\.jpg/i', '_lock.jpg', $original);

        $filenameLock = DOCUMENT_ROOT . $lockFile;

        if (file_exists($filenameLock)) {
            return false;
        } else {
            $handle = fopen($filenameLock, 'w+');
            if (flock($handle, LOCK_EX)) {
                fwrite($handle, '1');
                flock($handle, LOCK_UN);
                fclose($handle);
                return true;
            } else {
                fclose($handle);
                return false;
            }
        }
    }

    private function ClearLock($original)
    {
        $lockFile = preg_replace('/\.jpg/i', '_lock.jpg', $original);
        unlink(DOCUMENT_ROOT . $lockFile);
    }

    private function CreateImage($original, $newImage, $params)
    {
        if ($this->Error()) {
            return '';
        }

        $serverPath = DOCUMENT_ROOT;

        if ($this->SetLock($original)) {

            $format = $this->SelectFormat($original, $params);

            if (!count($format)) {
                $this->_error = 4;
                return array();
            }

            $im = imagecreatefromjpeg($serverPath . $original);

            imagejpeg($im, $serverPath . $newImage, JPEG_QUALITY_PREVIEW);
            $src_img = imagecreatefromjpeg($serverPath . $newImage);
            $dst_img = imagecreatetruecolor($format['newWidth'], $format['newHeight']);
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $format['newWidth'], $format['newHeight'], imagesx($src_img), imagesy($src_img));
            imagejpeg($dst_img, $serverPath . $newImage, 100); //CHANGED BY TOS
            imagedestroy($src_img);
            imagedestroy($dst_img);

            $this->ClearLock($original);

            $output = array(
                'width' => $params['width'],
                'newWidth' => $format['newWidth'],
                'height' => $params['height'],
                'newHeight' => $format['newHeight']
            );

            return $output;
        } else {
            $this->_error = 5;
            return array();
        }
    }

    private function SelectFormat($original, $params)
    {
        $size = getimagesize($_SERVER['DOCUMENT_ROOT'] . $original);

        $imtype = $size[2];

        if ($imtype != 2) {
            $this->_error = 6;
            return;
        }

        $width = $size[0];
        $height = $size[1];
        $maxWidth = $params['width'];
        $maxHeight = ($params['maxHeight']) ? $params['maxHeight'] : 10000;

        $koef = $width / $height;

        if ($koef > 1) {

            if ($width > $maxWidth) {
                $newWidth = $maxWidth;
            } else {
                $newWidth = $width;
            }
        } else {

            if ($height > $maxHeight) {
                $newHeight = $maxHeight;
            } else {
                $newHeight = $height;
            }

            $newWidth = ceil($newHeight * $koef);

            if ($newWidth > $maxWidth) {
                $newWidth = $maxWidth;
            }
        }

        $newHeight = ceil($newWidth / $koef);

        $format = array('newWidth' => $newWidth, 'newHeight' => $newHeight);

        return $format;
    }

    private function ImageFolder($id, $imageId, $isOriginal)
    {

        assert($imageId >= 0);
        assert($id >= 0);

        $isPreview = (boolean) $imageId == 0;

        switch ($this->type) {
            case 'gallery' :
                if ($isOriginal) {
                    $folderExternal = IMG_GALLERY_ORIGINAL;
                } else {
                    $folderExternal = IMG_GALLERY_CACHE;
                }
                break;
            case 'news' :
                if ($isOriginal) {
                    $folderExternal = IMG_NEWS_ORIGINAL;
                } else {
                    $folderExternal = IMG_NEWS_CACHE;
                }
                break;
            case 'articles' :
                if ($isOriginal) {
                    $folderExternal = IMG_ARTICLES_ORIGINAL;
                } else {
                    $folderExternal = IMG_ARTICLES_CACHE;
                }
                break;
            case 'catalog' :
                if ($isOriginal) {
                    $folderExternal = IMG_CATALOG_ORIGINAL;
                } else {
                    $folderExternal = IMG_CATALOG_CACHE;
                }
                break;
            case 'shop_brand' :
                if ($isOriginal) {
                    $folderExternal = '/img/original/shop_brand/';
                } else {
                    $folderExternal = '/img/shop_brand/';
                }
                break;
            case 'shop_model' :
                if ($isOriginal) {
                    $folderExternal = '/img/original/shop_model/';
                } else {
                    $folderExternal = '/img/shop_model/';
                }
                break;
            case 'shop_model_color' :
                if ($isOriginal) {
                    $folderExternal = '/img/original/shop_model_color/';
                } else {
                    $folderExternal = '/img/shop_model_color/';
                }
                break;
            default : return;
        }

        $folderInternal = floor($id / 1000);

        $folder = $folderExternal . $folderInternal . '/';

        if (!is_dir(DOCUMENT_ROOT . $folder)) {

            mkdir(DOCUMENT_ROOT . $folder);
        }

        return $folder;
    }

    private function ImageFilename($id, $imageId, $width)
    {

        assert($imageId >= 0);
        $width = (int) $width;

        $suffixImagename = ($imageId == 0) ? '' : '_' . $imageId;

        if ($width) {
            $imageFilename = $id . $suffixImagename . '_' . $width . '.jpg';
        } else {
            $imageFilename = $id . $suffixImagename . '.jpg';
        }

        return $imageFilename;
    }

    private function DeleteFileInDirectoryPattern($dir, $pattern)
    {
        if (is_dir($dir)) {
            $dh = opendir($dir);
            if ($dh) {
                while (($file = readdir($dh)) !== false) {
                    if (preg_match($pattern, $file)) {
                        unlink($dir . $file);
                    }
                }
                closedir($dh);
            }
        }
    }

}
