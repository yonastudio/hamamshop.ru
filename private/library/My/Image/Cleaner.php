<?php

/**
 * Description of Cleaner
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class My_Image_Cleaner
{

    public function clearCached($id, $type = 'gallery')
    {
        $path = floor($id / 1000);
        $searchPath = "img/" . $type . "/" . $path . "/" . $id . "_*.jpg";
        $files = glob($searchPath);
        if (count($files) > 0) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }                
            }
        }
    }

}
