<?php

/**
 * Description of Uploader
 *
 * @author веб-студия "WeZoom" <web@wezoom.net> http://wezoom.net
 * @author ведущий разрабочик Торош Александр <webtorua@gmail.com>
 */

include_once PATH_LIBRARY . '/PHPthumb/ThumbLib.inc.php';

class My_Image_Uploader
{

    public function galleryImageUpload($item_id)
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        $upload->addValidator('ImageSize', false,
                array('minwidth' => IMAGE_MIN_WIDTH,
                    'maxwidth' => IMAGE_MAX_WIDTH,
                    'minheight' => IMAGE_MIN_HEIGHT,
                    'maxheight' => IMAGE_MAX_HEIGHT)
        );
        $upload->addValidator('Size', false, 10000000);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5($item_id . time()) . '.jpg';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            return false;
        }

        list($img_width, $img_height, $type, $attr) = getimagesize(TEMP_DIR . $image);

        $imageFilter = new My_Image_Filter('gallery');

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }

        return array(
            'filename' => $newImage,
            'width' => $img_width,
            'height' => $img_height
        );
    }
    
    public function newsImgThumbUpload($item_id)
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        $upload->addValidator('ImageSize', false,
                array('minwidth' => IMAGE_MIN_WIDTH,
                    'maxwidth' => IMAGE_MAX_WIDTH,
                    'minheight' => IMAGE_MIN_HEIGHT,
                    'maxheight' => IMAGE_MAX_HEIGHT)
        );
        $upload->addValidator('Size', false, 10000000);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5($item_id . time()) . '.jpg';
 
        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            //var_dump($upload->getMessages());exit;
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            return false;
        }

        list($img_width, $img_height, $type, $attr) = getimagesize(TEMP_DIR . $image);

        $imageFilter = new My_Image_Filter('news');

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }
        
        $table = Doctrine_Core::getTable('Default_Model_News')->find($item_id);
        $table->img_thumb = true;
        $table->save();

        return;
    }
    
    public function articleImgThumbUpload($item_id)
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        
        $upload->addValidator('ImageSize', false,
                array('minwidth' => IMAGE_MIN_WIDTH,
                    'maxwidth' => IMAGE_MAX_WIDTH,
                    'minheight' => IMAGE_MIN_HEIGHT,
                    'maxheight' => IMAGE_MAX_HEIGHT)
        );
        $upload->addValidator('Size', false, 10000000);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5($item_id . time()) . '.jpg';
 
        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            //var_dump($upload->getMessages());exit;
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            return false;
        }

        list($img_width, $img_height, $type, $attr) = getimagesize(TEMP_DIR . $image);

        $imageFilter = new My_Image_Filter('articles');

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }
        
        $table = Doctrine_Core::getTable('Default_Model_Articles')->find($item_id);
        $table->img_thumb = true;
        $table->save();

        return;
    }        
    
}