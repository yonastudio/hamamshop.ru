<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Slug
 *
 * @author Александр
 */
class My_Translate_Slug
{

    public static function slugify($text)
    {
        $string = My_Translate_Transliteration::get($text);
        return Doctrine_Inflector::urlize(strtolower($string));
    }

}

?>
