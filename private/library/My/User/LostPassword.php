<?php
 /**
 * Description of LostPassword
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_User_LostPassword
{

    /**
     *
     * @param array $user
     * @return string
     */
    public function getHash($data)
    {
        $hash = md5(sha1(substr($data['email'],1,8)) . $data['password_salt'] . SECRET . date("Ymd"));
        return $hash;
    }

    /**
     * Генерация нового пароля
     * @param Default_Model_User $user
     * @return string
     */
    public function renewPassword($data)
    {
        $newpassword = substr(base64_encode(md5(microtime() . $data['password_salt'] . SECRET)),0,8);

        $user = Doctrine_Core::getTable('Default_Model_User')->find($data['id']);
        $user->setPassword($newpassword);
        $user->save();

        return $newpassword;
    }

}
