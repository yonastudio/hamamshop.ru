<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rubric
 *
 * @author tos
 */
class My_Category_Category
{

    public $breadcrumbs = null;
    private $_IdByName = null;
    private $_LevelById = null;
    private $_NameById = null;
    private $_TitleTrById_ru = null;
    private $_TitleTrById_uk = null;
    private $_TitleTrById_en = null;
    private $_HasChildren = null;

    private $_handler;
    private static $instance = null;

    public function __construct()
    {
        $this->_handler = new My_Error_Handler();
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new My_Category_Category();
        }
        return self::$instance;
    }

    public function pushBreadcrumbs($val)
    {
        $this->breadcrumbs[] = $val;
    }

    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    public function getCategoryIdByName($name)
    {
        if (!empty($this->_IdByName)) {
            return $this->_IdByName[$name];
        } else {
            $this->_IdByName = $this->cacheIdByName();
            return $this->_IdByName[$name];
        }
    }

    public function getCategoryLevelById($id)
    {
        if (!empty($this->_LevelById)) {
            return $this->_LevelById[$id];
        } else {
            $this->_LevelById = $this->cacheLevelById();
            return $this->_LevelById[$id];
        }
    }

    public function getCategoryNameById($id)
    {
        if (!empty($this->_NameById)) {
            return $this->_NameById[$id];
        } else {
            $this->_NameById = $this->cacheNameById();
            return $this->_NameById[$id];
        }
    }

    public function getCategoryTitleTrById($id, $lang = 'ru')
    {
        if (!empty($this->{_TitleTrById . '_' . $lang})) {
            return $this->{_TitleTrById . '_' . $lang}[$id];
        } else {
            $this->{_TitleTrById . '_' . $lang} = $this->cacheTitleTrById($lang);
            return $this->{_TitleTrById . '_' . $lang}[$id];
        }
    }

    public function hasCategoryChildren($id)
    {
        if (!empty($this->_HasChildren)) {
            return $this->_HasChildren[$id];
        } else {
            $this->_HasChildren = $this->cacheHasChildren();
            return $this->_HasChildren[$id];
        }
    }

    public function getFullData($id)
    {
        $prefix = Zend_Registry::get('prefix');
        $q = Doctrine_Query::create()
                ->select('c.*,'
						 . ' cd.meta_title' . $prefix . ' AS meta_title,'
                         . ' cd.description' . $prefix . ' AS description,'
                         . ' cd.keywords' . $prefix . ' AS keywords,'
                         . ' cd.text' . $prefix . ' AS text')
                ->from('Default_Model_Category c')
                ->leftJoin('c.CategoryDescription cd')
                ->where('c.id = ?', $id);
        $row = $q->fetchOne();
        if ($row) {
            return $row->toArray();
        }
    }

    public function save($data, $id = null)
    {
        if ($data['name']) {
            $name = trim($data['name']);
        } else {
            $name = My_Translate_Slug::slugify(trim($data['title']));
        }

        if (!$id) {

            if ($data['parent']) {

                if (Doctrine_Core::getTable('Default_Model_Category')->findOneByName($name)) {
                    echo "Рубрика с таким именем уже существует. Укажите уникальное имя";
                    return false;
                }

                $child = new Default_Model_Category();
                $child->name = $name;
                $child->title = trim($data['title']);
                $child->title_uk = trim($data['title_uk']);
                $child->title_en = trim($data['title_en']);
                $child->hidden = 0;
                $child->sortorder = $data['sortorder'];

                $category = Doctrine_Core::getTable('Default_Model_Category')->findOneByName($data['parent']);
                $child->getNode()->insertAsLastChildOf($category);

                $cat = Doctrine_Core::getTable('Default_Model_Category')->findOneByName($name);
                $cat->sortorder = $cat['id'];
                $cat->save();

                $description = new Default_Model_CategoryDescription();
                $description->category_id = $cat['id'];
                $description->description = $data['description'];
                $description->description = $data['description_uk'];
                $description->description = $data['description_en'];
                $description->keywords = $data['keywords'];
                $description->keywords = $data['keywords_uk'];
                $description->keywords = $data['keywords_en'];
                $description->text = $data['text'];
                $description->text = $data['text_uk'];
                $description->text = $data['text_en'];
				$description->meta_title = $data['meta_title'];
                $description->meta_title_en = $data['meta_title_en'];
                $description->save();

                return $cat['id'];
            }
        } else {

            $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
            $other = Doctrine_Core::getTable('Default_Model_Category')->findOneByName($name);

            if ($other) {
                if ($id != $other['id']) {
                    echo "Рубрика с таким именем уже существует. Укажите уникальное имя";
                    return false;
                }
            }

            $category->name = $name;
            $category->title = trim($data['title']);
            $category->title_uk = trim($data['title_uk']);
            $category->title_en = trim($data['title_en']);
            $category->hidden = $data['hidden'];
            ;
            $category->sortorder = $data['sortorder'];
            $category->save();

            $description = Doctrine_Core::getTable('Default_Model_CategoryDescription')->find($id);
            if (!$description) {
                $description = new Default_Model_CategoryDescription();
            }
            $description->category_id = $id;
            $description->description = $data['description'];
            $description->description_uk = $data['description_uk'];
            $description->description_en = $data['description_en'];
            $description->keywords = $data['keywords'];
            $description->keywords_uk = $data['keywords_uk'];
            $description->keywords_en = $data['keywords_en'];
            $description->text = $data['text'];
            $description->text_uk = $data['text_uk'];
            $description->text_en = $data['text_en'];
			$description->meta_title = $data['meta_title'];
            $description->meta_title_en = $data['meta_title_en'];
            $description->save();

            return true;
        }
    }

    public function populate($id)
    {
        $q = Doctrine_Query::create()
                ->select('c.*,'
                         . 'cc.description AS description,'
                         . 'cc.description_uk AS description_uk,'
                         . 'cc.description_en AS description_en,'
                         . 'cc.keywords AS keywords,'
                         . 'cc.keywords_uk AS keywords_uk,'
                         . 'cc.keywords_en AS keywords_en,'
                         . 'cc.text AS text,'
                         . 'cc.text_uk AS text_uk,'
                         . 'cc.text_en AS text_en,'
  						 . 'cc.meta_title AS meta_title,'
                         . 'cc.meta_title_en AS meta_title_en,'
        )
                ->from('Default_Model_Category c')
                ->leftJoin('c.CategoryDescription cc')
                ->where('c.id = ?', $id);
        $rows = $q->fetchArray();
        return $rows[0];
    }

    public function getCategoryMainArray($name)
    {
        $rubricId = $this->getCategoryIdByName($name);
        if (empty($rubricId)) {
            $this->_handler->ThrowException('404');
        }
        $findRubric = Doctrine_Core::getTable('Default_Model_Category')->find($rubricId);
        if (!$findRubric) {
            $this->_handler->ThrowException('404');
        }
        $nodesRubric = $findRubric->getNode();
        $descendants = $nodesRubric->getDescendants();
        $array = array();
        foreach ($descendants as $node) {
            $array[$node['id']] = str_repeat(' -- ', $node['level'] - 1) . $node['title'];
        }
        return $array;
    }

    public function getCategoryParentById($category_id)
    {
        $findRubric = Doctrine_Core::getTable('Default_Model_Category')->find($category_id);
        if (!$findRubric) {
            $this->_handler->ThrowException('404');
        }
        $nodesRubric = $findRubric->getNode();
        if ($nodesRubric->hasParent()) {
            $parent = $nodesRubric->getParent();
            return $parent;
        } else {
            return false;
        }
    }

    public function getCategoryParentByName($rubric_name)
    {
        $findRubric = Doctrine_Core::getTable('Default_Model_Category')->findOneByName($rubric_name);
        if (!$findRubric) {
            $this->_handler->ThrowException('404');
        }
        $nodesRubric = $findRubric->getNode();
        if ($nodesRubric->hasParent()) {
            $parent = $nodesRubric->getParent();
            return $parent;
        } else {
            return false;
        }
    }

    public function getCategoryChildrenById($rubricId)
    {
        $findRubric = Doctrine_Core::getTable('Default_Model_Category')->find($rubricId);
        if (!$findRubric) {
            $this->_handler->ThrowException('404');
        }
        $nodesRubric = $findRubric->getNode();
        if ($nodesRubric->hasChildren()) {
            $children = $nodesRubric->getChildren();
            return $children;
        } else {
            return false;
        }
    }

    public function getCategoryListByRootIds($rootId, $level = null, $without_root = null)
    {
        if (is_array($rootId)) {
            $rootIdIn = '';
            foreach ($rootId as $el) {
                $rootIdIn .= $el . ',';
            }
            $rootIdIn = substr($rootIdIn, 0, -1);
        } else {
            $rootIdIn = $rootId;
        }
        $q = Doctrine_Query::create()
                ->from('Default_Model_Category')
                ->where('lft >= 1')
                ->andWhere('root_id IN (' . $rootIdIn . ')')
                ->orderBy('root_id ASC, lft ASC');
        if ($level) {
            $q->andWhere('level = ?', $level);
        }
        if ($without_root) {
            $q->andWhere('id <> ?', $rootId);
        }
        $rows = $q->fetchArray();
        return $rows;
    }

    public function processChildren($id)
    {
        $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
        if (!$category) {
            return false;
        }
        if ($category->getNode()->hasChildren()) {
            $q = Doctrine_Query::create()
                    ->from("Default_Model_Category c")
                    ->where("(c.lft > ? AND c.rgt < ?)", array($category['lft'], $category['rgt']))
                    ->andWhere("c.level = ?", $category['level'] + 1)
                    ->andWhere("c.root_id = ?", $category['root_id'])
                    ->orderBy("c.sortorder ASC");
            $children = $q->fetchArray();
            return $children;
        } else {
            return false;
        }
    }

    public function processBreadcrumbs($id)
    {
        $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
        if (!$category) {
            return false;
        }
        if ($category->getNode()->hasParent()) {
            $parent = $category->getNode()->getParent();
            $row = $parent->toArray();
            $this->pushBreadcrumbs($row);
            $result = $this->processBreadcrumbs($row['id']);
            if (!$result) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function childrenIdsArray($id)
    {
        $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
        $q = Doctrine_Query::create()
                ->select('id')
                ->from("Default_Model_Category c")
                ->where("(c.lft > ? AND c.rgt < ?)", array($category['lft'], $category['rgt']))
                ->andWhere("c.level = ?", $category['level'] + 1)
                ->andWhere("c.root_id = ?", $category['root_id'])
                ->orderBy("c.sortorder ASC");
        $children = $q->fetchArray();
        $children_ids = array();
        foreach ($children as $el) {
            $children_ids[] = $el['id'];
        }
        return $children_ids;
    }

    public function getCategoryFullListArrayForFormSelect()
    {
        $treeObject = Doctrine_Core::getTable('Default_Model_Category')->getTree();
        $rootColumnName = $treeObject->getAttribute('rootColumnName');

        $arr = array('' => '');


        foreach ($treeObject->fetchRoots() as $root) {
            $options = array(
                'root_id' => $root->$rootColumnName
            );
            foreach ($treeObject->fetchTree($options) as $node) {
                $arr[$node['name']] = str_repeat('--', $node['level']) . $node['title'];
            }
        }

        return $arr;
    }

    public function noRootCategoriesArray($root_id = null)
    {
        $q = Doctrine_Query::create()
                ->from('Default_Model_Category')
                ->where('lft >= 1')
                ->andWhere('root_id = ?', $root_id)
                ->andWhere('level >= 1')
                ->orderBy('root_id ASC, lft ASC');
        $rows = $q->fetchArray();

        $arr = array('0' => '');
        foreach ($rows as $el) {
            $arr[$el['id']] = str_repeat('--', $el['level'] - 1) . $el['title'];
        }

        return $arr;
    }

    /**
     * Очистки кеша
     */
    public function cleanCached()
    {
        $this->_removeFile('haschildren');
        $this->_removeFile('id_title_ru');
        $this->_removeFile('id_title_uk');
        $this->_removeFile('id_title_en');
        $this->_removeFile('id_name');
        $this->_removeFile('id_level');
        $this->_removeFile('name_id');
    }

    private function _removeFile($file)
    {
        if (is_file(APPLICATION_PATH . '/configs/category/' . $file . '.php')) {
            unlink(APPLICATION_PATH . '/configs/category/' . $file . '.php');
        }
    }


    /**
     * создает и кеширует в файл массив для опеределения id категории по name
     * Если файл есть - не может его перезаписать.
     * Нужен метод, что удаляет файл.
     * @return string
     */
    private function cacheIdByName()
    {
        $configPath = APPLICATION_PATH . '/configs/category/name_id.php';
        if (file_exists($configPath) && is_array($configArray = include($configPath))) {
            return $configArray;
        } else {
            $q = Doctrine_Query::create()->from('Default_Model_Category c')->execute();
            $configArray = array();
            foreach ($q as $node) {
                $configArray[$node->name] = $node->id;
            }
            $writer = new Zend_Config_Writer_Array();
            $writer->write($configPath, new Zend_Config($configArray));
            return $configArray;
        }
    }

    private function cacheLevelById()
    {
        $configPath = APPLICATION_PATH . '/configs/category/id_level.php';
        if (file_exists($configPath) && is_array($configArray = include($configPath))) {
            return $configArray;
        } else {
            $q = Doctrine_Query::create()->from('Default_Model_Category c')->execute();
            $configArray = array();
            foreach ($q as $node) {
                $configArray[$node->id] = $node->level;
            }
            $writer = new Zend_Config_Writer_Array();
            $writer->write($configPath, new Zend_Config($configArray));
            return $configArray;
        }
    }

    private function cacheNameById()
    {
        $configPath = APPLICATION_PATH . '/configs/category/id_name.php';
        if (file_exists($configPath) && is_array($configArray = include($configPath))) {
            return $configArray;
        } else {
            $q = Doctrine_Query::create()->from('Default_Model_Category c')->execute();
            $configArray = array();
            foreach ($q as $node) {
                $configArray[$node->id] = $node->name;
            }
            $writer = new Zend_Config_Writer_Array();
            $writer->write($configPath, new Zend_Config($configArray));
            return $configArray;
        }
    }

    private function cacheTitleTrById($lang)
    {
        $configPath = APPLICATION_PATH . '/configs/category/id_title_' . $lang . '.php';
        if (file_exists($configPath) && is_array($configArray = include($configPath))) {
            return $configArray;
        } else {
            $q = Doctrine_Query::create()->from('Default_Model_Category')->execute();
            $configArray = array();
            switch ($lang) {
                case 'ru' :
                    $prefix = '';
                    break;
                case 'uk' :
                    $prefix = '_uk';
                    break;
                case 'en' :
                    $prefix = '_en';
                    break;
            }
            foreach ($q as $node) {
                $configArray[$node->id] = $node->{title . $prefix};
            }
            $writer = new Zend_Config_Writer_Array();
            $writer->write($configPath, new Zend_Config($configArray));
            return $configArray;
        }
    }

    private function cacheHasChildren()
    {
        $configPath = APPLICATION_PATH . '/configs/category/haschildren.php';
        if (file_exists($configPath) && is_array($configArray = include($configPath))) {
            return $configArray;
        } else {
            $q = Doctrine_Query::create()->from('Default_Model_Category')->execute();
            $configArray = array();
            foreach ($q as $node) {
                $c = Doctrine_Core::getTable('Default_Model_Category')->find($node['id']);
                if ($c->getNode()->hasChildren()) {
                    $configArray[$node->id] = 1;
                } else {
                    $configArray[$node->id] = 0;
                }
            }
            $writer = new Zend_Config_Writer_Array();
            $writer->write($configPath, new Zend_Config($configArray));
            return $configArray;
        }
    }

}

?>
