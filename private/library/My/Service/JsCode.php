<?php

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Service_JsCode
{

    private $_filename;

    public function __construct()
    {
        $this->_filename = APPLICATION_PATH . '/configs/jscode.php';
    }

    public function write($val)
    {
        $f = fopen($this->_filename, 'w+');
        fputs($f, $val);
    }

    public function read()
    {
        if (is_file($this->_filename)) {
            $file = file_get_contents($this->_filename);
            return $file;
        } else {            
            return false;
        }
    }

}
