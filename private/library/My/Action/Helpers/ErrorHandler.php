<?php

/**
 * Description of Cache
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class My_Action_Helper_ErrorHandler extends Zend_Controller_Action_Helper_Abstract
{
    
    private static $_handler;
    
    public static function Handler()
    {
        if (!self::$_handler) {
            self::$_handler = new My_Error_Handler();
        }

        return self::$_handler;
    }
    
    public function error($code)
    {
        return self::Handler()->ThrowException($code);
    }
    
   
}
