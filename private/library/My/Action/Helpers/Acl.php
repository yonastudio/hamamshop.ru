<?php
 /**
 * Description of Acl
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Action_Helper_Acl extends Zend_Controller_Action_Helper_Abstract
{
    public function checkAllowed($resource)
    {
        $storage = Zend_Auth::getInstance()->getStorage()->read();
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
        if (!$storage) {
            $redirector->gotoUrl('/admin');
        }

        $errorHandler = Zend_Controller_Action_HelperBroker::getStaticHelper('ErrorHandler');
        $acl = new My_Acl_Admin();
        $role = $acl->getRole($storage->access_level);

        $acl = new My_Acl_Admin();

        if (!$acl->isAllowed($role, $resource)) {
            $errorHandler->error(401);
        }
    }
}
