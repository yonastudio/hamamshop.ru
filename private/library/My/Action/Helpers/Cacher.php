<?php

/**
 * Description of Cache
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class My_Action_Helper_Cacher extends Zend_Controller_Action_Helper_Abstract
{
    
    private $_cachemanager;
    
    public function __construct()
    {
        $this->_cachemanager = Zend_Registry::get('cachemanager');
    }
    
    public function get($key)
    {
        return $this->_cachemanager->load($key);
    } 
    
    public function set($data, $key, $time = 300)
    {
        $this->_cachemanager->save($data, $key, array(), $time);
    } 
    
    public function remove($key)
    {
        return $this->_cachemanager->remove($key);
    } 
   
}
