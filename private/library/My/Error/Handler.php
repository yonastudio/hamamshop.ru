<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Handler
 *
 * @author tos
 */
class My_Error_Handler extends Zend_Controller_Action_Helper_Abstract
{

    public function ThrowException($code = null)
    {
        $layout = Zend_Layout::getMvcInstance();
        $view = Zend_Layout::getMvcInstance()->getView();
        $layout->disableLayout();

        switch ($code) {
            case '401' :
                $this->getResponse()->setHttpResponseCode(401);
                echo $view->render('error/401.phtml');
                break;
            case '404' :
                $this->getResponse()->setHttpResponseCode(404);
                echo $view->render('error/404.phtml');
                break;
            case '500' :
                $this->getResponse()->setHttpResponseCode(500);
                echo $view->render('error/500.phtml');
                break;
            default:
                $this->getResponse()->setHttpResponseCode(404);
                echo $view->render('error/404.phtml');
                break;
        }
        exit;
    }

}
