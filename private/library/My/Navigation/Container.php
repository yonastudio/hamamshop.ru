<?php

/**
 * Description of My_Navigation_Container
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Navigation_Container 
{

    public static function topMenu() {
        $pages = array(
            array(
                'label' => _('Главная'),
                'title' => _('Главная'),
                'controller' => 'index',
                'action' => 'index',
                'route' => 'default',
            ),
            array(
                'label' => _('О бренде'),
                'title' => _('О бренде'),
                'controller' => 'pages',
                'action' => 'index',
                'route' => 'page',
                'params' => array(
                    'slug' => 'brand'
                )
            ),
            array(
                'label' => _('Статьи'),
                'title' => _('Статьи'),
                'controller' => 'articles',
                'action' => 'index',
                'route' => 'default',
            ),
            array(
                'label' => _('Контакты'),
                'title' => _('Контакты'),
                'controller' => 'pages',
                'action' => 'contacts',
                'route' => 'contacts',
            ),

        );
        $container = new Zend_Navigation($pages);
        return $container;
    }

}
