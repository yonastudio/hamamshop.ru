<?php

/**
 * Description of My_Navigation_Admin
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Navigation_Categories
{

    public function categories()
    {
        $prefix = Zend_Registry::get('prefix');

        $category = My_Registry::Category();
        $categories = $category->getCategoryChildrenById(2);

        $pages = array();

        if ($categories) {
            $i = 0;
            foreach ($categories as $el) {
                if ($category->hasCategoryChildren($el['id'])) {
                    $pages[] = array(
                        'title' => $el['title' . $prefix],
                        'label' => $el['title' . $prefix],
                        'controller' => 'shop',
                        'action' => 'filter',
                        'route' => 'shop_filter',
                        'params' => array(
                            'filter_params' => 'name=' . $el['name']
                        ),
                    );
                    $ch = $this->process($el['id']);
                    if ($ch) {
                        $pages[$i]['pages'] = $ch;
                    }
                } else {
                    $pages[] = array(
                        'title' => $el['title' . $prefix],
                        'label' => $el['title' . $prefix],
                        'controller' => 'shop',
                        'action' => 'filter',
                        'route' => 'shop_filter',
                        'params' => array(
                            'filter_params' => 'name=' . $el['name']
                        ),
                    );
                }
                $i++;
            }
        }
        $container = new Zend_Navigation($pages);
        return $container;
    }

    public function process($id)
    {
        $category = My_Registry::Category();
        $children = $category->getCategoryChildrenById($id);
        $prefix = Zend_Registry::get('prefix');
        $array = array();
        if ($children) {
            $i = 0;
            foreach ($children as $el) {
                if ($category->hasCategoryChildren($el['id'])) {
                    $array[] = array(
                        'title' => $el['title' . $prefix],
                        'label' => $el['title' . $prefix],
                        'uri' => '#',
                        'class' => 'parent',
                    );
                    $ch = $this->process($el['id']);
                    if ($ch) {
                        $array[$i]['pages'] = $ch;
                    }
                } else {
                    $array[] = array(
                        'title' => $el['title' . $prefix],
                        'label' => $el['title' . $prefix],
                        'controller' => 'shop',
                        'action' => 'filter',
                        'route' => 'shop_filter',
                        'params' => array(
                            'filter_params' => 'name=' . $el['name']
                        ),
                    );
                }
                $i++;
            }
        }
        if (count($array) > 0) {
            return $array;
        } else {
            return;
        }
    }

}
