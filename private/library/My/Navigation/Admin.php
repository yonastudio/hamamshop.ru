<?php

/**
 * Description of My_Navigation_Admin
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Navigation_Admin
{

    public static function panel()
    {
        $pages = array(
            array(
                'label' => 'Страницы',
                'title' => 'Страницы',
                'controller' => 'pages',
                'action' => 'list',
                'route' => 'default',
                'resource' => 'pages',
            ),
            array(
                'label' => 'Статьи',
                'title' => 'Статьи',
                'controller' => 'articles',
                'action' => 'list',
                'route' => 'default',
                'resource' => 'articles',
            ),
            array(
                'label' => 'Новости',
                'title' => 'Новости',
                'controller' => 'news',
                'action' => 'list',
                'route' => 'default',
                'resource' => 'news',
            ),
            /*array(
                'label' => 'Галереи',
                'title' => 'Галереи',
                'controller' => 'gallery',
                'action' => 'list',
                'route' => 'default',
            ),*/
            /* array(
              'label' => 'Рассылки',
              'title' => 'Рассылки',
              'module' => 'default',
              'controller' => 'subscribe',
              'action' => 'index',
              'route' => 'default',
              ), */
            array(
                'label' => 'Категории',
                'title' => 'Категории',
                'uri' => '#',
                'resource' => 'categories',
                'pages' => array(
                    array(
                        'label' => 'Управление',
                        'title' => 'Управление',
                        'controller' => 'categories',
                        'action' => 'index',
                        'route' => 'default',
                    ),
                    array(
                        'label' => 'Очистить кеш категорий',
                        'title' => 'Очистить кеш категорий',
                        'controller' => 'categories',
                        'action' => 'clearcache',
                        'route' => 'default',
                    )
                )
            ),
            /*array(
                'label' => 'Каталог',
                'title' => 'Каталог',
                'uri' => '#',
                'pages' => array(
                    array(
                        'label' => 'Производители',
                        'title' => 'Производители',
                        'controller' => 'catalog-brands',
                        'action' => 'list',
                        'route' => 'default',
                    ),
                    array(
                        'label' => 'Товары/Услуги',
                        'title' => 'Товары/Услуги',
                        'controller' => 'catalog-items',
                        'action' => 'list',
                        'route' => 'default',
                    )
                )
            ),       */
            array(
                'label' => 'Интернет-магазин',
                'title' => 'Интернет-магазин',
                'uri' => '#',
                'pages' => array(
                    array(
                        'label' => 'Серии',
                        'title' => 'Серии',
                        'controller' => 'shop-brands',
                        'action' => 'list',
                        'route' => 'default',
                        'resource' => 'shop:brands',
                    ),
                    array(
                        'label' => 'Цвета',
                        'title' => 'Цвета',
                        'controller' => 'shop-colors',
                        'action' => 'list',
                        'route' => 'default',
                        'resource' => 'shop:colors',
                    ),
                    array(
                        'label' => 'Товары',
                        'title' => 'Товары',
                        'controller' => 'shop-models',
                        'action' => 'list',
                        'route' => 'default',
                        'resource' => 'shop:products',
                    ),
                    array(
                        'label' => 'Экспорт',
                        'title' => 'Экспорт',
                        'uri' => '/shop-export/create',
                        'resource' => 'shop:export',
                    ),
                    array(
                        'label' => 'Импорт',
                        'title' => 'Импорт',
                        'controller' => 'shop-export',
                        'action' => 'import',
                        'route' => 'default',
                        'resource' => 'shop:import',
                    ),
                    array(
                        'label' => 'Заказы',
                        'title' => 'Заказы',
                        'controller' => 'shop-order',
                        'action' => 'list',
                        'route' => 'default',
                        'resource' => 'shop:orders',
                    ),
                    array(
                        'label' => 'Пользователи',
                        'title' => 'Пользователи',
                        'controller' => 'users',
                        'action' => 'list',
                        'route' => 'default',
                        'resource' => 'users',
                    ),
                ),
            ),
            /*array(
                'label' => 'Управление меню',
                'title' => 'Управление меню',
                'controller' => 'menu',
                'action' => 'list',
                'route' => 'default',
            ), */
            /*array(
                'label' => 'SeoText',
                'title' => 'SeoText',
                'controller' => 'seotext',
                'action' => 'list',
                'route' => 'default',
            ),
            array(
                'label' => 'SeoUrl',
                'title' => 'SeoUrl',
                'controller' => 'seourl',
                'action' => 'list',
                'route' => 'default',
            ),*/
            array(
                'label' => 'Сервис',
                'title' => 'Сервис',
                'uri' => '#',
                'resource' => 'service',
                'pages' => array(
                    array(
                        'label' => 'Javascript',
                        'title' => 'Javascript',
                        'controller' => 'service',
                        'action' => 'jscode',
                        'route' => 'default',
                        'resource' => 'javascript',
                    ),
                    array(
                        'label' => 'Сгенерировать карту сайта',
                        'title' => 'Сгенерировать карту сайта',
                        'uri' => '/phpsitemap/' . SECRET . '/cron.php',
                        'resource' => 'sitemap',
                    )
                )
            ),
            array(
                'label' => 'Администраторы',
                'title' => 'Администраторы',
                'controller' => 'admin',
                'action' => 'list',
                'route' => 'default',
                'resource' => 'administrators',
            )
        );

        $container = new Zend_Navigation($pages);
        return $container;
    }

}
