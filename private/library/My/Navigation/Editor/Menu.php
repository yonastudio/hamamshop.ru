<?php
 /**
 * Description of Menu
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

define(MENU_PATH, APPLICATION_PATH . '/configs/menu/');

class My_Navigation_Editor_Menu
{

    function parse($configArray)
    {

    }

    function getConfig($name)
    {
        $file = MENU_PATH . $name . '.php';
        if (file_exists($file) && is_array($configArray = include($file))
        ) {
            return $configArray;
        }
    }

    public function create($name, $configArray)
    {
        $file = MENU_PATH . $name . '.php';
        if (is_file($file)) {
            unlink($file);
        }
        $writer = new Zend_Config_Writer_Array();
        $writer->write($file, new Zend_Config($configArray, true));
    }

    /*public function find($evercookie)
    {
        if (!$evercookie) {
            return;
        }
        $configArray = $this->all();
        return $configArray[$evercookie . '_'];
    }

    public function all()
    {
        if (file_exists(EVERCOOKIE_PATH)
                && is_array($configArray = include(EVERCOOKIE_PATH))) {
            return $configArray;
        }
    }

    public function add($evercookie, $name)
    {
        $configArray = $this->all();
        if (strlen($evercookie) > 6 && $name) {
            $configArray[$evercookie . '_'] = $name;
            $this->create($configArray);
        }
    }

    public function delete($evercookie)
    {
        $configArray = $this->all();
        $new = array();
        foreach ($configArray as $el => $val) {
            if ($el != $evercookie) {
                $new[$el] = $val;
            }
        }
        $this->create($new);
    } */

}
