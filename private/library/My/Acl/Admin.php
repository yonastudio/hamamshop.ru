<?php
 /**
 * Description of Roles
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Acl_Admin extends Zend_Acl
{

    public $roles = array(
        '10' => 'user',
        '20' => 'editor',
        '100' => 'administrator'
    );

    public function __construct()
    {
        $this->addRole(new Zend_Acl_Role('user'));
        $this->addRole(new Zend_Acl_Role('administrator'));
        $this->addRole(new Zend_Acl_Role('editor'),'administrator');

        $this->add(new Zend_Acl_Resource('pages'));
        $this->add(new Zend_Acl_Resource('articles'));
        $this->add(new Zend_Acl_Resource('news'));
        $this->add(new Zend_Acl_Resource('categories'));

        $this->add(new Zend_Acl_Resource('shop:brands'));
        $this->add(new Zend_Acl_Resource('shop:colors'));
        $this->add(new Zend_Acl_Resource('shop:products'));
        $this->add(new Zend_Acl_Resource('shop:export'));
        $this->add(new Zend_Acl_Resource('shop:import'));
        $this->add(new Zend_Acl_Resource('shop:orders'));
        $this->add(new Zend_Acl_Resource('users'));

        $this->add(new Zend_Acl_Resource('service'));
        $this->add(new Zend_Acl_Resource('sitemap'));
        $this->add(new Zend_Acl_Resource('javascript'));

        $this->add(new Zend_Acl_Resource('administrators'));

        $this->allow('administrator');

        $this->deny('editor','administrators');        
        $this->deny('editor','javascript');

        $this->allow('user','shop:orders');
    }

    public function getRole($key)
    {
        return $this->roles[$key];
    }

}
