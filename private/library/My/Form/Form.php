<?php

/**
 * Description of My_Form_Form
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Form_Form extends Zend_Form
{

    public static $_paramId;

    /* Декораторы для табличной структуры
     * ===================================
     * public $elementDecorators = array(
      'ViewHelper',
      'Errors',
      array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
      array('Label', array('tag' => 'td', 'class' => 'element-label')),
      array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
      );
      public $buttonDecorators = array(
      'ViewHelper',
      array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'buttonelement')),
      array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
      array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
      );
      public $formDecorators = array(
      'FormElements',
      array('HtmlTag', array('tag' => 'table', 'class' => 'decorated')),
      'Form',
      );
     */

    /* Декораторы для структуры Twitter Bootstrap
     * ==========================================
     */
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input')),
        array('Label', array()),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clearfix')),
    );
    public $elementDecoratorsNormal = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input')),
        array('Label', array()),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element')),
    );

    public $ckeckboxDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data2' => 'HtmlTag'), array('tag' => 'li')),
        array(array('data1' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'inputs-list')),
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input')),
        array('Label', array()),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clearfix')),
    );
    public $tableCkeckboxDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data2' => 'HtmlTag'), array('tag' => 'li')),
        array(array('data1' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'label-disp-inl inputs-list')),
        array('Label', array()),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input')),
        array(array('label' => 'HtmlTag'), array('tag' => 'div', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clearfix')),
    );
    public $formDecorators = array(
        'FormElements',
        array('HtmlTag', array('tag' => 'fieldset')),
        'Form',
    );
    public $noneDecorators = array(
        'ViewHelper',
        'Errors',
        array(),
        array(),
    );

    public function removeAllDecorators()
    {
        foreach ($this->getElements()as $elem) {
            $elem->removeDecorator('Label')
                    ->removeDecorator('HtmlTag');
        }
        return $this;
    }

    public function createSelectElementArray($array, $key_column, $val_column, $firstnull = false)
    {
        if (!is_array($array)) {
            $array = $this->getArrayForFormSelect($array, $val_column);
        }

        $result = array();
        if ($firstnull) {
            $result[0] = ' ';
        }
        foreach ($array as $el) {
            $result[$el[$key_column]] = $el[$val_column];
        }
        return $result;
    }

    private function getArrayForFormSelect($table, $val_column)
    {
        $q = Doctrine_Query::create()
                ->from($table)
                ->orderBy($val_column . ' ASC');
        $rows = $q->fetchArray();
        return $rows;
    }

    public function addValidatorNRE($table, $field, $id = null, $unique_field = null)
    {
        if ($unique_field) {
            $this->$field->addValidator(new My_Validate_NoRecordExists($table, $field, $id, $unique_field));
        } else {
            if ($id) {
                $this->$field->addValidator(new My_Validate_NoRecordExists($table, $field, $id));
            } else {
                $this->$field->addValidator(new My_Validate_NoRecordExists($table, $field));
            }
        }
    }

}
