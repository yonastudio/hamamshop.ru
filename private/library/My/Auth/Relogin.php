<?php

/**
 * Description of Relogin
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class My_Auth_Relogin
{
    public function relogin($data)
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();

        $storagedata = new stdClass();
        foreach ($data as $el => $val) {
            $storagedata->{$el} = $val;
        }
        $auth->getStorage()->write($storagedata);
    }
}
