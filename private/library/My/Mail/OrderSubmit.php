<?php
 /**
 * Description of OrderSubmit
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Mail_OrderSubmit extends My_Mail_Abstract
{

    public function sendHash($data)
    {
        $lang = Zend_Registry::get('lang');

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($data['email']);
        $mail->addTo('info@hamamshop.ru');

        switch ($lang) {
            case 'ru' :
                $mail->setSubject('Оформление заказа, hamamshop.ru');
                $link = SERVERNAME . '/shop-order/order-submitted?order_id=' . $data['order_id'] . '&hash=' . $data['hash'];
                $text = '<p><b>Оформление заказа на ' . SERVERNAME . '</b></p>';
                $text .= "<p>Для подтверждения заказа, Вам необходимо перейти по <a href=\"" . $link . "\">ссылке</a></p>";
                if ($data['payment'] == '2') {
                    $text .= '<p>Для оплаты заказа через VISA/MasterCard перейдите по ссылке подтверждения. Следуйде инструкциям на странице.</p>';
                }
                $text .= $link;
                break;
            case 'en' :
                $mail->setSubject('Checkout, hamamshop.ru');
                $link = SERVERNAME . '/en/shop-order/order-submitted?order_id=' . $data['order_id'] . '&hash=' . $data['hash'];
                $text = '<p><b>Checkout on ' . SERVERNAME . '</b></p>';
                $text .= "<p>To confirm your order, you need to move on this <a href=\"" . $link . "\">link</a></p>";
                    if ($data['payment'] == '2') {
                    $text .= '<p>For payment order by VISA / MasterCard pass on links confirmation. Follow the instructions on page.</p>';
                }
                $text .= $link;
                break;
        }
        $mail->setBodyHtml($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>' . $text . '</pre>';
            return;
        }

        $mail->send();
    }

    public function sendNotification($email, $order_id)
    {
        $lang = Zend_Registry::get('lang');

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($email);
        $mail->addCc('info@hamamshop.ru');

        $order_number = sprintf("%04d", $order_id);

        switch ($lang) {
            case 'ru' :
                $mail->setSubject('Заказ ринят, hamamshop.ru');
                $text = $this->view->translate("Спасибо! Ваш заказ принят. Мы обязательно свяжемся с Вами в ближайшее время.") . "\n";
                $text .= "Ваш номер заказа: " . $order_number . "\n";
                break;
            case 'en' :
                $mail->setSubject('Order received, hamamshop.ru');
                $text = $this->view->translate("Спасибо! Ваш заказ принят. Мы обязательно свяжемся с Вами в ближайшее время.") . "\n";
                $text .= "Your order number: " . $order_number . "\n";
                break;
        }
        $mail->setBodyText($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>' . $text . '</pre>';
            return;
        }

        $mail->send();
    }

}
