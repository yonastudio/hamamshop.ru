<?php
 /**
 * Description of OrderPayed
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Mail_OrderPayed extends My_Mail_Abstract
{
    public function sendNotification($data)
    {
        $lang = Zend_Registry::get('lang');

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($data['email']);
        $mail->addTo('info@hamamshop.ru');

        ob_start();
        printf("%04d", $data['id']);
        $order_number = ob_get_contents();
        ob_clean();

        switch($lang) {
            case 'ru' :
                $mail->setSubject('Заказ оплачен, Hamam.ua');
                $text = "Спасибо! Заказ №" . $order_number . " успешно оплачен. Мы обязательно свяжемся с Вами в ближайшее время.";
                break;
            case 'en' :
                $mail->setSubject('Order is paid, Hamam.ua');
                $text = "Thank you! Order №" . $order_number . " successfully paid. We will contact you shortly.";
                break;
        }

        $mail->setBodyText($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>'.$text.'</pre>';
            return;
        }

        $mail->send();
    }
}
