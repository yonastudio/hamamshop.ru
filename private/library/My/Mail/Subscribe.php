<?php

/**
 * Description of Subscribe
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Болонин Андрей <pilotftzi@gmail.com>
 */
class My_Mail_Subscribe extends My_Mail_Abstract
{

    public function __construct()
    {
        $this->view = Zend_Layout::getMvcInstance()->getView();
    }

    public function sendMessageWithAttachment($filename, $text, $from, $to, $subject, $name_from = '', $name_to = '')
    {
        $mail = new Zend_Mail('UTF-8');
        $at = new Zend_Mime_Part($filename);

        $mail->setBodyHtml($text);
        $mail->setFrom($from, $name_from);
        $mail->addTo($to, $name_to);
        $mail->setSubject($subject);

        if ($filename) {
            $fileContents = file_get_contents(INVOICE_DIR . $filename);
            $attachment = $mail->createAttachment($fileContents);
            $attachment->filename = $filename;
        }

        if (APPLICATION_ENV != 'development') {
            if ($mail->send()) {
                return true;
            }
        }
    }

    public function sendMessageFeedback($post)
    {
        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom('info@hamamshop.ru', 'hamamshop.ru');
        $mail->addTo('info@hamamshop.ru');
        $mail->setSubject('Обратная связь');

        $html = '';
        foreach ($post as $el => $val) {
            if (in_array($el, array('name', 'email', 'phone', 'message'))) {
                $html .= "<p>$el : $val</p>";
            }
        }
        $mail->setBodyHtml($html);

        if (APPLICATION_ENV != 'development') {
            if ($mail->send()) {
                return true;
            }
        }
    }

}