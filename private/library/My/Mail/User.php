<?php
 /**
 * Description of User
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Mail_User extends My_Mail_Abstract
{
    public function registrationNotiffy($data, $lang = 'ru')
    {
        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($data['email']);

        $login = $data['email'];
        $password = $data['password_1'];

        switch ($lang) {
            case 'ru' :
                $mail->setSubject('Регистрация на ' . SERVERNAME);
                $text = "Поздравляем! Вы успешно зарегистрировались на сайте " . SERVERNAME . "!\n";
                $text .= "Ваши регистрационные данные:\n";
                $text .= "Логин: $login\n";
                $text .= "Пароль: $password\n";
                $text .= "Сохраните это письмо";
                break;
            case 'en' :
                $mail->setSubject('Registration on ' . SERVERNAME);
                $text = "Congratulations! You have successfully registered on the site " . SERVERNAME . "!\n";
                $text .= "Your username and password:\n";
                $text .= "Username: $login\n";
                $text .= "Password: $password\n";
                $text .= "Save this letter";
                break;
        }
        $mail->setBodyText($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>'.$text.'</pre>';
            return;
        }

        $mail->send();
    }

    public function lostPasswordInstructions($user)
    {
        $lostpass = new My_User_LostPassword();
        $hash = $lostpass->getHash($user);

        $lang = Zend_Registry::get('lang');

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($user['email']);

        $link = SERVERNAME . '/' . $lang . '/users/lost-password-renew?uid=' . $user['id'] . '&hash=' . $hash;

        switch ($lang) {
            case 'ru' :
                $mail->setSubject('Восстановление пароля на ' . SERVERNAME);
                $text = "<p>Для восстановления пароля перейдите по ссылке <a href=\"" . $link . "\">" . $link . "</a></p>";
                $text .= "<p>Ссылка будет доступна до конца текущих суток.</p>";
                $text .= "<p>Если это письмо пришло к Вам случайно, проигнорируйте его. Спасибо.</p>";
                break;
            case 'en' :
                $mail->setSubject('Password recovery on ' . SERVERNAME);
                $text = "<p>To recover your password please go to link <a href=\"" . $link . "\">" . $link . "</a></p>";
                $text .= "<p>The link will be available until the end of the current day.</p>";
                $text .= "<p>If this letter came to You by chance, ignore him. Thank You.</p>";
                break;
        }
        $mail->setBodyHtml($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>'.$text.'</pre>';
            return;
        }

        $mail->send();
    }

    public function renewPassword($email, $newpassword)
    {
        $lang = Zend_Registry::get('lang');

        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom(MAIL_ADRESS, MAIL_FROM);
        $mail->addTo($email);

        switch ($lang) {
            case 'ru' :
                $mail->setSubject('Данные для авторизации на ' . SERVERNAME);
                $text = "Ваши данные для авторизации:\n";
                $text .= "Логин: $email\n";
                $text .= "Пароль: $newpassword\n";
                $text .= "Сохраните это письмо";
                break;
            case 'en' :
                $mail->setSubject('Шnformation for authentication on ' . SERVERNAME);
                $text = "Your information for authentication:\n";
                $text .= "Login: $email\n";
                $text .= "Password: $newpassword\n";
                $text .= "Save this letter";
                break;
        }
        $mail->setBodyText($text);

        if (APPLICATION_ENV == 'development') {
            echo '<pre>'.$text.'</pre>';
            return;
        }

        $mail->send();
    }

}
