<?php
 /**
 * Description of Memcached
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Cache_Memcached
{

    public static function implodeParams($params)
    {
        if ($params) {
            return '_' . implode('', $params);
        } else {
            return;
        }
    }

    public static function MCkey($method, $params, $lang = 'ru')
    {
        if (is_array($params)) {
            $params = self::implodeParams($params);
        }
        $params = substr(md5($params),0,10);
        $options = $method . $params . '_' . $lang;
        return $options;
    }

}
