<?php

/**
 * Description of My_Registry
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Registry extends Zend_Registry
{
    private static $_model = null;
    private static $_imageFilter = null;
    private static $_category = null;

    public static function Category()
    {
        if (!self::$_category) {
            self::$_category = new My_Category_Category();
        }

        return self::$_category;
    }

    public static function Model()
    {
        if (!self::$_model) {
            self::$_model = new My_Widget_Model();
        }

        return self::$_model;
    }

    public static function imageFilter()
    {
        if (!self::$_imageFilter) {
            self::$_imageFilter = new My_Class_Image_Filter();
        }

        return self::$_imageFilter;
    }

    public static function Auth($access_level = null)
    {
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $storage = Zend_Auth::getInstance()->getStorage();
        $data = $storage->read();
        if (!$data || !$data->access_level) {
            $redirector->gotoUrl('/admin/login');
        }
        if ($data->access_level < $access_level) {
            $redirector->gotoUrl('/admin/noaccess');
        }
        return $data;
    }

}
