<?php
 /**
 * Description of Acl
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Plugin_Acl_Admin extends Zend_Controller_Plugin_Abstract
{

    private $_acl = null;
    private $_role;

    public function __construct()
    {
        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$storage->access_level) {
            return;
        }
        $this->_acl = new My_Acl_Admin();
        $this->_role = $this->_acl->getRole($storage->access_level);
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $menu = $view->navigation()->menu($view->adminPanel);
        $menu->setAcl($this->_acl)->setRole($this->_role);
    }

}
