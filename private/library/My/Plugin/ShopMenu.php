<?php
 /**
 * Description of ShopMenu
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Plugin_ShopMenu extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $view = Zend_Layout::getMvcInstance()->getView();

        $container = new My_Navigation_Categories();
        $view->shopMenu = $container->categories();
    }
}
