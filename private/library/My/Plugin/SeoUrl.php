<?php

/**
 * Description of My_Form_Form
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Plugin_SeoUrl extends Zend_Controller_Plugin_Abstract
{

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        $view = Zend_Layout::getMvcInstance()->getView();

        $url = $_SERVER['REQUEST_URI'];
        $cachemanager = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');

        $data = $cachemanager->get('seourl_' . md5($url));
        if (!$data) {
            $seo_text = new Default_Model_SeoUrl();
            $data = $seo_text->getSeoUrlByUrl($url);
            $cachemanager->set('_' . $data, 'seourl_' . md5($url), 3600);
        } else {
            $data = substr($data, 1);
        }

        if (!$data) {
            return;
        }

        $prefix = Zend_Registry::get('prefix');
        $view->headTitle($data['title' . $prefix], 'SET');
        $view->headMeta()->setName('description', $data['description' . $prefix]);
        $view->headMeta()->setName('keywords', $data['keywords' . $prefix]);

    }

}

