<?php

/**
 * Description of My_Form_Form
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Plugin_LangSelector extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $bootstrap = Zend_Controller_Front::getInstance()
                        ->getParam('bootstrap');

        $lang = $request->getParam('lang');

        if (!in_array($lang, array('ru', 'uk', 'en'))) {
            $request->setParam('lang', 'ru');
        }

        $lang = $request->getParam('lang');
        if ($lang == 'ru') {
            $locale = 'ru_RU';
            $prefix = '';
        } elseif ($lang == 'uk') {
            $locale = 'uk_UA';
            $prefix = '_uk';
        } else {
            $locale = 'en_US';
            $prefix = '_en';
        }

        $zl = new Zend_Locale();
        $zl->setLocale($locale);
        Zend_Registry::set('Zend_Locale', $zl);
        Zend_Registry::set('lang', $lang);
        Zend_Registry::set('prefix', $prefix);

        $cache = Zend_Registry::get('cachemanager');
        Zend_Locale::setCache($cache);

        $translate = $bootstrap->getResource('translate');
        $translate->addTranslation(APPLICATION_PATH . '/languages/' . $lang . '.mo', $locale);

        if ($lang != 'en') {
            $translator = new Zend_Translate('array', APPLICATION_PATH . '/languages/' . $lang . '.php', $locale);
            Zend_Validate_Abstract::setDefaultTranslator($translator);
        }

    }

}

