<?php
 /**
 * Description of My_Controller_Abstract
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
abstract class My_Controller_Abstract extends Zend_Controller_Action
{

    protected $form;
    protected $request;
    protected $Model;
    protected $ModelName;
    protected $controller;
    protected $title;
    protected $aclResource;

    protected function listAction()
    {
        $this->_helper->Acl->checkAllowed($this->aclResource);
        $this->_helper->layout()->setLayout('admin');

        $this->view->entries = $this->Model->all();

        $this->view->headTitle($this->title);
        $this->view->title = $this->title;

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed($this->aclResource);
        $this->_helper->layout()->setLayout('admin');

        $data = $this->request->getPost();

        if ($this->request->isPost() && $this->form->isValid($data)) {
            $formData = $this->form->getValues();
            $this->Model->addItem($formData);
            $this->_redirect($this->controller . '/list');
        }

        $this->view->headTitle($this->title);
        $this->view->title = $this->title;

        $this->view->form = $this->form;

        $this->view->setActiveMenu('adminPanel', $this->title);
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed($this->aclResource);
        $this->_helper->layout()->setLayout('admin');

        $data = $this->request->getPost();
        $id = $this->_getParam('id');

        if ($this->request->isPost() && $this->form->isValid($data)) {
            $formData = $this->form->getValues();
            $this->Model->saveItem($formData, $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->Model->find($id);
        $this->form->populate($data);
        $this->view->form = $this->form;

        $this->view->headTitle($this->title);
        $this->view->title = $this->title;

        $this->view->form = $this->form;

        $this->view->setActiveMenu('adminPanel', $this->title);
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed($this->aclResource);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable($this->ModelName)->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}
