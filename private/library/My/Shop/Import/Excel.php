<?php
 /**
 * Description of Excel
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Shop_Import_Excel
{

    public function read($filename)
    {
        require_once PATH_LIBRARY . '/ExcelReader.php';

        $data = new Spreadsheet_Excel_Reader(TEMP_DIR . $filename);

        $rows = $data->rowcount();
        $cols = $data->colcount();
        $x = 0;
        $arr = array();
        while ($x <= $rows) {
            $y = 1;
            while ($y <= $cols) {
                $arr[$x][$y] = $data->val($x + 1, $y);
                $y++;
            }
            $x++;
        }

        if (file_exists(TEMP_DIR . $filename)) {
            unlink(TEMP_DIR . $filename);
        }

        return $arr;
    }

}
