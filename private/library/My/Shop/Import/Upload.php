<?php
 /**
 * Description of Upload
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Shop_Import_Upload
{

    public function upload()
    {
        $upload = new Zend_File_Transfer_Adapter_Http();

        $upload->addValidator('Size', false, 1000000);
        $upload->addValidator('Extension', false, 'xls');

        $filename = md5(time()) . '.xls';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $filename,
            'overwrite' => true
        ));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $filename)) {
                unlink(TEMP_DIR . $filename);
            }
            var_dump($upload->getMessages());
            return false;
        }

        return $filename;
    }

}
