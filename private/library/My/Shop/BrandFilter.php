<?php

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Shop_BrandFilter
{

    public function filter($category_name)
    {
        $q = Doctrine_Query::create()
                ->select('b.*')
                ->from('Default_Model_Brand b')
                ->leftJoin('b.Models m')
                ->leftJoin('m.Category c')
                ->orderBy('b.name ASC');
        if ($category_name) {
            $q->andWhere('c.name = ?', $category_name);
        }
        $rows = $q->fetchArray();
        return $rows;
    }

}
