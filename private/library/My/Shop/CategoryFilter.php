<?php

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
class My_Shop_CategoryFilter
{

    public function filter($params = array())
    {
        $q = Doctrine_Query::create()
                ->select('md.*')
                ->from('Default_Model_Modification md')
                ->leftJoin('md.Model m')
                ->leftJoin('m.Brand b')
                ->leftJoin('m.Category c')
                ->where('status <> 0');
        if ($params['name']) {
            $q->andWhere('c.name = ?', $params['name']);
        }
        if ($params['brand']) {
            $q->andWhere('b.name = ?', $params['brand']);
        }
        if ($params['by']) {
            $by = strtoupper($params['by']);
        } else {
            $by = 'ASC';
        }
        if ($params['sort'] && in_array($by, array('ASC', 'DESC'))) {
            $q->orderBy($params['sort'] . ' ' . $params['by']);
        } else {
            $q->orderBy('sortorder DESC');
        }
        $rows = $q->fetchArray();
        return $rows;
    }

}
