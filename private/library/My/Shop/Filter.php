<?php
 /**
 * Description of Filter
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Shop_Filter
{
    public function getParams($params)
    {
        $keys = explode('&',$params);
        $array = array();
        if (count($keys) > 0) {
            foreach ($keys as $el) {
                $keys = explode('=', $el);
                if (count($keys) == 2) {
                    $array[$keys[0]] = $keys[1];
                }
            }
        }
        return $array;
    }

    public function filter($data = array())
    {
        $prefix = Zend_Registry::get('prefix');
        $q = Doctrine_Query::create()
                ->select('sm.*,'
                . 'c.name AS c_name,'
                . 'c.id AS c_id,'
                . 'c.title' . $prefix . ' AS c_title,'
                . 'sb.name AS sb_name,'
                . 'sb.title AS sb_title,'
                )
                ->from('Default_Model_ShopModel sm')
                ->leftJoin('sm.Category c')
                ->leftJoin('sm.ShopBrand sb');
        if ($data['name']) {
            $category = My_Registry::Category();
            $category_id = $category->getCategoryIdByName($data['name']);
            if ($category->hasCategoryChildren($category_id)) {
                $children_ids = $category->childrenIdsArray($category_id);
                $children_ids[] = $category_id;
                $q->andWhereIn('sm.category_id', $children_ids);
            } else {
                $q->andWhere('c.name = ?', $data['name']);
            }
        }
        if ($data['brand']) {
            $q->andWhere('sb.name = ?', $data['brand']);
        }
        if ($data['group'] && in_array($data['group'], array('classic','avantGarde','pure','sport'))) {
            $varname = $data['group'];
            $mas = My_Storage_Shop_Groups::$array[$varname];
            $q->andWhereIn('sb.id', $mas);
        }
        if ($data['q']) {
            $search = $data['q'];
            $q->leftJoin('sm.ShopModelDescription smd');
            $q->andWhere(
                "sm.title".$prefix." LIKE '%".$search."%' OR"
                . " smd.description".$prefix." LIKE '%".$search."%' OR"
                . " c.title".$prefix." LIKE '%".$search."%' OR"
                . " sb.title LIKE '%".$q."%'"
            );
        }
        $by = (in_array($data['by'],array('asc','desc'))) ? $data['by'] : 'asc';
        if ($data['sort']) {
            $q->orderBy($data['sort'] . ' ' . strtoupper($by));
        }
        if (!$data['sort']) {
            //$q->orderBy('title ASC');
        }
        return $q;
    }

    public function filterPaginator($q, $page = 1, $limit = 12)
    {
        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($q);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);

        return $paginator;
    }

}
