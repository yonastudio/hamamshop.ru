<?php
 /**
 * Description of LiqPay
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Shop_Payment_LiqPay
{

    public function getForm($payment_order_id, $order_id, $sumprice)
    {
        $tr = Zend_Registry::get('Zend_Translate');
        $lang = Zend_Registry::get('lang');

        $merchant_id = LIQPAY_MID;
        $signature = LIQPAY_SIGNATURE;
        $url = "https://www.liqpay.com/?do=clickNbuy";
        $method = 'card';

        ob_start();
        printf("%04d", $order_id);
        $order_number = ob_get_contents();
        ob_clean();

        $order_id_hash = substr(md5($order_id), 0, 8);
        $xml = "<request>
		<version>1.2</version>
		<result_url>" . SERVERNAME . "/" . $lang . "/shop-order/payment-success?poid=$payment_order_id&amp;hash=$order_id_hash</result_url>
		<server_url></server_url>
		<merchant_id>$merchant_id</merchant_id>
		<order_id>$payment_order_id</order_id>
		<amount>$sumprice</amount>
		<currency>UAH</currency>
		<description>" . $tr->translate('Оплата заказа №') . ' ' .  $order_number . "</description>
		<pay_way>$method</pay_way>
		</request>
		";

        $xml_encoded = base64_encode($xml);
        $lgsignature = base64_encode(sha1($signature . $xml . $signature, 1));

        return '<form action="' . $url . '" method="POST">'
               . '<input type="hidden" name="operation_xml" value="' . $xml_encoded . '" />'
               . '<input type="hidden" name="signature" value="' . $lgsignature . '" />'
               . '<input type="submit" class="visasubmit" value="'.$tr->translate('Оплатить').'" />'
               . '</form>';
    }

}
