<?php
 /**
 * Description of Excel
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Shop_Export_Excel
{

    public function build($data)
    {
        $objPHPExcel = PHPExcel_IOFactory::load(APPLICATION_PATH . '/configs/export/excel_template.xls');

        $objPHPExcel->getProperties()->setTitle("Список товаров Hamam.ua");
        $objPHPExcel->setActiveSheetIndex(0);
        $document = $objPHPExcel->getActiveSheet();

        $count_rows = count($data);
        $modifColors = new Default_Model_ShopModifColor();

        $j = 0;
        $i = 0;
        foreach ($data as $el) {
            $j++;
            $title = $el['ShopModel']['ShopBrand']['title'] . ' ' . $el['ShopModel']['title'] . ' ' . $el['title'];

            $document->setCellValueByColumnAndRow(0, $i + 2, $el['id']);
            $document->setCellValueByColumnAndRow(2, $i + 2, $title);
            $document->setCellValueByColumnAndRow(3, $i + 2, $el['price']);
            $document->setCellValueByColumnAndRow(4, $i + 2, $el['status']);
            
            $mColorsRows = $modifColors->findByModifId($el['id']);
            if (count($mColorsRows) > 0) {
                foreach($mColorsRows as $cr) {
                    $i++;
                    $document->setCellValueByColumnAndRow(1, $i + 2, $cr['id']);
                    $document->setCellValueByColumnAndRow(2, $i + 2, $cr['service_title']);
                    $document->setCellValueByColumnAndRow(4, $i + 2, $cr['presence']);
                }                
            }
            $i++;
            $document->setCellValueByColumnAndRow(1, $i + 2, ' ');
            $i++;
        }

        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $filename = '/export/' . date('Y-m-d_His') . '.xls';
        $objWriter->save(DOCUMENT_ROOT . $filename);

        return $filename;
    }

}
