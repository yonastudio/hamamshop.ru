<?php

/**
 * Description of My_Form_Form
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
abstract class My_Storage_Abstract
{
    
    public $array;

    public function getValueByKey($key)
    {
        if (is_array($this->array)) {
            if ($this->array[$key] !== false) {
                return $this->array[$key];
            }            
        }
    }        

}
