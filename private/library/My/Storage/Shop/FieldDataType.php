<?php

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Storage_Shop_FieldDataType extends My_Storage_Abstract
{

    public $array = array(
        '0' => 'Текстовое поле',
        '1' => 'Да/Нет',
        '2' => 'Целочисленное',
        '3' => 'С десятичной дробью'
    );
    
}
