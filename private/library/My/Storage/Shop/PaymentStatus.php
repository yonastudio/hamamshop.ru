<?php
 /**
 * Description of PaymentStatus
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

array(
    _('Ожидает оплаты'),
    _('Оплачено'),
    _('Возврат средств'),
);
 
class My_Storage_Shop_PaymentStatus extends My_Storage_Abstract
{

    public function __construct()
    {
        $this->array = array(
            '0' => _('Ожидает оплаты'),
            '1' => _('Оплачено'),
            '2' => _('Возврат средств'),
        );
    }

}
