<?php
 /**
 * Description of My_Storage_Shop_Payment
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

array(
    _('Наличными'),
    _('VISA/MasterCard'),
);
 
class My_Storage_Shop_Payment extends My_Storage_Abstract
{

    public function __construct()
    {
        $this->array = array(
            '1' => _('Наличными'),
            //'2' => _('VISA/MasterCard'),
        );
    }

}
