<?php

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Storage_Shop_ActionType extends My_Storage_Abstract
{

    public $array = array(
        '1' => 'Скидка',
        '2' => '+ Подарок',
        '3' => '+ Покупка = Подарок',
    );
    
}
