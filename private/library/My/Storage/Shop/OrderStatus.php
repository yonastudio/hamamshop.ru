<?php
 /**
 * Description of OrderStatus
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

array(
    _('Ожидает подтверждения'),
    _('Заказ подтвержден'),
    _('Ожидает доставки/самовывоза'),
    _('Успешно'),
    _('Закрыт'),
);
 
class My_Storage_Shop_OrderStatus extends My_Storage_Abstract
{

    public function __construct()
    {
        $this->array = array(
            '0' => _('Ожидает подтверждения'),
            '1' => _('Заказ подтвержден'),
            '2' => _('Ожидает доставки/самовывоза'),
            '3' => _('Успешно'),
            '4' => _('Закрыт'),
        );

    }

}
