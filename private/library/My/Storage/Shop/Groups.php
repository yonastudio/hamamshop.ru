<?php
 /**
 * Description of Menu
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class My_Storage_Shop_Groups
{
 
    public static $array = array(
        'classic' => array(3, 11, 8, 4),
        'avantGarde' => array(9, 13, 27, 28, 21),
        'pure' => array(7, 16, 2, 17, 29, 25),
        'sport' => array(5, 20, 10, 6, 31, 18),
    );
    
}
