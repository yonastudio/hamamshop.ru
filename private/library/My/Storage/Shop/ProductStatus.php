<?php

array(
    _('В наличии'),
    _('Нет в наличии'),
    _('Есть на складе'),
    _('Под заказ'),
);

/**
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class My_Storage_Shop_ProductStatus extends My_Storage_Abstract
{

    public $array = array(
        '0' => 'Нет в наличии',
        '1' => 'В наличии',
        //'2' => 'Нет в наличии',
        //'3' => 'Есть на складе',
        //'4' => 'Под заказ',
    );
    
}
