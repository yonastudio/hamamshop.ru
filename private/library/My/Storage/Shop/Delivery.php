<?php
 /**
 * Description of Delivery
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
array(
    _('Самовывоз'),
    _('Доставка'),
);
 
class My_Storage_Shop_Delivery extends My_Storage_Abstract
{

    public function __construct()
    {
        $this->array = array(
            '1' => _('Самовывоз'),
            '2' => _('Доставка'),
        );
    }

}
