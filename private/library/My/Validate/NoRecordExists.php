<?php

/**
 * Description of Validate_NoRecordExists
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Болонин Андрей <pilotftzi@gmail.com>
 */
class My_Validate_NoRecordExists extends Zend_Validate_Abstract
{
    
    private $_table;
    private $_field;
    private $_translit_field;
    private $_id;

    const ERROR_RECORD_FOUND = 'recordFound';
    const ERROR_RECORD_FOUND_TRANSLIT = 'recordFoundTranslit';

    protected $_messageTemplates = array(
        self::ERROR_RECORD_FOUND => "Найдена запись, совпадающая со значением '%value%'",
        self::ERROR_RECORD_FOUND_TRANSLIT => "Найдена запись, совпадающая с уникальным идентификатором значения '%value%'"
    );

    public function __construct($table, $field, $id = null, $translit_field = null)
    {
        if (is_null(Doctrine_Core::getTable($table)))
            return null;
        if (!Doctrine_Core::getTable($table)->hasColumn($field))
            return null;
        if ($translit_field) {
            if (!Doctrine_Core::getTable($table)->hasColumn($translit_field))
                return null;
        }

        $this->_table = Doctrine_Core::getTable($table);
        $this->_field = $field;
        $this->_translit_field = $translit_field;

        if ($id) {
            $record = Doctrine_Core::getTable($table)->find($id);
            if ($record) {
                $this->_id = $id;
            } else {
                return null;
            }
        }
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $funcName = 'findOneBy' . $this->_field;
        $funcName_translit = 'findOneBy' . $this->_translit_field;

        $record_other = $this->_table->$funcName($value);

        if ($this->_translit_field) {

            $translit_value = My_Translate_Slug::slugify($value);
            $record_other_translit = $this->_table->$funcName_translit($translit_value);

            if ($this->_id) {

                if ($record_other) {
                    if ($this->_id != $record_other['id']) {
                        $this->_error(self::ERROR_RECORD_FOUND_TRANSLIT);
                        return false;
                    }
                }
                if ($record_other_translit) {
                    if ($this->_id != $record_other_translit['id']) {
                        $this->_error(self::ERROR_RECORD_FOUND_TRANSLIT);
                        return false;
                    }
                }
            } else {

                if ($record_other || $record_other_translit) {
                    $this->_error(self::ERROR_RECORD_FOUND_TRANSLIT);
                    return false;
                }
            }
        } else {

            if ($this->_id) {

                if ($record_other) {
                    if ($this->_id != $record_other['id']) {
                        $this->_error(self::ERROR_RECORD_FOUND);
                        return false;
                    }
                }
            } else {

                if ($record_other) {
                    $this->_error(self::ERROR_RECORD_FOUND);
                    return false;
                }
            }
        }

        return true;
    }

}