<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initWezoomcmsConfig()
    {
        if (!Zend_Registry::isRegistered('cmsconfig')) {
            $cms_config = $this->getOption('cmsconfig');

            $cms_config['languages_count'] = count(explode(',', $cms_config['languages_list']));
            $cms_config['lang_count'] = $cms_config['languages_count'];
            $cms_config['width'] = ceil(100 / $cms_config['languages_count']) . '%';

            $languages_list = $cms_config['languages_list'];
            $cms_config['languages_list'] = array();

            foreach (explode(',', $languages_list) as $el) {
                $cms_config['languages_list'][] = $el;
                switch ($el) {
                    case 'ru' :
                        $cms_config['lang_list'][0]['l'] = 'ru';
                        $cms_config['lang_list'][0]['p'] = '';
                        break;
                    case 'uk' :
                        $cms_config['lang_list'][1]['l'] = 'uk';
                        $cms_config['lang_list'][1]['p'] = '_uk';
                        break;
                    case 'en' :
                        $cms_config['lang_list'][2]['l'] = 'en';
                        $cms_config['lang_list'][2]['p'] = '_en';
                        break;
                }
            }

            Zend_Registry::set('cmsconfig', $cms_config);
        }
    }

    protected function _initActionHelpers()
    {
        Zend_Controller_Action_HelperBroker::addPrefix('My_Action_Helper');
        Zend_Controller_Action_HelperBroker::addPath('My/Action/Helpers', 'Helper');
    }

    protected function _initRoutes()
    {
        $this->bootstrap('FrontController');

        $router = $this->getResource('FrontController')->getRouter();

        $router->removeDefaultRoutes();

        My_Router_Multilingual::setLanguagePrefixes(array('ru', 'uk', 'en'));

        $defaultRoute = new My_Router_Multilingual(
            ':controller/:action/*',
            array('lang' => 'ru', 'module' => 'default', 'controller' => 'index', 'action' => 'index'),
            array(':controller' => '[a-z0-9-]+', ':action' => '[a-z0-9-]+')
        );

        $router->addRoute('default', $defaultRoute);

        //Точка входу в админку
        $router->addRoute(
            'admin',
            new Zend_Controller_Router_Route(
                'admin',
                array(
                     'lang' => 'ru',
                     'controller' => 'admin',
                     'action' => 'index',
                )
            ));

        //Add
        $router->addRoute(
            'add',
            new Zend_Controller_Router_Route(
                ':controller/add/:galleryid',
                array(
                     'lang' => 'ru',
                     'action' => 'add',
                     'galleryid' => ''
                ),
                array(
                     'controller' => '[a-z]+',
                     'galleryid' => '\d+'
                )
            ));

        //Add_Page_type
        $router->addRoute(
            'add_page_type',
            new Zend_Controller_Router_Route(
                ':controller/add/type/:type',
                array(
                     'action' => 'add',
                     'type' => 0
                ),
                array(
                     'controller' => '[a-z]+',
                     'type' => '\d+'
                )
            ));

        //Page
        $router->addRoute(
            'page',
            new My_Router_Multilingual(
                'page/:slug',
                array(
                     'lang' => 'ru',
                     'controller' => 'pages',
                     'action' => 'index'
                ),
                array(
                     'slug' => '[a-z0-9-]+'
                )));

        //page-name.html
        $router->addRoute(
            'page_html',
            new Zend_Controller_Router_Route_Regex(
                '([a-z0-9_-]+)\.html',
                array(
                     'controller' => 'pages',
                     'action' => 'index',
                ),
                array(1 => 'slug'),
                '%s.html'
            ));

        //Article
        $router->addRoute(
            'article',
            new My_Router_Multilingual(
                'article/:id/:slug',
                array(
                     'lang' => 'ru',
                     'controller' => 'articles',
                     'action' => 'show',
                     'slug' => ''
                ),
                array(
                     'id' => '\d+',
                     'slug' => '[a-z0-9-]+'
                )));

        //News
        $router->addRoute(
            'newspost',
            new My_Router_Multilingual(
                'newspost/:id/:slug',
                array(
                     'lang' => 'ru',
                     'controller' => 'news',
                     'action' => 'show',
                     'slug' => ''
                ),
                array(
                     'id' => '\d+',
                     'slug' => '[a-z0-9-]+'
                )));

        //Gallery. Show
        $router->addRoute(
            'galleryshow',
            new My_Router_Multilingual(
                'gallery/:id',
                array(
                     'lang' => 'ru',
                     'controller' => 'gallery',
                     'action' => 'show',
                ),
                array(
                     'id' => '\d+',
                )));

        //Contacts
        $router->addRoute(
            'contacts',
            new My_Router_Multilingual(
                'contacts',
                array(
                     'lang' => 'ru',
                     'controller' => 'pages',
                     'action' => 'contacts'
                )));

        //Contacts-sended
        $router->addRoute(
            'contacts-sended',
            new My_Router_Multilingual(
                'contacts-sended',
                array(
                     'lang' => 'ru',
                     'controller' => 'pages',
                     'action' => 'contacts-sended'
                )));

        //Shop
        $router->addRoute(
            'shop_catalog',
            new My_Router_Multilingual(
                'catalog',
                array(
                     'lang' => 'ru',
                     'controller' => 'shop',
                     'action' => 'index',
                )));

        $router->addRoute(
            'shop_filter',
            new My_Router_Multilingual(
                'catalog/:filter_params/:page',
                array(
                     'lang' => 'ru',
                     'controller' => 'shop',
                     'action' => 'filter',
                     'page' => 1,
                     'filter_params' => 'all'
                ),
                array(
                     'filter_params' => '[\&=%a-z0-9-_]+',
                     'page' => '\d+'
                )
            ));

        // Shop. Product.
        $router->addRoute(
            'shop_product',
            new My_Router_Multilingual(
                ':name/:id',
                array(
                     'lang' => 'ru',
                     'controller' => 'shop',
                     'action' => 'product',
                ),
                array(
                    'name' => '[a-z0-9_-]+',
                    'id' => 'p[0-9]+'
                )
            ));


        //Search
        $router->addRoute(
            'search',
            new My_Router_Multilingual(
                'search',
                array(
                     'lang' => 'ru',
                     'controller' => 'shop',
                     'action' => 'search'
                )));
    }

    protected function _initNavigation()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->topMenu = My_Navigation_Container::topMenu();

        $view->adminPanel = My_Navigation_Admin::panel();
    }

    protected function _initModuleLoaders()
    {
        $this->bootstrap('Frontcontroller');

        $fc = $this->getResource('Frontcontroller');
        $modules = $fc->getControllerDirectory();

        foreach ($modules AS $module => $dir) {
            $moduleName = strtolower($module);
            $moduleName = str_replace(array('-', '.'), ' ', $moduleName);
            $moduleName = ucwords($moduleName);
            $moduleName = str_replace(' ', '', $moduleName);

            $loader = new Zend_Application_Module_Autoloader(array(
                                                                  'namespace' => $moduleName,
                                                                  'basePath' => realpath($dir . "/../"),
                                                             ));
        }
    }

    protected function _initZFDebug()
    {
        $zfdebugConfig = $this->getOption('zfdebug');

        //This will go off the application.ini settings
        if ($zfdebugConfig['enabled'] != 1) {
            return;
        }

        // Ensure Doctrine connection instance is present, and fetch it
        $this->bootstrap('Doctrine');
        $doctrine = $this->getResource('Doctrine');

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        if (!isset($zfdebugConfig['plugins'])) {
            $options = array(
                'plugins' => array('Html',
                                   'Time',
                                   'Memory',
                                   'File' => array('base_path' => APPLICATION_PATH),
                                   'Cache' => array('backend' => $bootstrap->getResource('cachemanager')
                                           ->getCache('articles')
                                           ->getBackend()),
                                   'Database' => array('adapter' => $bootstrap->getResource('db')),
                                   'ZFDebug_Controller_Plugin_Debug_Plugin_Doctrine',
                                   'Variables',
                                   'Registry',
                                   'Exception',
                )
            );
        } else {

            if (in_array('html', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Html';
            if (in_array('time', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Time';
            if (in_array('memory', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Memory';
            if (in_array('file', $zfdebugConfig['plugins']))
                $options['plugins']['File'] = array('base_path' => APPLICATION_PATH);
            if (in_array('cache', $zfdebugConfig['plugins']))
                $options['plugins']['Cache'] = array('backend' => $bootstrap->getResource('cachemanager')
                        ->getCache('articles')
                        ->getBackend());
            if (in_array('database', $zfdebugConfig['plugins']))
                $options['plugins']['Database'] = array('adapter' => $bootstrap->getResource('db'));
            if (in_array('doctrine', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'ZFDebug_Controller_Plugin_Debug_Plugin_Doctrine';
            if (in_array('variables', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Variables';
            if (in_array('registry', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Registry';
            if (in_array('exception', $zfdebugConfig['plugins']))
                $options['plugins'][] = 'Exception';
        }

        $debug = new ZFDebug_Controller_Plugin_Debug($options);

        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin($debug);
        return $debug;
    }

    protected function _initMemcached()
    {
        if (APPLICATION_ENV == 'development') {
            $oBackend = new Zend_Cache_Backend_File(array(
               'cache_dir' => __DIR__ . '/../cache',
            ));
        } else {
            $oBackend = new Zend_Cache_Backend_Memcached(
                array(
                    'servers' => array(array(
                                           'host' => MEMCACHE_HOST,
                                           'port' => MEMCACHE_PORT,
                                       )),
                ));
        }

        $oFrontend = new Zend_Cache_Core(
            array(
                 'caching' => true,
                 'cache_id_prefix' => 'new_hamamshop_ru',
                 'write_control' => true,
                 'automatic_serialization' => true,
                 'ignore_user_abort' => true,
                 'automatic_cleaning_factor' => 0
            ));

        $oCache = Zend_Cache::factory($oFrontend, $oBackend);
        Zend_Registry::set('cachemanager', $oCache);
    }

}

function myhtmlify($html)
{
    return str_replace('%3D','=',$html);
}