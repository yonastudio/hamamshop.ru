<?php

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);

define('MEMCACHE_HOST', 'localhost');
define('MEMCACHE_PORT', '11211');

define('IMG_GALLERY_ORIGINAL', '/img/original/gallery/');
define('IMG_NEWS_ORIGINAL', '/img/original/news/');
define('IMG_ARTICLES_ORIGINAL', '/img/original/articles/');
define('IMG_CATALOG_ORIGINAL', '/img/original/catalog/');

define('IMG_GALLERY_CACHE', '/img/gallery/');
define('IMG_NEWS_CACHE', '/img/news/');
define('IMG_ARTICLES_CACHE', '/img/articles/');
define('IMG_CATALOG_CACHE', '/img/catalog/');

define('JPEG_QUALITY_PREVIEW', 100);
define('JPEG_QUALITY_ORIGINAL', 100);

define('IMAGE_MIN_WIDTH', 200);
define('IMAGE_MIN_HEIGHT', 200);
define('IMAGE_MAX_WIDTH', 2000);
define('IMAGE_MAX_HEIGHT', 2000);
define('IMAGE_MAX_FILESIZE', 10000000);

define('IMAGE_EXT_ALLOWED', 'jpg, jpeg, png, gif');

define('TEMP_DIR', DOCUMENT_ROOT . '/tmp/');
define('PATH_LIBRARY', DOCUMENT_ROOT . '/../library');

define('SERVERNAME', "https://".$_SERVER['SERVER_NAME']);
define('HTTP_HOST', $_SERVER['HTTP_HOST']);

define('SECRET','j8kShK982KS290Sak');

define('MAIL_ADRESS','info@hamamshop.ru');
define('MAIL_FROM','hamamshop.ru');

define('LIQPAY_MID','i9657456873');
define('LIQPAY_SIGNATURE','arIN6o6zTT2tPjXD16Wf8coTY4kGfsIpeDtH1upw2');

define('DISCOUNT_BONUS_SUM','10000');
define('DISCOUNT_BONUS_VAL','10');