<?php

class Application_Form_User extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text','fio',array(
            'decorators' => $this->elementDecorators,
            'label' => _('ФИО'),
            'required' => true,
            'class' => 'req',
            'validators' => array($str_length)
        ));
        $this->addElement('text','email',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Email',
            'required' => true,
            'validators' => array(
                'EmailAddress',
                array('stringLength', false, array(5, 64))
            ),
            'class' => 'req',
        ));
        $this->addElement('text','phone',array(
            'decorators' => $this->elementDecorators,
            'label' => _('Телефон'),
            'required' => true,
            'class' => 'req',
            'validators' => array($str_length)
        ));
        $this->addElement('text','adress',array(
            'decorators' => $this->elementDecorators,
            'label' => _('Адрес'),
            'required' => false,
            'validators' => array($str_length)
        ));

        $this->addElement('text','discount_card',array(
            'decorators' => $this->elementDecorators,
            'label' => _('Дисконтная карта'),
            'required' => false,
            'validators' => array(array('stringLength', false, array('encoding' => 'utf-8', 'min' => '4', 'max' => '4')))
        ));

        $discount = new Default_Model_Discount();
        $this->addElement('select','discount_id',array(
            'decorators' => $this->elementDecorators,
            'multiOptions' => $this->createSelectElementArray($discount->all(),'id','title',1),
            'label' => _('Скидка'),
        ));

        $this->addElement('password', 'password_1', array(
            'label' => _('Пароль'),
            'required' => true,
            'autocomplete' => 'off',
            'class' => 'req',
            'decorators' => $this->elementDecorators,
            'autocomplete' => 'off'
        ));
        $this->addElement('password', 'password_2', array(
            'label' => _('Повторите пароль'),
            'required' => true,
            'autocomplete' => 'off',
            'class' => 'req',
            'decorators' => $this->elementDecorators,
            'autocomplete' => 'off'
        ));

        $this->setDecorators($this->formDecorators);
    }


}

