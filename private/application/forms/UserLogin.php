<?php

class Application_Form_UserLogin extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');

        $this->addElement('text','email',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Email',
            'required' => true,
            'validators' => array(
                'EmailAddress',
                array('stringLength', false, array(5, 64))
            ),
        ));

        $this->addElement('password', 'password', array(
            'label' => _('Пароль'),
            'required' => true,
            'decorators' => $this->elementDecorators,
        ));

        $this->setDecorators($this->formDecorators);
    }


}

