<?php

class Application_Form_Administrator extends My_Form_Form
{

    public function init()
    {        
        $this->addElement('text', 'login', array(
            'filters' => array('StringToLower'),
            'required' => true,
            'style' => 'width: 99%'
        ));
        $this->addElement('text', 'email', array(
            'required' => true,
            'validators' => array(
                'EmailAddress',
                array('stringLength', false, array(5, 64))
            ),
        ));
        $this->addElement('text', 'realname', array());
        $this->addElement('text', 'access_level', array(
            'required' => true,
            'autocomplete' => 'off'
        ));

        $this->addElement('password', 'password', array(
            'autocomplete' => 'off',
            'required' => true,
            'validators' => array(
                array('stringLength', false, array(6, 32))
            ),
            'style' => 'width: 99%'
        ));

        $this->removeAllDecorators();
    }

}

