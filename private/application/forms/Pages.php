<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author tos
 */
class Application_Form_Pages extends My_Form_Form
{

    public function init()
    {
        $this->setAction('')->setMethod('post');
        $param_id = $this->getAttrib('param_id');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text', 'slug', array(
            'filters' => array('StringToLower'),
            'style' => 'width: 95%',
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Pages', 'slug', $param_id)
            )
        ));

        $this->addElement('select', 'parent_id', array(
            'multiOptions' => $this->pagesArray()
        ));

        $this->addElement('text', 'title', array(
            'required' => true,
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Pages', 'title', $param_id, 'slug')
            )
        ));
        $this->addElement('text', 'title_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'description', array(
            'rows' => 2,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 2,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 2,
            'validators' => array($str_length)
        ));

        $this->addElement('text', 'keywords', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_en', array(
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'text');
        $this->addElement('textarea', 'text_uk');
        $this->addElement('textarea', 'text_en');

        $this->removeAllDecorators();
    }

    private function pagesArray()
    {
        $array = $this->createSelectElementArray('Default_Model_Pages', 'id', 'title', 1);
        $newarray = array();
        foreach ($array as $el => $val) {
            if ($el != $this->getAttrib('page_id')) {
                $newarray[$el] = $val;
            }
        }
        return $newarray;
    }

}