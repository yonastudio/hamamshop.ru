<?php

class Application_Form_UserLostPassword extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');

        $this->addElement('text','email',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Email',
            'required' => true,
            'validators' => array(
                'EmailAddress',
                array('stringLength', false, array(5, 64))
            ),
        ));

        $this->setDecorators($this->formDecorators);
    }


}

