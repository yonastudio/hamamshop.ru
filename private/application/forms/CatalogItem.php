<?php

class Application_Form_CatalogItem extends My_Form_Form
{

    public function init()
    {
        //$CB = new Default_Model_CatalogBrand();
        $this->setAttrib('id', 'form');

        $this->addElement('text', 'slug', array(
            'label' => 'Transliteration',
        ));
        $this->addElement('hidden', 'category_id');
        $this->addElement('select', 'brand_id', array(
            'label' => 'Производитель',
            'multiOptions' => $this->createSelectElementArray('Default_Model_CatalogBrand', 'id', 'title', 1),
        ));
        
        $this->addElement('text', 'title', array(
            'required' => true,
        ));
        $this->addElement('text', 'title_uk');
        $this->addElement('text', 'title_en');

        $this->addElement('textarea', 'description', array(
            'rows' => 2
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 2
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 2
        ));
        $this->addElement('text', 'keywords');
        $this->addElement('text', 'keywords_uk');
        $this->addElement('text', 'keywords_en');
        $this->addElement('textarea', 'text');
        $this->addElement('textarea', 'text_uk');
        $this->addElement('textarea', 'text_en');

        $this->removeAllDecorators();
    }

}

