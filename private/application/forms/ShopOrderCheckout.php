<?php
 /**
 * Description of ShopOrderCheckout
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Application_Form_ShopOrderCheckout extends My_Form_Form
{
    public function init()
    {
        $this->setAttrib('id','form-checkout');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text','fio',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('ФИО'),
            'required' => true,
            'class' => 'req',
            'validators' => array($str_length)
        ));
        $this->addElement('text','email',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => 'Email',
            'required' => true,
            'validators' => array(
                'EmailAddress',
                array('stringLength', false, array(5, 100))
            ),
            'class' => 'req',
        ));
        $this->addElement('text','phone',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('Телефон'),
            'required' => true,
            'class' => 'req',
            'validators' => array($str_length)
        ));
        $payment = new My_Storage_Shop_Payment();
        $this->addElement('select','payment',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('Оплата'),
            'multiOptions' => $payment->array,
            'required' => true,

        ));
        $delivery = new My_Storage_Shop_Delivery();
        $this->addElement('select','delivery',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('Доставка'),
            'multiOptions' => $delivery->array,
            'required' => true,
        ));
        $this->addElement('textarea','adress',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('Адрес доставки'),
            //'required' => true,
            //'class' => 'req',
            'rows' => 3,
            'disabled' => 'disabled'
        ));
        $this->addElement('textarea','message',array(
            'decorators' => $this->elementDecoratorsNormal,
            'placeholder' => _('Комментарий'),
            'required' => false,
            'rows' => 3
        ));

        $this->addElement('hidden','token');
        $this->getElement('token')
            ->removeDecorator('Label')
            ->removeDecorator('HtmlTag');

        $this->addElement('hidden','user_id');

        $this->getElement('user_id')
            ->removeDecorator('Label')
            ->removeDecorator('HtmlTag');

        $this->setDecorators($this->formDecorators);
    }
}
