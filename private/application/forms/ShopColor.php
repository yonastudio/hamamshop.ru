<?php

class Application_Form_ShopColor extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');

        $this->addElement('text','service_title',array(
            //'decorators' => $this->elementDecorators,
            'label' => 'Название (сервисное)',
        ));
        $this->addElement('text','title',array(
            //'decorators' => $this->elementDecorators,
            'label' => 'Название (на сайте)',
            'class' => 'text-ru',
            'required' => true
        ));
        $this->addElement('text','title_en',array(
            //'decorators' => $this->elementDecorators,
            'class' => 'text-en'
        ));
        $this->addElement('text','hex',array(
            //'decorators' => $this->elementDecorators,
            'label' => 'Цвет',
            'required' => true,
            'style' => 'width: 100px'
        ));
        //$this->addElement('hidden','model_id');
        //$this->addElement('hidden','item_id');

        //$this->setDecorators($this->formDecorators);
        $this->removeAllDecorators();
    }


}

