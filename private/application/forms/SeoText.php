<?php

class Application_Form_SeoText extends My_Form_Form
{

    public function init()
    {
        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('id', 'form');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text', 'url', array(
            'filters' => array('StringToLower'),
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'text');
        $this->addElement('textarea', 'text_uk');
        $this->addElement('textarea', 'text_en');

        $this->removeAllDecorators();
    }

}

