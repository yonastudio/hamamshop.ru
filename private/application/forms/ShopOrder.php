<?php

class Application_Form_ShopOrder extends My_Form_Form
{

    public function init()
    {
        $this->addElement('text','id',array(
            'disabled' => true
        ));
        $this->addElement('text','fio',array(
            'required' => true
        ));
        $this->addElement('text','email',array(
            'required' => true
        ));
        $this->addElement('text','phone');
        $payment = new My_Storage_Shop_Payment();
        $this->addElement('radio','payment',array(
            'multiOptions' => $payment->array,
            'decorators' => $this->tableCkeckboxDecorators
        ));
        $delivery = new My_Storage_Shop_Delivery();
        $this->addElement('radio','delivery',array(
            'multiOptions' => $delivery->array,
            'decorators' => $this->tableCkeckboxDecorators
        ));
        $this->addElement('textarea','adress',array(
            'rows' => '4'
        ));
        $this->addElement('textarea','message',array(
            'rows' => '4'
        ));
        $status = new My_Storage_Shop_OrderStatus();
        $this->addElement('radio','status',array(
            'multiOptions' => $status->array,
            'decorators' => $this->tableCkeckboxDecorators
        ));
        $payment_status = new My_Storage_Shop_PaymentStatus();
        $this->addElement('radio','payment_status',array(
            'multiOptions' => $payment_status->array,
            'decorators' => $this->tableCkeckboxDecorators
        ));
        $this->removeAllDecorators();
    }


}

