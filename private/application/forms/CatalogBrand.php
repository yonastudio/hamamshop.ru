<?php

class Application_Form_CatalogBrand extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id', 'form');

        $this->addElement('text', 'title', array(
            'required' => true,
        ));
        $this->addElement('text', 'title_uk');
        $this->addElement('text', 'title_en');

        $this->addElement('text', 'slug');

        $this->removeAllDecorators();
    }

}

