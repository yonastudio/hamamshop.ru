<?php
 /**
 * Description of ShopModel
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Application_Form_ShopModel extends My_Form_Form
{
    public function init()
    {
        $brand = new Default_Model_ShopBrand();
        $this->addElement('select','brand_id',array(
            'multiOptions' => $this->createSelectElementArray($brand->all(),'id','title',1),
            'required' => true,
        ));

        $category = new My_Category_Category();
        $this->addElement('select','category_id',array(
            'multiOptions' => $category->noRootCategoriesArray(2),
            'required' => true,
        ));

        $this->addElement('text', 'name');

        $this->addElement('text', 'title', array(
            'required' => true,
        ));
        $this->addElement('text', 'title_en');
        $this->addElement('text', 'title_uk');

        $this->addElement('textarea', 'description', array(
            'rows' => 5,
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 5,
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 5,
        ));
        $this->addElement('textarea', 'keywords', array(
            'rows' => 5,
        ));
        $this->addElement('textarea', 'keywords_en', array(
            'rows' => 5,
        ));

		$this->addElement('text', 'discount');					

        $this->removeAllDecorators();
    }
}
