<?php
 /**
 * Description of ShopModel
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Application_Form_ShopModelFilter extends My_Form_Form
{
    public function init()
    {
        $this->setMethod('get');
        $brand = new Default_Model_ShopBrand();
        $this->addElement('select','brand_id',array(
            'multiOptions' => $this->createSelectElementArray($brand->all(),'id','title',1)
        ));

        $category = new My_Category_Category();
        $this->addElement('select','category_id',array(
            'multiOptions' => $category->noRootCategoriesArray(2)
        ));
        $this->addElement('text','title');
        $this->addElement('text','model_id',array('style' => 'width:40px'));
        $this->addElement('submit','submit',array(
            'class' => 'btn',
            'value' => 'Поиск'
        ));
        $this->removeAllDecorators();
    }


}
