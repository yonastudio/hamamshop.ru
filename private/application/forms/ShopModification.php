<?php
 /**
 * Description of ShopBrand
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Application_Form_ShopModification extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');

        $this->addElement('text','title',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Название',
            'class' => 'text-ru'
        ));
        $this->addElement('text','title_en',array(
            'decorators' => $this->elementDecorators,
            'class' => 'text-en'
        ));
        $this->addElement('text','price',array(
            'required' => true,
            'filter' => array('digits'),
            'decorators' => $this->elementDecorators,
            'label' => 'Цена (напр. 125.25)'
        ));
		$this->addElement('text','old_price',array(
            'filter' => array('digits'),
            'decorators' => $this->elementDecorators,
            'label' => 'Старая цена'
        ));
        $this->addElement('text','sortorder',array(
            'filter' => array('digits'),
            'decorators' => $this->elementDecorators,
            'label' => 'Сортировка'
        ));
        /*$this->addElement('textarea','description',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Краткое описание',
            'rows' => '5'
        ));*/
        $status = new My_Storage_Shop_ProductStatus();
        $this->addElement('select','status',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Состояние',
            'multiOptions' => $status->array
        ));

        $this->addElement('hidden','model_id');
        $this->getElement('model_id')
             ->removeDecorator('Label')
             ->removeDecorator('HtmlTag');

        $this->addElement('hidden','modif_id');
        $this->getElement('modif_id')
             ->removeDecorator('Label')
             ->removeDecorator('HtmlTag');

        $this->setDecorators($this->formDecorators);
    }

}
