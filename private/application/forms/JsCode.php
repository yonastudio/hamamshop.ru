<?php

class Application_Form_JsCode extends My_Form_Form
{

    public function init()
    {
        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('id', 'form');
        $this->addElement('textarea','source',array(
            'require' => false,
            'rows' => '10',
            'cols' => '50',
            'style' => 'width: 98%',
            'label' => 'Источник',            
            'decorators' => $this->elementDecorators
        ));                
        
        $this->setDecorators($this->formDecorators);
    }


}

