<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author tos
 */
class Application_Form_Gallery extends My_Form_Form
{

    public function init()
    {
        $this->setAction('')->setMethod('post');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));
        
        $this->addElement('text', 'sortorder');
        
        $this->addElement('text', 'title', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_uk',array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            'validators' => array($str_length)
        ));
        
        $this->addElement('textarea', 'description', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        
        $this->removeAllDecorators();
    }

}

?>
