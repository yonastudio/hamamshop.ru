<?php

class Application_Form_ShopModifColor extends My_Form_Form
{

    public function init()
    {
        $this->setAttrib('id','form-modal');

        $SMC = new Default_Model_ShopColor();
        $this->addElement('select','color_id',array(
            'decorators' => $this->elementDecorators,
            'label' => 'Цвет',
            'required' => true,
            'multiOptions' => $this->createSelectElementArray($SMC->all($this->getAttrib('model_id')),'id','service_title')
        ));

        $this->addElement('hidden','modif_id');
        $this->addElement('hidden','image_id');
        $this->getElement('modif_id')->setDecorators($this->noneDecorators);
        $this->getElement('image_id')->setDecorators($this->noneDecorators);

        $this->setDecorators($this->formDecorators);
    }


}

