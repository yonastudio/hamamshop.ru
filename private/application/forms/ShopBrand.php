<?php
 /**
 * Description of ShopBrand
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Application_Form_ShopBrand extends My_Form_Form
{
      public function init()
      {
          $this->setAttrib('id', 'form');

        $this->addElement('text', 'title', array(
            'required' => true,
        ));
        $this->addElement('textarea','text');

        $this->addElement('text', 'name');

        $this->removeAllDecorators();
      }
}
