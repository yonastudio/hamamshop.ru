<?php

class Application_Form_SeoUrl extends My_Form_Form
{

    public function init()
    {
        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('id', 'form');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text', 'url', array(
            'filters' => array('StringToLower'),
            'validators' => array($str_length)
        ));


        $this->addElement('text', 'title', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            'validators' => array($str_length)
        ));
        
        $this->addElement('textarea', 'description', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 3,
            'validators' => array($str_length)
        ));
        
        $this->addElement('text', 'keywords', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_en', array(
            'validators' => array($str_length)
        ));

        $this->removeAllDecorators();
    }

}

