<?php

class Application_Form_Login extends My_Form_Form
{

    public function init()
    {
        $this->setMethod('post');

        $this->addElement('text', 'login', array(
                'filters'  => array('StringToLower'),
                'required' => true,
                'decorators' => $this->elementDecorators,
                'label' => 'Логин',
                'class' => 'span3',
        ));
        $this->addElement('password', 'password', array(
                'required' => true,
                'decorators' => $this->elementDecorators,
                'label' => 'Пароль',
                'class' => 'span3',
        ));
        $this->addElement('submit', 'submit', array(
                'ignore'   => true,
                'label'    => 'Вход',
                'class' => 'btn',
                'decorators' => $this->buttonDecorators,
        ));
        $this->setDecorators($this->formDecorators);
    }


}

