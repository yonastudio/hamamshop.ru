<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author tos
 */
class Application_Form_Galleryimage extends My_Form_Form
{

    public function init()
    {                 
        $this->addElement('file', 'image', array(
            'validators' => array(
                'extension' => array('jpg', 'jpeg', 'png', 'gif'),
                'size' => '10000000',
                'ImageSize' => array(
                    'minwidth' => IMAGE_MIN_WIDTH,
                    'maxwidth' => IMAGE_MAX_WIDTH,
                    'minheight' => IMAGE_MIN_HEIGHT,
                    'maxheight' => IMAGE_MAX_HEIGHT)
            ),
            'required' => false
        ));
        
        $this->addElement('text', 'sortorder',array('style' => 'width: 200px;'));
        $this->addElement('text', 'caption');
        $this->addElement('text', 'caption_uk');
        $this->addElement('text', 'caption_en');
        
        $this->addElement('Submit', 'submit', array(
            'label' => 'Загрузить',
            'required' => false,
            'ignore' => true,
                )
        );
        
        $this->removeAllDecorators();
    }

}

?>
