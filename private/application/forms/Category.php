<?php

/**
 * Description of Category
 *
 * @author веб-студия "WeZoom" <web@wezoom.net> http://wezoom.net
 * @author ведущий разрабочик Торош Александр <webtorua@gmail.com>
 */
class Application_Form_Category extends My_Form_Form
{

    public function init()
    {
        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('id', 'form');
        $param_id = $this->getAttrib('param_id');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '100'));
        
        $this->addElement('text', 'name', array(
            'label' => 'name',
            'filters' => array('StringToLower'),
            //'decorators' => $this->elementDecorators,
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Category', 'name', $param_id)
            )
        ));
        $this->addElement('text', 'title', array(
            'required' => true,
            'label' => 'Название',
            //'decorators' => $this->elementDecorators,
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Category', 'title', $param_id, 'name')
            )
        ));
        $this->addElement('text', 'title_uk', array(
            'label' => 'Название uk',
            //'decorators' => $this->elementDecorators,
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            //'required' => true,
            'label' => 'Название en',
            //'decorators' => $this->elementDecorators,
            'validators' => array($str_length)
        ));
        
        $this->addElement('textarea', 'description', array(
            'label' => 'Description',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('textarea', 'description_uk', array(
            'label' => 'Description_uk',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('textarea', 'description_en', array(
            'label' => 'Description_en',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        
        $this->addElement('text', 'keywords', array(
            'label' => 'Keywords',
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('text', 'keywords_uk', array(
            'label' => 'Keywords_uk',
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('text', 'keywords_en', array(
            'label' => 'Keywords_en',
                //'decorators' => $this->elementDecorators
        ));
        
        $this->addElement('textarea', 'text', array(
            'label' => 'Text',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('textarea', 'text_uk', array(
            'label' => 'Text_uk',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        $this->addElement('textarea', 'text_en', array(
            'label' => 'Text_en',
            'cols' => 50,
            'rows' => 3,
                //'decorators' => $this->elementDecorators
        ));
        
        $this->addElement('checkbox', 'hidden', array(
            'label' => 'Спрятать',
                //'decorators' => $this->ckeckboxDecorators
        ));
        //$this->setDecorators($this->formDecorators);

		$this->addElement('text','meta_title', array(
            'label' => 'Meta title',
        ));
		$this->addElement('text', 'meta_title_en', array(
            'label' => 'Meta title EN',
        ));

        $this->removeAllDecorators();
    }

}
