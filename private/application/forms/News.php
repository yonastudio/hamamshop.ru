<?php

class Application_Form_News extends My_Form_Form
{

    public function init()
    {
        $param_id = $this->getAttrib('param_id');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text', 'slug', array(
            'filters' => array('StringToLower'),
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_News', 'slug', $param_id)
            )
        ));
        $this->addElement('text', 'title', array(
            'required' => true,
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_News', 'title', $param_id, 'slug')
            )
        ));
        $this->addElement('text', 'title_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'text');
        $this->addElement('textarea', 'text_uk');
        $this->addElement('textarea', 'text_en');

        $this->addElement('text', 'created_at', array(
            'style' => 'width: 200px;'
        ));

        $this->removeAllDecorators();
    }

}

