<?php

class Application_Form_Articles extends My_Form_Form
{

    public function init()
    {
        $param_id = $this->getAttrib('param_id');
        $str_length = array('stringLength', false, array('encoding' => 'utf-8', 'max' => '255'));

        $this->addElement('text', 'slug', array(
            'style' => 'width: 99%',
            'filters' => array('StringToLower'),
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Articles', 'slug', $param_id)
            )
        ));

        $this->addElement('text', 'title', array(
            'required' => true,
            'validators' => array(
                $str_length,
                new My_Validate_NoRecordExists('Default_Model_Articles', 'title', $param_id, 'slug')
            ),
        ));
        $this->addElement('text', 'title_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'title_en', array(
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'description', array(
            'rows' => 5,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_uk', array(
            'rows' => 5,
            'validators' => array($str_length)
        ));
        $this->addElement('textarea', 'description_en', array(
            'rows' => 5,
            'validators' => array($str_length)
        ));

        $this->addElement('text', 'keywords', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_uk', array(
            'validators' => array($str_length)
        ));
        $this->addElement('text', 'keywords_en', array(
            'validators' => array($str_length)
        ));

        $this->addElement('textarea', 'text');
        $this->addElement('textarea', 'text_uk');
        $this->addElement('textarea', 'text_en');

        $this->addElement('hidden', 'category_id');

        $this->removeAllDecorators();
    }

}

