<?php
/**
 * Description of IndexController
 * 
 * @author Торош Олександр <webtorua@gmail.com>
 */
class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->getPage('index');
        $this->view->headScript()->appendFile($this->view->compress('/lib/nivo-slider/jquery.nivo.slider.pack.js'));
        $this->view->headLink()->appendStylesheet($this->view->compress('/lib/nivo-slider/nivo-slider.css'));
        $this->view->homepage = true;
    }

}
