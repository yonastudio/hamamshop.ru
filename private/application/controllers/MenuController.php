<?php

class MenuController extends Zend_Controller_Action
{

    public $menu;
    public $form;
    public $request;
    private $tree = array();

    public function init()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $this->menu = new My_Navigation_Editor_Menu();
        $this->form = new Application_Form_Menu();
        $this->request = $this->getRequest();
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        $config = Zend_Registry::get('cmsconfig');
        if ($config['menu_control']) {
            $menu_store = new My_Storage_Menu();
            $this->view->entries = $menu_store->array;
        }

        $title = 'Управление меню';
        $this->view->title = $title;
        $this->view->headTitle($title);
    }

    public function editAction()
    {
        $name = $this->request->getParam('name');
        if (!$name) {
            $this->_redirect('/menu/list');
        }

        $data = $this->menu->getConfig($name);
        $this->buildTree($data);
        var_dump($this->tree);
    }

    private function buildTree($arr, $level = '0')
    {
         if (count($arr)>0) {
              foreach ($arr as $el) {
                  if (is_array($el[0])) {
                      $this->addTree($el);
                      $this->buildTree($el, $level + 1);
                  }
              }
         }
    }

    private function addTree($arr)
    {
        $newArr = $this->tree;
        $newArr[] = $arr;
        $this->tree = $newArr;
    }

    public function deleteAction()
    {
        // action body
    }


}









