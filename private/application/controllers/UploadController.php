<?php

include_once __DIR__ . '/../../library/PHPthumb/ThumbLib.inc.php';

class UploadController extends Zend_Controller_Action
{

    public function init()
    {
         My_Registry::Auth();
    }

    public function indexAction()
    {
        return;
    }

    /**
     * Загрузка единичных изображений
     * @return JSON
     */
    public function imageAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $item_id = $this->_getParam('id');
        $type = $this->_getParam('type', 'articles');
        $minw = $this->_getParam('minw', 50);
        $minh = $this->_getParam('minh', 50);
        $maxw = $this->_getParam('maxw', 2000);
        $maxh = $this->_getParam('maxh', 2000);
        $msize = $this->_getParam('msize', 10000000);
        $imgw = $this->_getParam('imgw', 150);

        $upload = new Zend_File_Transfer_Adapter_Http();
        $responce = new stdClass();

        $upload->addValidator('ImageSize', false, array('minwidth' => $minw,
            'maxwidth' => $maxw,
            'minheight' => $minh,
            'maxheight' => $maxh)
        );
        $upload->addValidator('Size', false, $msize);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5($item_id . time()) . '.jpg';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            $responce->error = 'Ошибка. ' . implode($upload->getMessages());
            echo json_encode($responce);
            return;
        }

        list($img_width, $img_height, $t, $attr) = getimagesize(TEMP_DIR . $image);

        $imageFilter = new My_Image_Filter($type);

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($type == 'shop_model_color') {
            if ($img_width > $img_height) {
                $thumb->cropFromCenter($img_height);
            } else {
                $thumb->cropFromCenter($img_width);
            }
        }

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }

        $thumb->save($newImageFullpath, 'jpg');

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }

        $cleaner = new My_Image_Cleaner();
        $cleaner->clearCached($item_id, $type);

        $responce->filename = $newImage;
        $responce->width = $img_width;
        $responce->height = $img_height;
        $responce->image = $imageFilter->ShowImageHtmlView($item_id, 0, array('width' => $imgw, 'maxHeight' => $imgw,'hash' => true));

        switch ($type) {
            case 'articles' :
                $this->_enableArticle($item_id);
                break;
            case 'news' :
                $this->_enableNewsItem($item_id);
                break;
            case 'shop_brand' :
                $this->_enableShopBrand($item_id);
            case 'shop_model_color' :
                $this->_enableShopColor($item_id);
        }

        echo json_encode($responce);
    }

    /**
     * Загрузка множественных изображений
     * @return JSON
     */
    public function multiImageAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $id = $this->_getParam('id');
        $type = $this->_getParam('type', 'gallery');
        $minw = $this->_getParam('minw', 50);
        $minh = $this->_getParam('minh', 50);
        $maxw = $this->_getParam('maxw', 2000);
        $maxh = $this->_getParam('maxh', 2000);
        $msize = $this->_getParam('msize', 10000000);
        $imgw = $this->_getParam('imgw', 150);

        $upload = new Zend_File_Transfer_Adapter_Http();
        $responce = new stdClass();

        $upload->addValidator('ImageSize', false, array('minwidth' => $minw,
            'maxwidth' => $maxw,
            'minheight' => $minh,
            'maxheight' => $maxh)
        );
        $upload->addValidator('Size', false, $msize);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5(time()) . '.jpg';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            $responce->error = 'Ошибка. ' . implode($upload->getMessages());
            echo json_encode($responce);
            return;
        }

        list($img_width, $img_height, $t, $attr) = getimagesize(TEMP_DIR . $image);

        switch ($type) {
            case 'gallery' :
                $item_id = $this->_multiGallery($id);
                break;
            case 'shop_model' :
                $item_id = $this->_multiShopModel($id);
                break;
        }

        $imageFilter = new My_Image_Filter($type);

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }

        $cleaner = new My_Image_Cleaner();
        $cleaner->clearCached($item_id, $type);

        $responce->image = $imageFilter->ShowImageHtmlView($item_id, 0, array('width' => $imgw, 'hash' => true));
        $responce->id = $item_id;

        echo json_encode($responce);
    }

    private function _multiGallery($id)
    {
        $table = new Default_Model_GalleryImage();
        $table->gallery_id = $id;
        $table->save();

        $idr = $table->identifier();
        $item_id = $idr['id'];

        return $item_id;
    }

    private function _multiShopModel($id)
    {
        $si = new Default_Model_ShopImage();
        $item_id = $si->add($id);

        $table = Doctrine_Core::getTable('Default_Model_ShopModel')->find($id);
        if (!$table) {
            return;
        }
        if (!$table['image_id']) {
            $table->image_id = $item_id;
            $table->save();
        }

        return $item_id;
    }

    public function cropAction()
    {
        // action body
    }

    private function _enableArticle($item_id)
    {
        $table = Doctrine_Core::getTable('Default_Model_Articles')->find($item_id);
        if (!$table) {
            return;
        }
        $table->img_thumb = 1;
        $table->save();
    }

    private function _enableNewsItem($item_id)
    {
        $table = Doctrine_Core::getTable('Default_Model_News')->find($item_id);
        if (!$table) {
            return;
        }
        $table->img_thumb = 1;
        $table->save();
    }

    private function _enableShopBrand($item_id)
    {
        $table = Doctrine_Core::getTable('Default_Model_ShopBrand')->find($item_id);
        if (!$table) {
            return;
        }
        $table->img_thumb = 1;
        $table->save();
    }

    private function _enableShopColor($item_id)
    {
        $table = Doctrine_Core::getTable('Default_Model_ShopColor')->find($item_id);
        if (!$table) {
            return;
        }
        $table->img_thumb = 1;
        $table->save();
    }

    /**
     * @todo УБРАТЬ. Заменить на multiImageAction()
     * @return JSON
     */
    public function catalogAction()
    {
        $psi = new Default_Model_CatalogItemImage();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $portfolio_site_id = $this->_getParam('id');
        $type = 'catalog';

        $upload = new Zend_File_Transfer_Adapter_Http();
        $responce = new stdClass();

        $upload->addValidator('ImageSize', false, array('minwidth' => 200,
            'maxwidth' => 2000,
            'minheight' => 200,
            'maxheight' => 2000)
        );
        $upload->addValidator('Size', false, 10000000);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5(time()) . '.jpg';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            //$responce->error = $upload->getMessages();
            $responce->error = 'Ошибка';
            echo json_encode($responce);
            return;
        }

        list($img_width, $img_height, $t, $attr) = getimagesize(TEMP_DIR . $image);

        $psi = new Default_Model_CatalogItemImage();
        $psi->catalog_item_id = $portfolio_site_id;
        $psi->width = $img_width;
        $psi->height = $img_height;
        $psi->save();

        $idr = $psi->identifier();
        $item_id = $idr['id'];

        $imageFilter = new My_Image_Filter($type);

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        $psi->sortorder = $item_id;
        $psi->filename = $newImage;
        $psi->save();

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }

        $cleaner = new My_Image_Cleaner();
        $cleaner->clearCached($item_id, $type);

        $responce = new stdClass();

        $responce->image = $imageFilter->ShowImageHtmlView($item_id, 0, array('width' => 75, 'maxHeight' => 75, 'hash' => true));
        $responce->id = $item_id;

        echo json_encode($responce);
    }

    /**
     * @todo УБРАТЬ. Заменить на multiImageAction()
     * @return JSON
     */
    public function galleryAction()
    {
        $table = new Default_Model_GalleryImage();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $id = $this->_getParam('id');
        //$type = $this->_getParam('type');
        $type = 'gallery';

        $upload = new Zend_File_Transfer_Adapter_Http();
        $responce = new stdClass();

        $upload->addValidator('ImageSize', false, array('minwidth' => 200,
            'maxwidth' => 2000,
            'minheight' => 200,
            'maxheight' => 2000)
        );
        $upload->addValidator('Size', false, 10000000);
        $upload->addValidator('Extension', false, 'jpg, jpeg, png, gif');

        $image = md5(time()) . '.jpg';

        $upload->addFilter('Rename', array(
            'target' => TEMP_DIR . $image,
            'overwrite' => true));

        if (!$upload->receive()) {
            if (file_exists(TEMP_DIR . $image)) {
                unlink(TEMP_DIR . $image);
            }
            //$responce->error = $upload->getMessages();
            $responce->error = 'Ошибка';
            echo json_encode($responce);
            return;
        }

        list($img_width, $img_height, $t, $attr) = getimagesize(TEMP_DIR . $image);

        $table = new Default_Model_GalleryImage();
        $table->gallery_id = $id;
        $table->save();

        $idr = $table->identifier();
        $item_id = $idr['id'];

        $imageFilter = new My_Image_Filter($type);

        $newImage = $imageFilter->ImageFilepathOriginal($item_id, 0);
        $newImageFullpath = DOCUMENT_ROOT . $newImage;

        $thumb = PhpThumbFactory::create(TEMP_DIR . $image);

        if ($img_width > 800) {
            $thumb->resize(800);
            if ($img_height > 1000) {
                $thumb->adaptiveResize(800, 1000);
            }
        }
        $thumb->save($newImageFullpath, 'jpg');

        $table->sortorder = $item_id;
        $table->filename = $newImage;
        $table->save();

        if (file_exists(TEMP_DIR . $image)) {
            unlink(TEMP_DIR . $image);
        }

        $cleaner = new My_Image_Cleaner();
        $cleaner->clearCached($item_id, $type);

        $responce = new stdClass();

        $responce->image = $imageFilter->ShowImageHtmlView($item_id, 0, array('width' => 75, 'maxHeight' => 75, 'hash' => true));
        $responce->id = $item_id;

        echo json_encode($responce);
    }

}

