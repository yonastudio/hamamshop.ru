<?php

class ShopColorsController extends Zend_Controller_Action
{

    private $SMC = null;

    private $form = null;

    private $request = null;

    public function init()
    {
        $this->SMC = new Default_Model_ShopColor();
        $this->form = new Application_Form_ShopColor();
        $this->request = $this->getRequest();

        $this->_helper->Acl->checkAllowed('shop:colors');
        $this->_helper->layout()->setLayout('admin');
    }

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $item_id = $this->SMC->addItem();
        $this->form->getElement('item_id')->setValue($item_id);

        $html = $this->view->partial('shop-colors/index.phtml',
                    array(
                          'title' => 'Цвет',
                          'form' => $this->form,
                          'act' => '/shop-colors/save',
                          'item_id' => $item_id
                    ));
        $responce = new stdClass();
        $responce->html = $html;
        echo json_encode($responce);
    }

    public function listAction()
    {
        //$this->view->headLink()
        //    ->appendStylesheet('/lib/jquery.miniColors/jquery.miniColors.css');

        //$this->view->headScript()
        //        ->appendFile('/lib/jquery.miniColors/jquery.miniColors.min.js');

        $this->view->entries = $this->SMC->all();

        $this->view->setActiveMenu('adminPanel', 'Цвета');
        $this->view->headTitle('Управление цветами');
        $this->view->title = 'Управление цветами';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->view->headLink()
            ->appendStylesheet('/lib/jquery.miniColors/jquery.miniColors.css');

        $this->view->headScript()
                ->appendFile('/lib/jquery.miniColors/jquery.miniColors.min.js');

        $this->_helper->viewRenderer('edit');

        if ($this->request->isPost() &&
            $this->form->isValid($this->request->getPost())) {

            $formData = $this->form->getValues();
            $new_id = $this->SMC->addItem($formData);
            $this->_redirect('/shop-colors/edit/id/' . $new_id);
        }

        $this->view->form = $this->form;

        $this->view->setActiveMenu('adminPanel', 'Цвета');
        $this->view->headTitle('Добавление цвета');
        $this->view->title = 'Добавление цвета';

        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->view->headLink()
            ->appendStylesheet('/lib/jquery.miniColors/jquery.miniColors.css');

        $this->view->headScript()
                ->appendFile('/lib/jquery.miniColors/jquery.miniColors.min.js');

        $id = $this->request->getParam('id');

        if ($this->request->isPost() &&
            $this->form->isValid($this->request->getPost())) {

            $formData = $this->form->getValues();
            $this->SMC->saveItem($formData, $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->SMC->find($id);
        if ($data) {
            $this->form->populate($data);
        }

        $this->view->form = $this->form;
        $this->view->id = $id;

        $this->view->setActiveMenu('adminPanel', 'Цвета');
        $this->view->headTitle('Редактирование цвета');
        $this->view->title = 'Редактирование цвета';

        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    /*public function saveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $post = $this->request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $responce = new stdClass();
        if ($this->form->isValid($formData)) {
            $this->SMC->deleteOld();
            $result = $this->SMC->saveItem($formData);
            if ($result) {
                $responce->saved = 1;
            } else {
                $responce->error = "Ошибка передачи данных";
            }
        } else {
            $responce->error = "Заполните необходимые поля";
        }
        echo json_encode($responce);
    }  */

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_ShopColor')->find($id);
        if ($table) {
            $table->delete();
            echo true;
        }

    }

    public function deleteModifColorAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_ShopModifColor')->findOneById($id);
        if ($table) {
            $table->delete();
            echo true;
        }

    }

    /*public function changeImageOrderAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $first = $this->_getParam('first');
        $second = $this->_getParam('second');
        $item_id = $this->_getParam('item_id');

        $table_first = Doctrine_Core::getTable('Default_Model_ShopColor')->find($first);
        $table_second = Doctrine_Core::getTable('Default_Model_ShopColor')->find($second);

        $sortorder_first = $table_first['sortorder'];
        $sortorder_second = $table_second['sortorder'];

        $connection = Doctrine_Manager::connection();
        $connection->beginTransaction();

        $table_first->sortorder = $sortorder_second;
        $table_second->sortorder = $sortorder_first;

        $table_first->save();
        $table_second->save();

        $connection->commit();

        $q = Doctrine_Query::create()
                ->from('Default_Model_ShopColor')
                ->where('model_id = ?', $item_id)
                ->orderBy('sortorder ASC');
        $item_images = $q->fetchArray();

        $catalog_item = Doctrine_Core::getTable('Default_Model_ShopColor')->find($item_id);
        $catalog_item->image_id = $item_images[0]['id'];
        $catalog_item->save();

        echo true;
    } */

    public function modifsAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $model_id = $this->_getParam('model_id');
        $modif_id = $this->_getParam('modif_id');
        $form = new Application_Form_ShopModifColor(array('model_id' => $model_id));
        $form->getElement('modif_id')->setValue($modif_id);
        $html = $this->view->partial('shop-colors/modifs.phtml',
                    array(
                          'title' => 'Выбор цвета',
                          'form' => $form,
                          'act' => '/shop-colors/modifs-save',
                    ));
        $responce = new stdClass();
        $responce->html = $html;
        echo json_encode($responce);
    }

    public function modifsSaveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $responce = new stdClass();

        $post = $this->request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $modif_id = $formData['modif_id'];

        $modif = new Default_Model_ShopModification();
        $result = $modif->saveColor($formData, $modif_id);
        if ($result) {
            $responce->saved = 1;
        } else {
            $responce->error = "Модификация с таким именем уже содержит такой цвет. Укажите другой.";
        }

        echo json_encode($responce);
    }

    public function imagesAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $image_id = $this->_getParam('image_id');
        $imageEntity = Doctrine_Core::getTable('Default_Model_ShopImage')->find($image_id);
        $model_id = $imageEntity->model_id;

        $SMC = new Default_Model_ShopColor();
        $colors = $SMC->findByModelId($model_id);
        $options = array('' => '-');
        foreach ($colors as $color) {
            $options[$color['id']] = $color['title_en'];
        }

        $form = new Application_Form_ShopModifColor(array('model_id' => $model_id));
        $form->getElement('image_id')->setValue($image_id);
        $form->removeElement('modif_id');
        $form->getElement('color_id')->setMultiOptions($options);
        $form->getElement('color_id')->setValue($imageEntity->color_id);

        $html = $this->view->partial('shop-colors/images.phtml',
                    array(
                          'title' => 'Фото - цвет',
                          'form' => $form,
                          'act' => '/shop-colors/images-save',
                    ));
        $responce = new stdClass();
        $responce->html = $html;
        echo json_encode($responce);
    }

    public function imagesSaveAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $responce = new stdClass();

        $post = $this->request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $image_id = $formData['image_id'];
        $color_id = $formData['color_id'];

        $model = new Default_Model_ShopImage();
        $result = $model->saveImageColor($image_id, $color_id);
        if ($result) {
            $responce->saved = 1;
        } else {
            $responce->error = "Среди фотографий этого товара уже имеется такой цвет. Укажите другой.";
        }

        echo json_encode($responce);
    }

    private function _params($data)
    {
        $arr = explode('&', $data);
        $new = array();
        foreach ($arr as $el) {
            $item = explode('=', $el);
            $new[$item[0]] = $item[1];
        }
        return $new;
    }

}







