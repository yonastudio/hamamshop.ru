<?php

class UsersController extends My_Controller_Abstract
{

    public function init()
    {
        $this->request = $this->getRequest();
        $this->form = new Application_Form_User();
        $this->form->setAttrib('id','form');
        $this->form->getElement('password_1')->setRequired(false);
        $this->form->getElement('password_2')->setRequired(false);
        $this->Model = new Default_Model_User();
        $this->ModelName = 'Default_Model_User';
        $this->controller = 'users';
        $this->title = 'Пользователи';
        $this->aclResource = 'users';
    }

    public function indexAction()
    {
        $this->_redirect('/');
    }

    public function loginAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer('modal');

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $form = new Application_Form_UserLogin();

        $post = $request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $iscart = $request->getParam('iscart', 0);

        if ($post) {
            if ($form->isValid($formData)) {
                $filter = new Zend_Filter_StripTags();
                $email = $filter->filter($formData['email']);
                $password = $filter->filter($formData['password']);

                $authAdapter = new ZendX_Doctrine_Auth_Adapter(
                    Doctrine_Core::getConnectionByTableName('Default_Model_User'));
                $authAdapter->setTableName('Default_Model_User')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password_hash')
                        ->setCredentialTreatment('MD5(CONCAT(?,password_salt))')
                        ->setIdentity($email)
                        ->setCredential($password);

                $auth = Zend_Auth::getInstance();
                $auth->clearIdentity();

                $result = $auth->authenticate($authAdapter);

                if ($result->isValid()) {
                    $data = $authAdapter->getResultRowObject(null, array(
                                                                        'password_hash',
                                                                        'password_salt'));
                    $auth->getStorage()->write($data);
                    $this->view->islogined = 1;
                } else {
                    $this->view->message = '<ul class="errors" style="margin-left: 150px;"><li>' . $this->view->translate('Неправильное имя или пароль') . '</li></ul>';
                }
            }
        }

        $this->view->title = $this->view->translate('Вход');
        $this->view->act = '/' . Zend_Registry::get('lang') . '/users/login';
        $this->view->form = $form;
        $this->view->submittitle = $this->view->translate('Войти');
        $this->view->iscart = $iscart;
        $this->view->message .= '<div style="margin-left: 150px;"><a style="color: #666;" id="user-lost-password" href="#">' . $this->view->translate('Забыли пароль?') . '</a></div>';

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function registrationAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer('modal');

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $form = new Application_Form_User();
        $form->removeElement('discount_id');

        $post = $request->getParam('formData');
        $formData = $this->_params(urldecode($post));

        if ($post) {
            if ($form->isValid($formData)) {
                $users = new Default_Model_User();
                if ($users->addItem($formData)) {
                    $this->view->title = $this->view->translate('Регистрация');
                    $this->view->act = '/';
                    $this->view->form = $this->view->translate('Поздравляем, Вы успешно зарегистировались!')
                                        . '<br />'
                                        . $this->view->translate('Вы можете войти использовав свой логин и пароль, которые Вы указали при регистрации.')
                                        . '<br />'
                                        . '<a id="login" class="btn" href="#">' . $this->view->translate("Войти") . '</a>';
                    $this->view->submittitle = $this->view->translate('Подтвердить');
                    $this->view->hidesubmit = 1;

                    $mail = new My_Mail_User();
                    $mail->registrationNotiffy($formData);

                    return;
                } else {
                    $this->view->message = '<ul class="errors" style="margin-left: 150px;"><li>' . $this->view->translate('Ошибка. Пользователь с таким Email уже зарегистрирован') . '</li></ul>';
                }
            }
        }

        $this->view->title = $this->view->translate('Регистрация');
        $this->view->act = '/' . Zend_Registry::get('lang') . '/users/registration';
        $this->view->form = $form;
        $this->view->submittitle = $this->view->translate('Подтвердить');
    }

    public function profileAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer('modal');

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if ($storage && !$storage->access_level) {

            $form = new Application_Form_User();
            $form->getElement('password_1')->setRequired(false)->setAttrib('class', null);
            $form->getElement('password_2')->setRequired(false)->setAttrib('class', null);
            $form->getElement('email')->setRequired(false)->setAttrib('disabled', 'disabled')->setAttrib('class', null);
            $form->removeElement('discount_id');
            $form->removeElement('discount_card');

            $post = $request->getParam('formData');
            $formData = $this->_params(urldecode($post));

            if ($post) {
                if ($form->isValid($formData)) {
                    $users = new Default_Model_User();
                    if ($users->saveItem($formData, $storage->id)) {
                        $this->view->message = '<div style="margin-left: 150px; color:green;">' . $this->view->translate('Сохранено') . '</div>';
                    }
                }
            }

            $user = Doctrine_Core::getTable('Default_Model_User')->find($storage->id);
            if ($user) {
                $form->populate($user->toArray());
            }

            $this->view->message .= '<div style="margin-left: 150px;"><a href="#" id="user-orders-list" data-controls-modal="cart-modal" style="color: #666;">' . $this->view->translate('История заказов') . '</a></div>';
            $this->view->title = $this->view->translate('Профиль пользователя');
            $this->view->act = '/' . Zend_Registry::get('lang') . '/users/profile';
            $this->view->form = $form;
            $this->view->submittitle = $this->view->translate('Сохранить');

        } else {
            $this->_forward('login', 'users', null, array());
        }

    }

    public function ordersAction()
    {
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if ($storage && !$storage->access_level) {
            $so = new Default_Model_ShopOrder();
            $orders = $so->userOrders($storage->id);
            $this->view->orders = $orders;

            $user = Doctrine_Core::getTable('Default_Model_User')->find($storage->id);
            $user = $user->toArray();
            if ($user['discount_card']) {
                $this->view->discount = true;
            }

            //$this->view->orderModifs = new Default_Model_ShopOrderModifs();
        }

    }

    public function lostPasswordAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer('modal');

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $form = new Application_Form_UserLostPassword();

        $post = $request->getParam('formData');
        $formData = $this->_params(urldecode($post));

        if ($post) {
            if ($form->isValid($formData)) {

                $this->view->title = $this->view->translate('Восстановление пароля');
                $this->view->act = '/';
                $this->view->hidesubmit = 1;

                $email = $formData['email'];
                $user = Doctrine_Core::getTable('Default_Model_User')->findOneByEmail($email);

                if ($user) {
                    $this->view->form = $this->view->translate('На указанный email отправлены инструкции по восстановлению пароля');

                    $mail = new My_Mail_User();
                    $mail->lostPasswordInstructions($user->toArray());
                } else {
                    $this->view->form = $this->view->translate('Пользователя с таким email не существует');
                }

                return;
            }
        }

        $this->view->title = $this->view->translate('Восстановление пароля');
        $this->view->act = '/' . Zend_Registry::get('lang') . '/users/lost-password';
        $this->view->form = $form;
        $this->view->submittitle = $this->view->translate('Подтвердить');
    }

    public function lostPasswordRenewAction()
    {
        $uid = $this->_getParam('uid');
        $hash = $this->_getParam('hash');

        if (!$uid || !$hash) {
            $this->view->error = true;
            return;
        }

        $user = Doctrine_Core::getTable('Default_Model_User')->find($uid);
        $user->toArray();
        if ($user) {
            $lostpass = new My_User_LostPassword();
            $hash_true = $lostpass->getHash($user);
            if ($hash_true == $hash) {
                $newpassword = $lostpass->renewPassword($user);

                $mail = new My_Mail_User();
                $mail->renewPassword($user['email'], $newpassword);
                return;
            }
        }
        $this->view->error = true;
    }

    private function _params($data)
    {
        $arr = explode('&', $data);
        $new = array();
        foreach ($arr as $el) {
            $item = explode('=', $el);
            $new[$item[0]] = $item[1];
        }
        return $new;
    }

    /*public function listAction()
    {
        // action body
    }

    public function addAction()
    {
        // action body
    }

    public function editAction()
    {
        // action body
    }

    public function deleteAction()
    {
        // action body
    } */


}























