<?php

class ShopModelsController extends Zend_Controller_Action
{

    private $SM;
    private $SMF;
    private $SI;

    private $request;
    private $form;

    public function init()
    {
        $this->SM = new Default_Model_ShopModel();
        $this->SMF = new Default_Model_ShopModification();
        $this->SI = new Default_Model_ShopImage();

        $this->request = $this->getRequest();
        $this->form = new Application_Form_ShopModel();
    }

    public function indexAction()
    {

    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout()->setLayout('admin');
        $form = new Application_Form_ShopModelFilter();

        $params = $this->request->getParams();
        $form->populate($params);
        $this->view->paginator = $this->SM->adminList($params);

        $title = 'Товары';
        $this->view->title = $title;
        $this->view->headTitle($title);
        $this->view->form = $form;
        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout()->setLayout('admin');

        $this->view->headScript()
                ->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_model.js');

        if ($this->request->isPost() && $this->form->isValid($this->request->getPost())) {
            $id = $this->SM->addItem($this->form->getValues());
            if ($id) {
                $this->_redirect('/shop-models/edit/id/' . $id);
            }
        }

        $this->view->setActiveMenu('adminPanel', 'Товары');
        $this->view->form = $this->form;
        $this->view->headTitle('Добавление товара');
        $this->view->title = 'Добавление товара';
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout()->setLayout('admin');

        $this->view->headScript()
                ->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_model.js');

        $id = $this->request->getParam('id');
        if ($this->request->isPost() && $this->form->isValid($this->request->getPost())) {
            $this->SM->saveItem($this->form->getValues(), $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->SM->find($id);
        if ($data) {
            $this->form->populate($data);
        }
        $this->view->form = $this->form;

        $this->view->id = $id;
        $this->view->img_thumb = $data['img_thumb'];
		$this->view->data = $data;
        $this->view->form = $this->form;

        $this->view->images = $this->SI->all($id);
        $this->view->modifications = $this->SMF->all($id);


        //$this->view->colors = $colors->all();


        $this->view->setActiveMenu('adminPanel', 'Товары');
        $this->view->headTitle('Редактирование товара');
        $this->view->title = 'Редактирование товара';

        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $modifs = Doctrine_Core::getTable('Default_Model_ShopModification')->findByModelId($id);
        $model = Doctrine_Core::getTable('Default_Model_ShopModel')->find($id);

        if ($modifs) {
            $modifs->delete();
        }

        if ($model) {
            $model->delete();
        }
        
        echo true;
    }

    public function deleteImageAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');

        $table = Doctrine_Core::getTable('Default_Model_ShopImage')->find($id);
        $item_id = $table['model_id'];

        if ($table->delete()) {

            $cleaner = new My_Image_Cleaner();
            $cleaner->clearCached($id, 'shop_model');

            $model = Doctrine_Core::getTable('Default_Model_ShopModel')->find($item_id);
            if ($id == $model['image_id']) {
                $shop_image = new Default_Model_ShopImage();
                $image_id = $shop_image->mostUp($item_id);
                if ($image_id) {
                    $model->image_id = $image_id;
                } else {
                    $model->image_id = null;
                }
                $model->save();
            }

            echo true;
            return;
        }

    }

    public function changeImageOrderAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $first = $this->_getParam('first');
        $second = $this->_getParam('second');
        $item_id = $this->_getParam('item_id');

        $table_first = Doctrine_Core::getTable('Default_Model_ShopImage')->find($first);
        $table_second = Doctrine_Core::getTable('Default_Model_ShopImage')->find($second);

        $sortorder_first = $table_first['sortorder'];
        $sortorder_second = $table_second['sortorder'];

        $connection = Doctrine_Manager::connection();
        $connection->beginTransaction();

        $table_first->sortorder = $sortorder_second;
        $table_second->sortorder = $sortorder_first;

        $table_first->save();
        $table_second->save();

        $connection->commit();

        $shop_image = new Default_Model_ShopImage();
        $image_id = $shop_image->mostUp($item_id);

        $shop_model = Doctrine_Core::getTable('Default_Model_ShopModel')->find($item_id);
        if ($shop_model) {
            $shop_model->image_id = $image_id;
            $shop_model->save();
        }

        echo true;
    }


}









