<?php

class ShopBrandsController extends Zend_Controller_Action
{

    private $SB;
    private $form;
    private $request;
    private $post;

    public function init()
    {
        $this->SB = new Default_Model_ShopBrand();
        $this->form = new Application_Form_ShopBrand();
        $this->request = $this->getRequest();
        $this->post = $this->request->getPost();
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('shop:brands');
        $this->_helper->layout()->setLayout('admin');

        $this->view->entries = $this->SB->all();
        $this->view->headTitle('Список серий');
        $this->view->title = 'Список серий';
        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('shop:brands');
        $this->_helper->layout()->setLayout('admin');

        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');


        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $id = $this->SB->addItem($this->form->getValues());
            if ($id) {
                $this->_redirect('/shop-brands/edit/id/' . $id);
            }
        }

        $this->view->setActiveMenu('adminPanel', 'Бренды');
        $this->view->form = $this->form;
        $this->view->headTitle('Добавление серии');
        $this->view->title = 'Добавление серии';
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('shop:brands');
        $this->_helper->layout()->setLayout('admin');

        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');


        $id = $this->_getParam('id');
        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $this->SB->saveItem($this->form->getValues(), $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->SB->find($id);
        if ($data) {
            $this->form->populate($data);
        }
        $this->view->form = $this->form;

        $this->view->id = $id;
        $this->view->img_thumb = $data['img_thumb'];
        $this->view->setActiveMenu('adminPanel', 'Серии');
        $this->view->headTitle('Редактирование серии');
        $this->view->title = 'Редактирование серии';

        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('shop:brands');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_ShopBrand')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }


}









