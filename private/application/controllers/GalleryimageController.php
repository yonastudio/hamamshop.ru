<?php

/**
 * Description of GalleryController
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class GalleryimageController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->setActiveMenu('adminPanel', 'Галереи');
    }
    
    public function indexAction()
    {
        
    }
    
    public function showAction()
    {
        
    }
    
    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        
        $gallery_id = $this->_getParam('galleryid');
        
        $galleryimage = new Default_Model_GalleryImage();
        $entries = $galleryimage->getImagesByGalleryId($gallery_id);
        $this->view->entries = $entries;
        $this->view->gallery_id = $gallery_id;
        
        $this->view->headTitle('Список изображений');
        $this->view->title = 'Список изображений';

        $this->view->chAddOn = 1;
        $this->view->chAddOnGalleryimage = $gallery_id;
    }
    
    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        
        $form = new Application_Form_Galleryimage();
        $galleryimage = new Default_Model_GalleryImage();
        
        $request = $this->getRequest();
        $data = $request->getPost();
        
        $gallery_id = $this->_getParam('galleryid');
        
        if ($request->isPost() && $form->isValid($data)) {
            $galleryimage->addGalleryImage($data, $gallery_id);
            $this->_redirect('/galleryimage/list/galleryid/' . $gallery_id);
        }
        
        $this->view->form = $form;
        
        $this->view->headTitle('Добавление изображения');
        $this->view->title = 'Добавление изображения';
        
        $this->view->chSaveOn = 1;
    }
    
    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        
        $form = new Application_Form_Galleryimage();
        $galleryimage = new Default_Model_GalleryImage();
        
        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();
        
        if ($request->isPost() && $form->isValid($data)) {
            $galleryimage->saveGalleryImage($data, $id);
            $this->_redirect($this->view->url());
        }
        
        $imagedata = $galleryimage->getGalleryImageById($id);
        $form->populate($imagedata);
        $this->view->form = $form;
        $this->view->id = $id;
        $this->view->galleryid = $imagedata['gallery_id'];
        
        $this->view->headTitle('Редактирование изображения');
        $this->view->title = 'Редактирование изображения';
        
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }
    
    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        
        
        $table = Doctrine_Core::getTable('Default_Model_GalleryImage')->find($id);
        
        if ($table->delete()) {
            $cleaner = new My_Image_Cleaner();
            $cleaner->clearCached($id, 'gallery');
            echo true;
        }
    }
    
    public function changeorderAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        
        $first = $this->_getParam('first');
        $second = $this->_getParam('second');
                
        $table_first = Doctrine_Core::getTable('Default_Model_GalleryImage')->find($first);
        $table_second = Doctrine_Core::getTable('Default_Model_GalleryImage')->find($second);
        
        $sortorder_first = $table_first['sortorder'];
        $sortorder_second = $table_second['sortorder'];
        
        $connection = Doctrine_Manager::connection();
        $connection->beginTransaction();                
        
        $table_first->sortorder = $sortorder_second;
        $table_second->sortorder = $sortorder_first;
                
        $table_first->save();     
        $table_second->save();
        
        $connection->commit();
        
        echo true;
    }
    
}
