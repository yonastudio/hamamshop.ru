<?php

/**
 * Description of GalleryController
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class GalleryController extends Zend_Controller_Action
{

    public function init()
    {
        
    }

    public function indexAction()
    {
        $gallery = new Default_Model_Gallery();
        $entries = $gallery->getGalleriesPreviews();

        $this->view->entries = $entries;
    }

    public function showAction()
    {
        $id = $this->_getParam('id', 1);
        $imageid = $this->_getParam('imageid');

        $this->view->headScript()
                ->appendFile('/lib/pirobox/pirobox.min.js');

        $this->view->headLink()
                ->appendStylesheet('/lib/pirobox/css/style.css');

        $gallery = new Default_Model_Gallery();
        $gallerydata = $gallery->getGalleryById($id);

        $galleryimage = new Default_Model_GalleryImage();
        $images = $galleryimage->getImagesByGalleryId($id, 5);
        if (!$imageid) {
            $image = $images[0]['id'];
        } else {
            $image = $imageid;
        }
        $this->view->images = $images;
        $this->view->image = $image;
        $this->view->desc = $gallerydata['description'];
        $this->view->galleryname = $gallerydata['title'];

        $this->view->setActiveMenu('topMenu', 'Галерея');
    }

    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $gallery = new Default_Model_Gallery();

        $galleries = $gallery->findAll();
        
        $this->view->entries = $galleries;

        $this->view->headTitle('Список галерей');
        $this->view->title = 'Список галерей';

        $this->view->ch(array('chAddOn' => '1'));
    }

    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $form = new Application_Form_Gallery();
        $gallery = new Default_Model_Gallery();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $gallery->saveGallery($formData);
            $this->_redirect('/gallery/edit/id/' . $new_id);
        }

        $this->view->form = $form;

        $this->view->title = 'Добавление галереи';
        $this->view->headTitle('Добавление галереи');

        $this->view->ch(array('chSaveOn' => '1'));
        $this->view->setActiveMenu('adminPanel', 'Галереи');
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $form = new Application_Form_Gallery();
        $gallery = new Default_Model_Gallery();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();

        $galleryimage = new Default_Model_GalleryImage();
        $galleryimages = $galleryimage->getImagesByGalleryId($id);

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $gallery->saveGallery($formData, $id);
            $this->_redirect($this->view->url());
        }

        $row = $gallery->getGalleryById($id);
        $form->populate($row);

        $this->view->galleryimages = $galleryimages;
        $this->view->form = $form;
        $this->view->id = $id;
        $this->view->count = $row['count'];

        $this->view->headTitle('Редактирование галереи');
        $this->view->title = 'Редактирование галереи';

        $this->view->ch(array('chSaveOn' => '1', 'chAddOn' => '1', 'chDelOn' => '1'));
        $this->view->setActiveMenu('adminPanel', 'Галереи');
    }

    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_Gallery')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}
