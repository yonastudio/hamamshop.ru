<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        if (!$errors) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $this->getResponse()->setHttpResponseCode(404);
                break;
            default:                
                $this->getResponse()->setHttpResponseCode(500);
                break;
        }
        
        $this->_helper->layout->disableLayout();

        if ($log = $this->getLog()) {
            $log->crit($_SERVER['REQUEST_URI'] . ' ' . $this->view->message . ' ' . $errors->exception->getMessage());
        }

        if (APPLICATION_ENV == 'development') {
            $this->view->exception = $errors->exception;
        } else {
            $this->_helper->ErrorHandler->error(404);
        }

        //if ($this->getInvokeArg('displayExceptions') == true) {
        //    $this->view->exception = $errors->exception;
        //} else {

        //}

        $this->view->request = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

