<?php

class ShopOrderController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Отображение корзины покупателя. Пересчет
     * количества товара при наличии параметра
     * "counts".
     *
     */
    public function indexAction()
    {
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $session_id = Zend_Session::getId();

        $delete = $request->getParam('del');
        $id = $request->getParam('id');
        if ($delete && $id) {
            $item = Doctrine_Core::getTable('Default_Model_ShopCart')->findOneByIdAndSessionId($id, $session_id);
            if ($item) {
                $item->delete();
            }

            //$this->_helper->Cacher->remove('Cart_sum_' . $session_id);
            //$this->_helper->Cacher->remove('Cart_count_' . $session_id);
        }

        $counts = $request->getParam('counts');
        if ($counts) {
            $filter = new My_Shop_Filter();
            $params = $filter->getParams($counts);
            if (count($params) > 0) {
                foreach ($params as $el => $val) {
                    $item = Doctrine_Core::getTable('Default_Model_ShopCart')->findOneByIdAndSessionId($el, $session_id);
                    if ($item) {
                        if ($val == '0') {
                            $item->delete();
                        } else {
                            if ($item['count'] != $val) {
                                $item->count = $val;
                                $item->save();
                            }
                        }
                    }
                }
            }
        }

        $cart = new Default_Model_ShopCart();
        $this->view->entries = $cart->all($session_id);
    }

    public function addToCartAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $data = $request->getParams();
        $responce = new stdClass();
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if (!$auth->access_level) {

            $session_id = Zend_Session::getId();

            $this->_helper->Cacher->remove('Cart_sum_' . $session_id);
            $this->_helper->Cacher->remove('Cart_count_' . $session_id);

            $cart = new Default_Model_ShopCart();
            $cart->addItem($data, $session_id);

            $sumAndCount = $cart->sumAndCount($session_id);

            $this->_helper->Cacher->set($sumAndCount['sum'], 'Cart_sum_' . $session_id, 3600);
            $this->_helper->Cacher->set($sumAndCount['count'], 'Cart_count_' . $session_id, 3600);

            $responce->sum = ($sumAndCount['sum']) ? $sumAndCount['sum'] : '0';
            $responce->count = ($sumAndCount['count']) ? $sumAndCount['count'] : '0.00';
            $responce->success = 1;

        } else {
            $responce->error = "Нельзя совершать покупки, находясь в админке.";
        }
        echo json_encode($responce);
    }

    public function refreshCartAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $session_id = Zend_Session::getId();

        $cart = new Default_Model_ShopCart();
        $sumAndCount = $cart->sumAndCount($session_id);

        $responce = new stdClass();
        $responce->sum = ($sumAndCount['sum']) ? $sumAndCount['sum'] : '0';
        $responce->count = ($sumAndCount['count']) ? $sumAndCount['count'] : 0;
        $responce->success = 1;

        echo json_encode($responce);
    }

    public function orderAction()
    {
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $session_id = Zend_Session::getId();

        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if ($storage->access_level) {
            $this->view->message = '<ul class="errors"><li>Нельзя совершать покупки, находясь в админке.</li></ul>';
            return;
        }

        $cart = new Default_Model_ShopCart();
        $this->view->entries = $cart->all($session_id);

        $this->view->title = $this->view->translate("Оформление заказа");

        $form = new Application_Form_ShopOrderCheckout();
        $form->getElement('token')->setValue($this->_getToken());

        if ($storage && !$storage->access_level) {
            $form->getElement('user_id')->setValue($storage->id);
        }

        if ($storage && !$storage->access_level) {
            $user = Doctrine_Core::getTable('Default_Model_User')->find($storage->id);
            $user = $user->toArray();

            $form->populate($user);
            $this->view->islogined = 1;
        }

        $this->view->form = $form;

    }

    public function orderSubmitAction()
    {
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $form = new Application_Form_ShopOrderCheckout();
        $session_id = Zend_Session::getId();

        $storage = Zend_Auth::getInstance()->getStorage()->read();

        $post = $request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        if (!$formData) {
            return $this->json(array('error' => 'Не удалось отправить данные из формы. Попробуйте еще раз'));
        }

        switch ($formData['delivery']) {
            case 1 :
                $form->getElement('adress')->setRequired(false);
                break;
            case 2 :
                $form->getElement('adress')->setAttrib('disabled', null);
                break;
        }

        $token = $formData['token'];
        if ($token != $this->_getToken()) {
            return $this->json(array('error' => 'Ошибка безопасности. Попробуйте еще раз'));
        }

        if (!$form->isValid($formData)) {
            return $this->json(array('errors' => array(
                'fio' => array_values($form->getElement('fio')->getMessages())[0],
                'email' => array_values($form->getElement('email')->getMessages())[0],
                'phone' => array_values($form->getElement('phone')->getMessages())[0],
                'payment' => array_values($form->getElement('payment')->getMessages())[0],
                'delivery' => array_values($form->getElement('delivery')->getMessages())[0],
                'adress' => array_values($form->getElement('adress')->getMessages())[0],
                'message' => array_values($form->getElement('message')->getMessages())[0],
            )));
        }

        if ($storage) {
            $user = new Default_Model_User();
            $data = $user->findOneWithDiscount($storage->id);
            $formData['discount_id'] = $data['discount_id'];
        }

        $order = new Default_Model_ShopOrder();
        $order_id = $order->addItem($formData);

        $cart = new Default_Model_ShopCart();
        $all = $cart->all($session_id);

        $modifs = new Default_Model_ShopOrderModifs();
        foreach ($all as $el) {
            $modifs->addItem($el, $order_id);
        }

        $cart->deleteBySession(Zend_Session::getId());
        
        $lang = Zend_Registry::get('lang');
        $hash = $this->_getHash($formData['email'], $order_id);

        if ($formData['payment'] == '2') {
            $this->json(array(
                'success' => true,
                'redirect' => '/' . $lang . '/shop-order/payment?order_id=' . $order_id . '&hash=' . $hash
            ));
        } else {
            $this->json(array(
                'success' => true,
                'redirect' => '/' . $lang . '/shop-order/order-submitted?order_id=' . $order_id . '&hash=' . $hash
            ));
        }

    }

    private function json($contents)
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_response->setHeader('Content-Type', 'applicatioon/json');
        $this->_response->setBody(json_encode($contents));
    }

    public function orderSubmittedAction()
    {
        $request = $this->getRequest();
        $order_id = $request->getParam('order_id');
        $hash = $request->getParam('hash');

        $table = Doctrine_Core::getTable('Default_Model_ShopOrder')->find($order_id);
        if ($table) {
            if ($hash == $this->_getHash($table['email'], $order_id)) {

                //Если при оформлении заказа человек выбрал оплату через Visa/MasterCard,
                //то после подтверждения, идет переадресация на страницу оплаты.
                if ($table['payment'] == 2) {
                    $lang = Zend_Registry::get('lang');
                    $this->_redirect('/' . $lang . '/shop-order/payment?order_id=' . $order_id . '&hash=' . $hash);
                } else {
                    $table->status = 1;
                    $table->save();
                    $mail = new My_Mail_OrderSubmit();
                    $mail->sendNotification($table['email'], $order_id);
                }
                return;
            }
        }
        $this->view->error = true;
    }

    /**
     * Страница с формой оплаты через Visa/MasterCard
     *
     *
     */
    public function paymentAction()
    {
        $request = $this->getRequest();
        $order_id = $request->getParam('order_id');
        $hash = $request->getParam('hash');

        $order = new Default_Model_ShopOrder();
        $data = $order->find($order_id);
        if (!$data) {
            $this->view->error = true;
        }

        if ($hash == $this->_getHash($data['email'], $order_id)) {
            if ($data['status'] == '0') {
                $lang = Zend_Registry::get('lang');
                //$payment_link = '/' . $lang . '/shop-order/payment?order_id=' . $order_id . '&hash=' . $hash;
                $mail = new My_Mail_OrderSubmit();
                $mail->sendNotification($data['email'], $order_id);

                $table = Doctrine_Core::getTable('Default_Model_ShopOrder')->find($order_id);
                $table->status = 1;
                $table->save();
                $this->_redirect('/' . $lang . '/shop-order/payment?order_id=' . $order_id . '&hash=' . $hash);
            }
            if ($data && $data['status'] == '1' && $data['payment_status'] == '0') {
                $payment_order = new Default_Model_ShopPaymentOrder();
                $payment_order_id = $payment_order->addItem($order_id);

                $sumprice = $this->view->shopUserDiscount($data['sumprice'], true);

                $liqpay = new My_Shop_Payment_LiqPay();
                $form = $liqpay->getForm($payment_order_id, $order_id, $sumprice);
                $this->view->form = $form;

                return;
            }
        }
        $this->view->error = true;
    }

    /**
     * После оплаты через LiqPay идет переадресация
     * на эту страницу с передачей POST-запроса
     *
     *
     */
    public function paymentSuccessAction()
    {
        $request = $this->getRequest();

        $payment_order_id = $this->_getParam('poid');
        if (!$payment_order_id) {
            $this->view->error = true;
        }

        $table = Doctrine_Core::getTable('Default_Model_ShopPaymentOrder')->find($payment_order_id);
        if (!$table) {
            $this->view->error = true;
            return;
        }
        $table->toArray();

        $hash = $this->_getParam('hash');
        $order_id = $table['order_id'];
        $order_id_hash = substr(md5($order_id), 0, 8);
        if ($order_id_hash != $hash) {
            $this->view->error = true;
            return;
        }

        $operation_xml = $request->getParam('operation_xml');
        $signature = $request->getParam('signature');

        if ($operation_xml && $signature) {

            ob_start();
            print_r($request->getParams());
            $params = ob_get_contents();
            ob_end_clean();

            $f = fopen(DOCUMENT_ROOT . "/payment_server_responce.log", 'a+');
            fputs($f, date("Y-m-d H:i:s") . ' ' . $params . "\n\n");

            $xml_decoded = base64_decode($operation_xml);
            $sign = base64_encode(sha1($signature . $operation_xml . $signature, 1));

            if ($signature == $sign) {
                file_put_contents(DOCUMENT_ROOT . "/payment_signes.log", "TRUE");
            } else {
                file_put_contents(DOCUMENT_ROOT . "/payment_signes.log", "FALSE");
            }

            file_put_contents(DOCUMENT_ROOT . "/payment.log", $xml_decoded);

            $xml_callback = file_get_contents(DOCUMENT_ROOT . "/payment.log", $xml_decoded);
            $callback_data = simplexml_load_string($xml_callback);
            if ($callback_data->order_id) {
                $payment = new Default_Model_ShopPaymentOrder();
                $payment->saveItemFromCallback($callback_data->order_id, $callback_data);

                if ($callback_data->status == 'success') {
                    $so_table = Doctrine_Core::getTable('Default_Model_ShopOrder')->find($order_id);
                    if ($so_table['payment_status'] == '0') {
                        $so_table->payment_status = 1;
                        $so_table->save();

                        $mail = new My_Mail_OrderPayed();
                        $mail->sendNotification($so_table->toArray());
                    }
                }
            }
        }

        $po = new Default_Model_ShopPaymentOrder();
        $po_data = $po->find($payment_order_id);

        $so = new Default_Model_ShopOrder();
        $so_data = $so->find($po_data['order_id']);

        if ($po_data['status'] == 'success') {
            $this->view->order_number = $order_id;
        } else {
            $liqpay = new My_Shop_Payment_LiqPay();
            $payment_order = new Default_Model_ShopPaymentOrder();

            $new_payment_order_id = $payment_order->addItem($order_id);
            $form = $liqpay->getForm($new_payment_order_id, $so_data['id'], $so_data['sumprice']);

            $this->view->form = $form;
            $this->view->error = true;
        }

    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('shop:orders');

        $this->_helper->layout()->setLayout('admin');
        $page = $this->_getParam('page', 1);

        $so = new Default_Model_ShopOrder();
        $this->view->paginator = $so->adminPaginator($page);

        $title = "Список заказов";
        $this->view->title = $title;
        $this->view->headTitle($title);
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('shop:orders');
        $this->_helper->layout()->setLayout('admin');

        $form = new Application_Form_ShopOrder();
        $request = $this->getRequest();
        $post = $request->getPost();
        $id = $request->getParam('id');

        $so = new Default_Model_ShopOrder();
        if ($request->isPost() && $form->isValid($post)) {
            $formData = $form->getValues();
            $so->saveItem($formData, $id);
            $this->_redirect($this->view->url());
        }

        $order = new Default_Model_ShopOrder();
        $data = $order->find($id);
        $data = $data->toArray();
        if ($data['discount_card']) {
            $this->view->discount_card = $data['discount_card'];
        }
        if ($data) {
            $form->populate($data);
        }
        $this->view->form = $form;
        $this->view->discount_val = $data['discount_val'];

        $som = new Default_Model_ShopOrderModifs();
        $this->view->entries = $som->findAllByOrderId($id);

        $title = "Заказ №" . $id;
        $this->view->title = $title;
        $this->view->headTitle($title);

        $this->view->chSaveOn = 1;
        $this->view->setActiveMenu('adminPanel', 'Заказы');
    }

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $id = $request->getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_ShopOrderModifs')->find($id);
        if ($table) {
            $table->delete();
            echo true;
        }
    }

    private function _getToken()
    {
        $session_id = Zend_Session::getId();
        $uagent = $_SERVER['HTTP_USER_AGENT'];
        $ip = $_SERVER['HTTP_REFERER'];
        $token = md5(base64_decode($uagent) . sha1($session_id) . md5($ip));
        $token = substr($token, 0, 11);
        return $token;
    }

    private function _getHash($email, $order_id)
    {
        $hash = md5($email . SECRET . $order_id);
        $hash = substr($hash, 0, 14);
        return $hash;
    }

    private function _params($data)
    {
        $arr = explode('&', $data);
        $new = array();
        foreach ($arr as $el) {
            $item = explode('=', $el);
            $new[$item[0]] = $item[1];
        }
        return $new;
    }

}







