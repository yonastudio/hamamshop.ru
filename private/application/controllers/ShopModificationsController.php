<?php

class ShopModificationsController extends Zend_Controller_Action
{

    private $SM;
    private $request;
    private $form;

    public function init()
    {
        $this->SM = new Default_Model_ShopModification();
        $this->request = $this->getRequest();
        $this->form = new Application_Form_ShopModification();
    }

    public function indexAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->request->isXmlHttpRequest()) {
            return;
        }

        $type = $this->_getParam('type');
        $model_id = $this->_getParam('model_id');

        $this->form->getElement('model_id')->setValue($model_id);

        switch ($type) {
            case 'many' :
                $html = $this->view->partial('modal.phtml',
                    array(
                          'title' => 'Добавление модификации',
                          'form' => $this->form,
                          'act' => '/shop-modifications/add'
                    ));
                break;
        }

        $responce = new stdClass();
        $responce->html = $html;
        echo json_encode($responce);
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->request->isXmlHttpRequest()) {
            return;
        }

        $post = $this->request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $responce = new stdClass();
        if ($this->form->isValid($formData)) {
            $this->SM->addItem($formData);
            $responce->saved = 1;
        } else {
            $responce->error = "Заполните необходимые поля";
        }
        echo json_encode($responce);
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->request->isXmlHttpRequest()) {
            return;
        }

        $modif_id = $this->_getParam('modif_id');
        $data = $this->SM->find($modif_id);
        if ($data) {
            $data['modif_id'] = $modif_id;
            $this->form->populate($data);
        } else {
            return;
        }

        $html = $this->view->partial('modal.phtml',
            array(
                  'title' => 'Редактирование модификации',
                  'form' => $this->form,
                  'act' => '/shop-modifications/edit-submit'
            ));

        $responce = new stdClass();
        $responce->html = $html;
        echo json_encode($responce);
    }

    public function editSubmitAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->request->isXmlHttpRequest()) {
            return;
        }

        $post = $this->request->getParam('formData');
        $formData = $this->_params(urldecode($post));
        $responce = new stdClass();
        if ($this->form->isValid($formData)) {
            $this->SM->saveItem($formData, $formData['modif_id']);
            $responce->saved = 1;
        } else {
            $responce->error = "Заполните необходимые поля";
        }
        echo json_encode($responce);
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('shop:products');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $modif = Doctrine_Core::getTable('Default_Model_ShopModification')->find($id);

        if ($modif) {
            $modif->delete();
        }

        echo true;
    }

    private function _params($data)
    {
        $arr = explode('&', $data);
        $new = array();
        foreach ($arr as $el) {
            $item = explode('=', $el);
            $new[$item[0]] = $item[1];
        }
        return $new;
    }

}









