<?php

class NewsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $news = new Default_Model_News();
        $query = $news->paginatorQuery();

        $page = $this->_getParam('page');

        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $this->view->paginator = $paginator;
		$this->view->disable_eng = true;
    }

    public function showAction()
    {
        $id = $this->_getParam('id');
        $this->view->getNew($id);
        
        $this->view->setActiveMenu('topMenu', 'Новости');
		$this->view->disable_eng = true;
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('news');
        $this->_helper->layout()->setLayout('admin');

        $news = Doctrine_Core::getTable('Default_Model_News')->findAll();
        $this->view->entries = array_reverse($news->toArray());

        $this->view->headTitle('Список новостей');
        $this->view->title = 'Список новостей';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('news');
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $news = new Default_Model_News();
        $form = new Application_Form_News();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $news->saveNew($formData);
            $this->_redirect('/news/edit/id/' . $new_id);
        }

        $this->view->headTitle('Добавление новости');
        $this->view->title = 'Добавление новости';

        $this->view->form = $form;

        $this->view->setActiveMenu('adminPanel', 'Новости');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('news');
        $this->_helper->layout()->setLayout('admin');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $news = new Default_Model_News();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $post = $request->getPost();
        
        $form = new Application_Form_News(array('attribs' => array('param_id' => $id)));

        if ($request->isPost() && $form->isValid($post)) {
            $formData = $form->getValues();
            $news->saveNew($formData, $id);
            $this->_redirect($this->view->url());
        }

        $data = $news->getNewById($id);
        $form->populate($data);
        $this->view->form = $form;

        $this->view->headTitle('Редактирование новости');
        $this->view->title = 'Редактирование новости';
        $this->view->id = $id;
        $this->view->img_thumb = $data['img_thumb'];
        
        $this->view->setActiveMenu('adminPanel', 'Новости');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('news');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_News')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}

