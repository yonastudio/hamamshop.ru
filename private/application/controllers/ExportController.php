<?php

/**
 * ExportController
 * @copyright Copyright (c) 2011 - 2013 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */
class ExportController extends Zend_Controller_Action
{

    public function wikimartXmlAction()
    {
        $this->_helper->layout->disableLayout();

        $filter = new My_Shop_Filter();
        $this->view->products = $filter->filter()->fetchArray();

    }

}