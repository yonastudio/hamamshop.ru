<?php

/**
 *
 * @author WeZoom
 */
class CategoriesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->layout = Zend_Layout::getMvcInstance();
        $this->error_handler = new My_Error_Handler();
    }

    /**
     * Адмінка. Відображення списку категорій в ієрархії
     */
    public function indexAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $treeObject = Doctrine_Core::getTable('Default_Model_Category')->getTree();
        $rootColumnName = $treeObject->getAttribute('rootColumnName');
        $output = '';
        foreach ($treeObject->fetchRoots() as $root) {
            $options = array(
                'root_id' => $root->$rootColumnName
            );
            foreach ($treeObject->fetchTree($options) as $node) {
                $hidden = ($node['hidden'] == 1) ? ' style="color:grey"' : '';
                if ($node['level'] != 0) {
                    $output .= str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $node['level']) . '- <a href="/categories/edit/id/' . $node['id'] . '"' . $hidden . '>' . $node['title'] . "</a><br/>";
                } else {
                    $output .= str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $node['level']) . '<b' . $hidden . '>' . $node['title'] . "</b><br/>";
                }
            }
        }
        $this->view->content = $output;
        $this->view->chAddOn = 1;
        $this->view->headTitle('Список категорий');
        $this->view->title = 'Список категорий';
    }

    /**
     * Додавання кореневої категорії.
     */
    public function insertRootAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        return; // выключено чтобы нельзя было добавить больше Root
        $name = strtolower($this->_request->getParam('name'));
        if ($name) {
            $category = new Default_Model_Category();
            $category->name = $name;
            $category->hidden = 0;
            $category->save();

            $treeObject = Doctrine_Core::getTable('Default_Model_Category')->getTree();
            $treeObject->createRoot($category);

            $cat = Doctrine_Core::getTable('Default_Model_Category')->findOneByName($name);
            $cat->sortorder = $cat['id'];
            $cat->save();
        }
        $object = new My_Category_Category();
        $object->cleanCached();
        $this->_redirect('/categories');
    }

    /**
     * Адмінка. Додавання категорії
     * @return <type>
     */
    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

	$this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $category = My_Registry::Category();
        $form = new Application_Form_Category();
        $form->removeElement('name');
        $form->removeElement('description');
        $form->removeElement('description_uk');
        $form->removeElement('description_en');
        $form->removeElement('keywords');
        $form->removeElement('keywords_uk');
        $form->removeElement('keywords_en');
        $form->removeElement('text');
        $form->removeElement('text_uk');
        $form->removeElement('text_en');
        $form->addElement('select', 'parent', array(
            'required' => true,
            'multiOptions' => $category->getCategoryFullListArrayForFormSelect(),
        ));

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $result = $category->save($data);
            if ($result) {
                $this->_redirect('/categories/edit/id/' . $result);
            } else {
                $this->_helper->viewRenderer->setNoRender();
                return;
            }
        }

        $this->view->form = $form;
        $this->view->headTitle('Добавление категории');
        $this->view->title = 'Добавление категории';

        $this->view->setActiveMenu('adminPanel', 'Управление');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

	$this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');
        
        $category = My_Registry::Category();

        $request = $this->getRequest();
        $data = $request->getPost();
        $id = $request->getParam('id');
        
        $form = new Application_Form_Category(array('attribs' => array('param_id' => $id)));

        if ($request->isPost() && $form->isValid($data)) {
            $result = $category->save($data, $id);
            if ($result) {
                $this->_redirect($this->view->url());         
            } else {   
                $this->_helper->viewRenderer->setNoRender();
                return;
            }
        }

        $row = $category->populate($id);
        $form->populate($row);

        $parent = $category->getCategoryParentById($id);
        $this->view->parent_category = $category->getCategoryTitleTrById($parent['id']);

        $this->view->form = $form;
        
        $this->view->headTitle('Редактирование категории');
        $this->view->title = 'Редактирование категории';
        
        $this->view->setActiveMenu('adminPanel', 'Управление');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');

        $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
        if ($category->getNode()->hasChildren()) {
            echo "Категория имеет подкатегории. Удалите сначала их.";
            return;
        }
        if ($category->getNode()->delete()) {
            echo true;
        }
    }

    public function clearcacheAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer->setNoRender();

        $category = new My_Category_Category();
        $category->cleanCached();

        echo "Кеш категорий очищен.";
    }

    public function listAction()
    {
        $this->_forward('index');
    }

    /**
     * XmlHttpRequest.
     * Адмінка. Повертає масив рубрик для певного типу матеріалу.
     */
    public function categorylistAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $category = new My_Category_Category();
        $category_name = $this->_request->getParam('category_name');
        if ($category_name) {
            $category_id = $category->getCategoryIdByName($category_name);
            if (empty($category_id)) {
                throw new Zend_Exception('incorrect name category!!!');
            }
            $find = Doctrine_Core::getTable('Default_Model_Category')->find($category_id);
            if (!$find) {
                throw new Zend_Exception('no find category!!!');
            }
            $nodesRubric = $find->getNode();
            $descendants = $nodesRubric->getDescendants();
        } else {
            $treeObject = Doctrine_Core::getTable('Default_Model_Category')->getTree();
            $rootColumnName = $treeObject->getAttribute('rootColumnName');
            $options = array(
                'root_id' => $root->$rootColumnName
            );
            $descendants = $treeObject->fetchTree($options);
        }
        $output = '';
        foreach ($descendants as $node) {
            if (!$node['hidden']) {
                $output .= str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $node['level']) . '<span node="' . $node['id'] . '">' . $node['name'] . '</span><br/>';
            }
            $output .= '<div class="close"></div>';
        }
        echo $output;
    }

    public function processAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $process = new My_Category_Process();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $id = (int) $request->getParam('id');

        if ($id == 0) {                        
            $categories_html = $process->processRoot(1);                                  
            $categories1_html = $process->processRoot(2);            
            $responce = $categories_html . '<div style="height: 10px;"></div>' . $categories1_html;
            echo json_encode(array('html' => $responce));
            return;
        }

        $html = $this->_helper->Cacher->get('processCategory_' . $id);
        if (!$html) {
            $data = My_Registry::Category()->processChildren($id);
            if (!$data) {
                echo json_encode(array('nochildren' => true));
                return;
            }
            $fulldata = array();
            foreach ($data as $el) {
                $cat = Doctrine_Core::getTable('Default_Model_Category')->find($el['id']);
                if ($cat) {
                    if ($cat->getNode()->hasChildren()) {
                        $el['haschildren'] = 1;
                    }
                    $fulldata[] = $el;
                }
            }
            $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
            My_Registry::Category()->pushBreadcrumbs($category->toArray());
            My_Registry::Category()->processBreadcrumbs($id);
            $breadcrumbs = array_reverse(My_Registry::Category()->getBreadcrumbs());
            $html = $this->view->partial('categories/process.phtml', array(
                        'categories' => $fulldata,
                        'breadcrumbs' => $breadcrumbs
                    ));
            $this->_helper->Cacher->set($html, 'processCategory_' . $id, 300);
        }
        echo json_encode(array('html' => $html));
    }

    public function xhrprocessAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $id = (int) $request->getParam('id');

        $html = $this->_helper->Cacher->get('xhrprocessCategory_' . $id);
        if (!$html) {
            if ($id == '0') {
                $category1 = Doctrine_Core::getTable('Default_Model_Category')->find(1);
                $category2 = Doctrine_Core::getTable('Default_Model_Category')->find(2);
                $data = array(
                    0 => $category1->toArray(),
                );
                $level = 0;
            } else {
                $data = My_Registry::Category()->processChildren($id);
                $category = Doctrine_Core::getTable('Default_Model_Category')->find($id);
                $level = $category['level'] + 1;
            }
            if (!$data) {
                return;
            }
            $fulldata = array();
            foreach ($data as $el) {
                $category = Doctrine_Core::getTable('Default_Model_Category')->find($el['id']);
                if ($category) {
                    if ($category->getNode()->hasChildren()) {
                        $el['haschildren'] = 1;
                    }
                    $fulldata[] = $el;
                }
            }
            $html = $this->view->partial('categories/xhrprocess.phtml', array(
                        'categories' => $fulldata,
                        'level' => $level
                    ));
            $this->_helper->Cacher->set($html, 'xhrprocessCategory_' . $id, 300);
        }
        //$responce = new stdClass();
        echo $html;
    }

    public function showAction()
    {
        $name = $this->_getParam('name');
        $category_id = My_Registry::Category()->getCategoryIdByName($name);
        $prefix = Zend_Registry::get('prefix');

        if (!$category_id) {
            $error = new My_Error_Handler();
            $error->ThrowException(404);
        }
        
        $categories = $this->_helper->Cacher->get('category_show_' . $category_id);
        if (!$categories) {
            $categories = My_Registry::Category()->processChildren($category_id);
            $this->_helper->Cacher->set($categories, 'category_show_' . $category_id, 300);
        }
        $this->view->categories = $categories;

        $category = Doctrine_Core::getTable('Default_Model_CategoryDescription')->find($category_id);
        if (!$category) {
            $this->error_handler->ThrowException(404);
        }
        $data = $category->toArray();

        if ($data['text' . $prefix]) {
            $this->view->text = $data['text' . $prefix];
        }
        if ($data['description']) {
            $this->view->headMeta()->setName('description', $data['description' . $prefix]);
        }
        if ($data['keywords']) {
            $this->view->headMeta()->setName('keywords', $data['keywords' . $prefix]);
        }

        $products = new My_Products_Products();

        $params = array();
        $params['cat'] = $category_id;

        $query = $products->productsPaginatorQuery($params);

        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(10);

        if ($paginator->count() != 0) {
            $pagintor_html = $this->view->partial('products/process.phtml', array(
                        'products' => $paginator
                    ));
        } else {
            $paginator_html = $this->view->translate('К сожалению ничего не найдено');
        }
        $title = My_Registry::Category()->getCategoryTitleTrById($category_id, Zend_Registry::get('lang'));

        $this->view->products = $pagintor_html;
		$headTitle = ($data['meta_title' . $prefix]) ? $data['meta_title' . $prefix] : $title ;
        $this->view->headTitle($headTitle);
        $this->view->pagetitle = $title;

    }

}
