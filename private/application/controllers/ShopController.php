<?php

class ShopController extends Zend_Controller_Action
{

    private $request = null;

    public function init()
    {
        $this->request = $this->getRequest();
    }

    public function indexAction()
    {
        $this->_forward('filter', 'shop', 'default', array(
          'page' => 1,
          'filter_params' => '',
        ));
        $this->view->category_title = $this->view->translate('Каталог');
        $this->view->headTitle($this->view->translate('Каталог товаров'));
        $this->view->headMeta()->setName('description', $this->view->translate('Каталог товаров'));
    }

    public function filterAction()
    {
        $filter = new My_Shop_Filter();

        $page = $this->request->getParam('page', 1);

        $filter_params = $this->request->getParam('filter_params');
        $params = $filter->getParams($filter_params);

        $this->view->getCategory($params['name']);
		$this->view->page = $page;

        if ($this->view->headTitle() == '<title></title>') {
            $this->view->category_title = $this->view->translate('Каталог');
            $this->view->headTitle($this->view->translate('Каталог товаров'));
        }

        if ($params['brand']) {
            $brand = new Default_Model_ShopBrand();
            $brand_name = $brand->findByName($params['brand']);
            $this->view->headTitle($brand_name['title']);
			$this->view->brand = $brand_name['title'];
        }

        $query = $filter->filter($params);
        $limit = ($params['limit']) ? $params['limit'] : 12 ;
        $paginator = $filter->filterPaginator($query, $page, $limit);

        $this->view->paginator = $paginator;

        Zend_Registry::set('filter_params', $filter_params);
        Zend_Registry::set('filter_params_array', $params);

        $this->view->setActiveMenu('topMenu', 'Каталог');
        $this->view->setActiveMenu('shopMenu', $this->view->category_title);
    }

    public function productAction()
    {
        $this->view->headScript()->appendFile('/lib/jscrollpane/jquery.jscrollpane.min.js')
            ->appendFile('/lib/jscrollpane/jquery.mousewheel.js');

        $this->view->headLink()->appendStylesheet('/lib/jscrollpane/jquery.jscrollpane.css');

        $id = str_replace('p','',$this->request->getParam('id'));
        $name = $this->request->getParam('name');

        if (!$id || !$name) {
            $this->_helper->ErrorHandler()->error(404);
        }

        $prefix = Zend_Registry::get('prefix');
        $SM = new Default_Model_ShopModel();
        $data = $SM->getFull($id);

        $this->view->relatedProducts = $SM->findByBrand($data['sb_id'], $id);

        $names = explode('_',$name);
        if (count($names) == 2) {
            $brand = $names[0];
            $name_title = $names[1];
        } else {
            $brand = '';
            $name_title = $name;
        }
        if ($data['sb_name'] != $brand || $data['name'] != $name_title) {
            $name = ($data['sb_name']) ? $data['sb_name'] . '_' . $data['name'] : $data['name'];
            $this->_redirect($this->view->url(array('id' => 'p' . $data['id'], 'name' => $name, 'lang' => Zend_Registry::get('lang')), 'shop_product', false));
        }

        if ($data['image_id']) {
           $this->view->og_image = $this->view->imageFilepathOriginal($data['image_id'], 0, 'shop_model');
        }

        $SI = new Default_Model_ShopImage();
        $this->view->images = $SI->all($id);

        $SMC = new Default_Model_ShopColor();
        $this->view->colors = $SMC->findByModelId($id);

        $SMDF = new Default_Model_ShopModification();
        $this->view->modifs = $SMDF->allModerated($id);

        $this->view->title = $data['title' . $prefix];
        $this->view->brand = $data['sb_title'];
        $this->view->description = $data['description' . $prefix];
        $this->view->data = $data;

        $this->view->headTitle($data['title' . $prefix] . ' ' . $data['sb_title']);
        $this->view->headMeta()->setName('description', $data['title' . $prefix] . ' ' . $data['sb_title']);
        if ($data['d_keywords']) $this->view->headMeta()->setName('keywords', $data['d_keywords']);

        $this->view->setActiveMenu('topMenu', 'Каталог');
        $this->view->setActiveMenu('shopMenu', $data['c_title']);
    }

    public function searchAction()
    {
        $this->view->headTitle('Поиск по сайту');

        $page = $this->_getParam('p', 1);

        $q = $this->_getParam('q');
        $q = trim(htmlspecialchars(strip_tags($q)));

        $this->view->q = $q;

        if (mb_strlen($q, 'utf-8') < 4) {
            $this->view->message = 'Поисковая фраза должна содержать не менее 4 символов';
            return;
        }

        $this->view->category_title = $this->view->translate('Поиск');

        $this->_forward('filter', 'shop', 'default', array(
          'page' => $page,
          'filter_params' => 'q=' . $q,
        ));
    }


}









