<?php

/**
 * Description of PagesController
 * 
 * @author Торош Олександр <webtorua@gmail.com>
 * 
 */
class PagesController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $slug = $this->_getParam('slug');
        $this->view->getPage($slug);
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('pages');
        $this->_helper->layout()->setLayout('admin');

        $pages = Doctrine_Core::getTable('Default_Model_Pages')->findAll();
        $this->view->entries = $pages;

        $this->view->headTitle('Список страниц');
        $this->view->title = 'Список страниц';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('pages');
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $pages = new Default_Model_Pages();
        $form = new Application_Form_Pages();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $pages->savePage($formData);
            $this->_redirect('/pages/edit/id/' . $new_id);
        }

        $this->view->headTitle('Добавление страницы');
        $this->view->title = 'Добавление страницы';

        $this->view->form = $form;

        $this->view->setActiveMenu('adminPanel', 'Страницы');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('pages');
        $this->_helper->layout()->setLayout('admin');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $pages = new Default_Model_Pages();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();

        $form = new Application_Form_Pages(array('attribs' => array('param_id' => $id)));

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $pages->savePage($formData, $id);
            $this->_helper->Cacher->remove(My_Cache_Memcached::MCkey('getPage', $data['slug']));
            $this->_redirect($this->view->url());
        }

        $data = $pages->getPageById($id);
        if ($data) {
            $form->populate($data);
        }
        $this->view->form = $form;

        $this->view->headTitle('Редактирование страницы');
        $this->view->title = 'Редактирование страницы';
        $this->view->id = $id;
        $this->view->slug = $data['slug'];
        $this->view->pagetitle = $data['title'];

        if ($this->view->cmsConfig('parent_page')) {
            $breadcrumbs = $pages->getPageBreadcrumbs($data['parent_id']);
            if ($breadcrumbs)
                $this->view->breadcrumbs = $breadcrumbs;
        }

        $this->view->setActiveMenu('adminPanel', 'Страницы');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('pages');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_Pages')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

    public function contactsAction()
    {
        $this->view->getPage('contacts');

        $request = $this->getRequest();
        $post = $request->getPost();

        $mail = new My_Mail_Subscribe();

        if ($request->isPost()) {
            $mail->sendMessageFeedback($post);
            $this->_redirect('/contacts-sended');
        }
    }

    public function contactsSendedAction()
    {
        $this->view->title = 'Сообщение отправлено';
    }

}