<?php

class SeourlController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $pages = Doctrine_Core::getTable('Default_Model_SeoUrl')->findAll();
        $this->view->entries = $pages;

        $this->view->headTitle('Список SeoUrl');
        $this->view->title = 'Список SeoUrl';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');

        $pages = new Default_Model_SeoUrl();
        $form = new Application_Form_SeoUrl();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $pages->saveSeoUrl($formData);
            $this->_redirect('/seourl/edit/id/' . $new_id);
        }

        $this->view->headTitle('Добавление SeoUrl');
        $this->view->title = 'Добавление SeoUrl';

        $this->view->form = $form;

        $this->view->setActiveMenu('adminPanel', 'SeoUrl');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $pages = new Default_Model_SeoUrl();
        $form = new Application_Form_SeoUrl();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $pages->saveSeoUrl($formData, $id);
            $this->_helper->Cacher->remove('seourl_' . md5($data['url']));
            $this->_redirect($this->view->url());
        }

        $form->populate($pages->getSeoUrlById($id));
        $this->view->form = $form;

        $this->view->headTitle('Редактирование SeoUrl');
        $this->view->title = 'Редактирование SeoUrl';

        $this->view->setActiveMenu('adminPanel', 'SeoUrl');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_SeoUrl')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}

