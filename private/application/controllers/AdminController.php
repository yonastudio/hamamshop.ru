<?php

/**
 * Description of AdminController
 * 
 * @author Торош Олександр <webtorua@gmail.com>
 */
class AdminController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->setLayout('admin');
    }

    /**
     * Точка входу
     */
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('admin');
        My_Registry::Auth();
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('administrators');

        $table = Doctrine_Core::getTable('Default_Model_Administrator')->findAll();
        if (!$table) {
            $this->_redirect('/admin/add');
        } else {
            $this->view->entries = $table->toArray();
        }        
        
        $this->view->title = 'Список администраторов';
        $this->view->headTitle('Список администраторов');
        
        $this->view->chAddOn = 1;
    }

    /**
     * Створення запису адміністратора.
     */
    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('administrators');

        $this->_helper->viewRenderer('edit');
        
        $form = new Application_Form_Administrator();
        $request = $this->getRequest();
        $params = $request->getParams();

        if ($request->isPost() && $form->isValid($params)) {

            $samelogin = Doctrine_Core::getTable('Default_Model_Administrator')->findOneByLogin($params['login']);
            if ($samelogin) {
                echo "Учетная запись с таким логином уже существует";
                return;
            }

            $administrator = new Default_Model_Administrator();
            $administrator->addAdministrator($form->getValues());

            $this->_redirect('/admin/list');
        }

        $this->view->form = $form;
        
        $this->view->title = 'Добавление администратора';
        $this->view->headTitle('Добавление администратора');
        
        $this->view->setActiveMenu('adminPanel', 'Администраторы');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('administrators');

        $this->_helper->viewRenderer('edit');
        
        $form = new Application_Form_Administrator();
        $form->getElement('password')->setRequired(false);
        $administrator = new Default_Model_Administrator();
        
        $request = $this->getRequest();
        $params = $request->getParams();
        $id = $params['id'];

        if ($request->isPost() && $form->isValid($params)) {
            $administrator->saveAdministrator($form->getValues(), $id);

            $this->_redirect($this->view->url());
        }
        
        $row = $administrator->getAdminById($id);
        $form->populate($row);
        
        $this->view->form = $form;
        
        $this->view->title = 'Редактирование администратора';
        $this->view->headTitle('Редактирование администратора');
        
        $this->view->setActiveMenu('adminPanel', 'Администраторы');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function loginAction()
    {
        $this->_helper->layout()->setLayout('clear');
        $form = new Application_Form_Login();

        if ($this->getRequest()->isPost()) {

            $filter = new Zend_Filter_StripTags();
            $username = $filter->filter($this->_request->getPost('login'));
            $password = $filter->filter($this->_request->getPost('password'));

            $authAdapter = new ZendX_Doctrine_Auth_Adapter(
                            Doctrine_Core::getConnectionByTableName('Default_Model_Administrator'));
            $authAdapter->setTableName('Default_Model_Administrator a')
                    ->setIdentityColumn('a.login')
                    ->setCredentialColumn('a.password_hash')
                    ->setCredentialTreatment('MD5(CONCAT(?,a.password_salt))')
                    ->setIdentity($username)->setCredential($password);
            $auth = Zend_Auth::getInstance();
            $auth ->clearIdentity();
            
            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                $data = $authAdapter->getResultRowObject(null, array(
                            'password_hash',
                            'password_salt'));
                $auth->getStorage()->write($data);
                $this->_redirect('/admin');
            } else {
                sleep(2);
                $this->view->message = '<p style="text-align: center; color: #C00;">Неправильный логин или пароль</p>';
            }
        }

        $this->view->title = "Вход в административную панель";
        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        $this->_redirect('/admin');
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('administrators');

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $request->getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_Administrator')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}