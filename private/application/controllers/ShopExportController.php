<?php

class ShopExportController extends Zend_Controller_Action
{

    public function init()
    {

        $this->_helper->layout()->setLayout('admin');
    }

    public function indexAction()
    {
        My_Registry::Auth();
    }

    public function createAction()
    {
        $this->_helper->Acl->checkAllowed('shop:export');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $SM = new Default_Model_ShopModification();
        $rows = $SM->exportList();

        $excel = new My_Shop_Export_Excel();
        $file = $excel->build($rows);
        $this->_redirect('/' . $file);
    }

    public function importAction()
    {
        $this->_helper->Acl->checkAllowed('shop:import');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $upload = new My_Shop_Import_Upload();
            $filename = $upload->upload();
            if ($filename) {
                $excel = new My_Shop_Import_Excel();
                $data = $excel->read($filename);
                if ($data) {
                    if (count($data) > 2) {
                        foreach (array_slice($data, 1) as $el) {
                            if ($el[1]) {
                                $item = Doctrine_Core::getTable('Default_Model_ShopModification')->find($el[1]);
                                if ($item) {
                                    if ($el[4] && $el[4] != $item['price']) $item->price = $el[4];
                                    if (in_array($el[5],array(0,1)) && $el[5] != $item['status']) $item->status = $el[5];
                                    $item->save();
                                }
                            } else {
                                if ($el[2]) {
                                    $mc = Doctrine_Core::getTable('Default_Model_ShopModifColor')->findOneById($el[2]);
                                    if ($mc) {
                                        $presence = (int) $el[5];
                                        if ($presence != (int) $mc['presence']) {
                                            $mc->presence = $presence;
                                            $mc->save();
                                        }
                                    }
                                }
                            }
                        }
                        echo "Данные успешно обновлены!";
                        return;
                    }
                }
            }
            echo "Ошибка";
        }
    }


}





