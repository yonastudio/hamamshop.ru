<?php

class ServiceController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function jscodeAction()
    {
        $this->_helper->Acl->checkAllowed('javascript');
        $this->_helper->layout()->setLayout('admin');
        
        $form = new Application_Form_JsCode();
        $jsCode = new My_Service_JsCode();

        $request = $this->getRequest();

        if ($request->isPost() && $form->isValid($request->getPost())) {
            $jsCode->write($form->getElement('source')->getValue());
            $this->_redirect($this->view->url());
        }

        $read = $jsCode->read();
        if ($read) {
            $data = array();
            $data['source'] = $read;
            $form->populate($data);
        }

        $this->view->form = $form;

        $title = 'Вставка Javascript';
        $this->view->headTitle($title);
        $this->view->title = $title;
        
        $this->view->chSaveOn = 1;
    }

    public function categoriesAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $first = Doctrine_Core::getTable('Default_Model_Category')
                ->findOneByName('pestemali');

        $second = Doctrine_Core::getTable('Default_Model_Category')
                ->findOneByName('aksessuary');

        $second->getNode()->moveAsNextSiblingOf($first);

        echo "done";
    }

}

