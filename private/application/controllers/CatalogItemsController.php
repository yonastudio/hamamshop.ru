<?php

class CatalogItemsController extends Zend_Controller_Action
{

    private $CI;
    private $CII;
    private $CB;
    private $form;
    private $request;
    private $post;

    public function init()
    {
        $this->CI = new Default_Model_CatalogItem();
        $this->CB = new Default_Model_CatalogBrand();
        $this->CII = new Default_Model_CatalogItemImage();
        $this->form = new Application_Form_CatalogItem();
        $this->request = $this->getRequest();
        $this->post = $this->request->getPost();
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $this->view->entries = $this->CI->all();

        $this->view->headTitle('Список Товаров/Услуг');
        $this->view->title = 'Список Товаров/Услуг';
        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');
        
        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $id = $this->CI->addItem($this->form->getValues());
            if ($id) {
                $this->_redirect('/catalog-items/edit/id/' . $id);
            }
        }

        $this->view->setActiveMenu('adminPanel', 'Товары/Услуги');
        $this->view->form = $this->form;
        $this->view->headTitle('Добавление Товара/Услуги');
        $this->view->title = 'Добавление Товара/Услуги';
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $id = $this->_getParam('id');

        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $id = $this->CI->saveItem($this->form->getValues(), $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->CI->find($id);
        $this->form->populate($data);

        $this->view->form = $this->form;
        $this->view->id = $id;
        $this->view->item = $data;
        $this->view->images = $this->CII->all($id);
        $this->view->headTitle('Редактирование Товара/Услуги');
        $this->view->title = 'Редактирование Товара/Услуги';

        $this->view->setActiveMenu('adminPanel', 'Товары/Услуги');
        $this->view->chSaveOn = 1;
        $this->view->chDelOn = 1;
        $this->view->chAddOn = 1;
    }

    public function deleteAction()
    {
        // action body
    }

    public function changeOrderAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $first = $this->_getParam('first');
        $second = $this->_getParam('second');
        $item_id = $this->_getParam('item_id');

        $table_first = Doctrine_Core::getTable('Default_Model_CatalogItemImage')->find($first);
        $table_second = Doctrine_Core::getTable('Default_Model_CatalogItemImage')->find($second);

        $sortorder_first = $table_first['sortorder'];
        $sortorder_second = $table_second['sortorder'];

        $connection = Doctrine_Manager::connection();
        $connection->beginTransaction();

        $table_first->sortorder = $sortorder_second;
        $table_second->sortorder = $sortorder_first;

        $table_first->save();
        $table_second->save();

        $connection->commit();

        $q = Doctrine_Query::create()
                ->from('Default_Model_CatalogItemImage')
                ->where('catalog_item_id = ?', $item_id)
                ->orderBy('sortorder ASC');
        $item_images = $q->fetchArray();

        $catalog_item = Doctrine_Core::getTable('Default_Model_CatalogItem')->find($item_id);
        $catalog_item->image_id = $item_images[0]['id'];
        $catalog_item->save();

        echo true;
    }

}

