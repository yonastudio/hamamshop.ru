<?php

class ArticlesController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $articles = new Default_Model_Articles();
        $query = $articles->paginatorQuery();

        $page = $this->_getParam('page');

        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $this->view->paginator = $paginator;

        $this->view->headTitle($this->view->translate('Статьи'));
        $this->view->title = $this->view->translate('Статьи');

		$this->view->disable_eng = true;
    }

    public function showAction()
    {
        $id = $this->_getParam('id');
        $data = $this->view->getArticle($id);
        $this->view->img_thumb = $data['img_thumb'];

        $this->view->setActiveMenu('topMenu', 'Статьи');

		$this->view->disable_eng = true;
    }

    public function listAction()
    {
        $this->_helper->Acl->checkAllowed('articles');
        $this->_helper->layout()->setLayout('admin');

        $articles = Doctrine_Core::getTable('Default_Model_Articles')->findAll();
        $this->view->entries = $articles;

        $this->view->headTitle('Список статей');
        $this->view->title = 'Список статей';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        $this->_helper->Acl->checkAllowed('articles');
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $articles = new Default_Model_Articles();
        $form = new Application_Form_Articles();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $articles->saveArticle($formData);
            $this->_redirect('/articles/edit/id/' . $new_id);
        }

        $this->view->headTitle('Добавление статьи');
        $this->view->title = 'Добавление статьи';

        $this->view->form = $form;

        $this->view->setActiveMenu('adminPanel', 'Статьи');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        $this->_helper->Acl->checkAllowed('articles');
        $this->_helper->layout()->setLayout('admin');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $articles = new Default_Model_Articles();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();

        $form = new Application_Form_Articles(array('attribs' => array('param_id' => $id)));

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $articles->saveArticle($formData, $id);
            $this->_redirect($this->view->url());
        }

        $article = $articles->getArticleById($id);
        if (!$article) {
            $this->_helper->ErrorHandler->error(404);
        }

        $form->populate($article);
        $this->view->form = $form;
        $this->view->id = $id;
        $this->view->img_thumb = $article['img_thumb'];
        $this->view->category_id = $article['category_id'];

        $this->view->headTitle('Редактирование статьи');
        $this->view->title = 'Редактирование статьи';

        $this->view->setActiveMenu('adminPanel', 'Статьи');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        $this->_helper->Acl->checkAllowed('articles');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_Articles')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}

