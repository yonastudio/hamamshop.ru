<?php

class ShopFavoritesController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$storage || $storage->access_level) {
            $this->view->message = $this->view->translate("Избранное только для авторизированых пользователей");
            return;
        }

        $delete = $request->getParam('del');
        $id = $request->getParam('id');
        if ($delete && $id) {
            $item = Doctrine_Core::getTable('Default_Model_ShopFavorites')->findOneByIdAndUserId($id, $storage->id);
            if ($item) {
                $item->delete();
            }
        }

        $sf = new Default_Model_ShopFavorites();
        $this->view->entries = $sf->all($storage->id);
    }

    public function addAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $params = $request->getParams();
        $responce = new stdClass();

        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if ($storage) {
            if (!$storage->access_level) {
                $sf = new Default_Model_ShopFavorites();
                $sf->addItem($params, $storage->id);
                $responce->success = 1;
            } else {
                $responce->error = "Нельзя совершать покупки, находясь в админке.";
            }
        } else {
            $responce->needauth = 1;
        }
        echo json_encode($responce);
    }

}