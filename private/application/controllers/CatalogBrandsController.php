<?php

class CatalogBrandsController extends Zend_Controller_Action
{

    private $CB;
    private $form;
    private $request;
    private $post;

    public function init()
    {
        $this->CB = new Default_Model_CatalogBrand();
        $this->form = new Application_Form_CatalogBrand();
        $this->request = $this->getRequest();
        $this->post = $this->request->getPost();
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $this->view->entries = $this->CB->all();
        $this->view->headTitle('Список производителей');
        $this->view->title = 'Список производителе';
        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');

        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $id = $this->CB->addItem($this->form->getValues());
            if ($id) {
                $this->_redirect('/catalog-brands/edit/id/' . $id);
            }
        }

        $this->view->setActiveMenu('adminPanel', 'Производители');
        $this->view->form = $this->form;
        $this->view->headTitle('Добавление производителя');
        $this->view->title = 'Добавление производителя';
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $id = $this->_getParam('id');

        if ($this->request->isPost() && $this->form->isValid($this->post)) {
            $this->CB->saveItem($this->form->getValues(), $id);
            $this->_redirect($this->view->url());
        }

        $data = $this->CB->find($id);
        if ($data) {
            $this->form->populate($data);
        }
        $this->view->form = $this->form;
        
        $this->view->headTitle('Редактирование производителя');
        $this->view->title = 'Редактирование производителя';
        $this->view->id = $id;
        $this->view->setActiveMenu('adminPanel', 'Производители');

        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_CatalogBrand')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }


}









