<?php

class SeotextController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $pages = Doctrine_Core::getTable('Default_Model_SeoText')->findAll();
        $this->view->entries = $pages;

        $this->view->headTitle('Список SeoText');
        $this->view->title = 'Список SeoText';

        $this->view->chAddOn = 1;
    }

    public function addAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->_helper->viewRenderer('edit');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $pages = new Default_Model_SeoText();
        $form = new Application_Form_SeoText();

        $request = $this->getRequest();
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $new_id = $pages->saveSeoText($formData);
            $this->_redirect('/seotext/edit/id/' . $new_id);
        }

        $this->view->headTitle('Добавление SeoText');
        $this->view->title = 'Добавление SeoText';

        $this->view->form = $form;

        $this->view->setActiveMenu('adminPanel', 'SeoText');
        $this->view->chSaveOn = 1;
    }

    public function editAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');
        $this->view->headScript()->appendFile('/lib/tiny_mce/jquery.tinymce.js')
                ->appendFile('/js/tiny_mce_config.js');

        $pages = new Default_Model_SeoText();
        $form = new Application_Form_SeoText();

        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $data = $request->getPost();

        if ($request->isPost() && $form->isValid($data)) {
            $formData = $form->getValues();
            $pages->saveSeoText($formData, $id);
            $this->_helper->Cacher->remove('seotext_' . md5($data['url']));
            $this->_redirect($this->view->url());
        }

        $form->populate($pages->getSeoTextById($id));
        $this->view->form = $form;

        $this->view->headTitle('Редактирование SeoText');
        $this->view->title = 'Редактирование SeoText';

        $this->view->setActiveMenu('adminPanel', 'SeoText');
        $this->view->chSaveOn = 1;
        $this->view->chAddOn = 1;
        $this->view->chDelOn = 1;
    }

    public function deleteAction()
    {
        My_Registry::Auth();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            return;
        }
        $id = $this->_getParam('id');
        $table = Doctrine_Core::getTable('Default_Model_SeoText')->find($id);
        if ($table->delete()) {
            echo true;
        }
    }

}

