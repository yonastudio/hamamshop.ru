<?php

class SubscribeController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        My_Registry::Auth();
        $this->_helper->layout()->setLayout('admin');

        $new = new Default_Model_News();
        $subscribe = new Default_Model_Subscribe();
        $mail = new My_Mail_Subscribe();

        $news = $new->getNewsForSubscribe();
        $message = $mail->createMessageByNews($news, 'wezoom');

        $request = $this->getRequest();
        $post = $request->getPost();

        if ($request->isPost()) {
            $emails = $subscribe->getEmails();
            foreach ($emails as $el) {
                $send = $mail->sendMessageSubscribeNews($message, $el['email']);
            }
            echo '</br>Отправленное сообщение:</br></br>' . $message;
        }

        $this->view->headTitle('Рассылки');
        $this->view->title = 'Рассылки';
    }

}

