<?php

/**
 * Default_Model_Base_SeoUrl
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $keywords
 * @property string $keywords_uk
 * @property string $keywords_en
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class Default_Model_Base_SeoUrl extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('seo_url');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => '4',
             ));
        $this->hasColumn('url', 'string', 255, array(
             'type' => 'string',
             'unsigned' => true,
             'notnull' => true,
             'unique' => true,
             'length' => '255',
             ));
        $this->hasColumn('title', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('title_uk', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('title_en', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('description', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('description_uk', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('description_en', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('keywords', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('keywords_uk', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('keywords_en', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_unicode_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}