<?php

/**
 * Default_Model_Base_ShopOrder
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_id
 * @property tinyint $discount_id
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property tinyint $payment
 * @property tinyint $delivery
 * @property string $adress
 * @property string $message
 * @property tinyint $status
 * @property tinyint $payment_status
 * @property Default_Model_ShopOrderModifs $ShopOrderModifs
 * @property Default_Model_User $User
 * @property Default_Model_Discount $Discount
 * @property Doctrine_Collection $Default_Model_ShopPaymentOrder
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class Default_Model_Base_ShopOrder extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('shop_orders');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => '4',
             ));
        $this->hasColumn('user_id', 'integer', 4, array(
             'type' => 'integer',
             'length' => '4',
             ));
        $this->hasColumn('discount_id', 'tinyint', 2, array(
             'type' => 'tinyint',
             'length' => '2',
             ));
        $this->hasColumn('fio', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('email', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('phone', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('payment', 'tinyint', 1, array(
             'type' => 'tinyint',
             'length' => '1',
             ));
        $this->hasColumn('delivery', 'tinyint', 1, array(
             'type' => 'tinyint',
             'length' => '1',
             ));
        $this->hasColumn('adress', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('message', 'string', null, array(
             'type' => 'string',
             'length' => '',
             ));
        $this->hasColumn('status', 'tinyint', 1, array(
             'type' => 'tinyint',
             'notnull' => true,
             'length' => '1',
             ));
        $this->hasColumn('payment_status', 'tinyint', 1, array(
             'type' => 'tinyint',
             'notnull' => true,
             'length' => '1',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Default_Model_ShopOrderModifs as ShopOrderModifs', array(
             'local' => 'id',
             'foreign' => 'order_id'));

        $this->hasOne('Default_Model_User as User', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $this->hasOne('Default_Model_Discount as Discount', array(
             'local' => 'discount_id',
             'foreign' => 'id'));

        $this->hasMany('Default_Model_ShopPaymentOrder', array(
             'local' => 'id',
             'foreign' => 'order_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}