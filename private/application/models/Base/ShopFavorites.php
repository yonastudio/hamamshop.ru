<?php

/**
 * Default_Model_Base_ShopFavorites
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_id
 * @property integer $modification_id
 * @property integer $color_id
 * @property Default_Model_ShopModification $ShopModification
 * @property Default_Model_ShopColor $ShopColor
 * @property Default_Model_User $User
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class Default_Model_Base_ShopFavorites extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('shop_favorites');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => '4',
             ));
        $this->hasColumn('user_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '4',
             ));
        $this->hasColumn('modification_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '4',
             ));
        $this->hasColumn('color_id', 'integer', 4, array(
             'type' => 'integer',
             'length' => '4',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Default_Model_ShopModification as ShopModification', array(
             'local' => 'modification_id',
             'foreign' => 'id'));

        $this->hasOne('Default_Model_ShopColor as ShopColor', array(
             'local' => 'color_id',
             'foreign' => 'id'));

        $this->hasOne('Default_Model_User as User', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}