<?php

/**
 * Default_Model_Administrator
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Default_Model_Administrator extends Default_Model_Base_Administrator
{

    public function addAdministrator($params)
    {
        $table = new Default_Model_Administrator();
        $table->email = $params['email'];
        $table->login = $params['login'];
        $table->realname = $params['realname'];
        $table->access_level = $params['access_level'];
        $table->setPassword($params['password']);
        $table->save();
        
        $id = $table->identifier();
        return $id[0]['id'];
    }
    
    public function saveAdministrator($params, $id)
    {
        $table = Doctrine_Core::getTable('Default_Model_Administrator')->find($id);
        if (!$table) {
            return false;
        }
        $table->email = $params['email'];
        $table->login = $params['login'];
        $table->realname = $params['realname'];
        $table->access_level = $params['access_level'];
        $table->setPassword($params['password']);
        $table->save();       
    }
    
    public function getAdminById($id)
    {
        $table = Doctrine_Core::getTable('Default_Model_Administrator')->find($id);
        if ($table) {
            $row = $table->toArray();
            return $row;
        }
    }
    
    public function setPassword($password)
    {
        if (strlen($password)) {
            $passwordSalt = substr(md5(time()), 0, rand(5, 8));
            $passwordHash = md5($password . $passwordSalt);
            $this->_set('password_hash', $passwordHash);
            $this->_set('password_salt', $passwordSalt);
        }
    }

}