<?php

/**
 * Default_Model_ShopModifColor
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Default_Model_ShopModifColor extends Default_Model_Base_ShopModifColor
{
    
    public function findByModifId($modif_id)
    {
        $q = Doctrine_Query::create()
                ->select('mc.*, c.service_title AS service_title')
                ->from('Default_Model_ShopModifColor mc')
                ->leftJoin('mc.ShopColor c')
                ->where('mc.modif_id = ?', $modif_id);
        $rows = $q->fetchArray();
        return $rows;
    }
    
}