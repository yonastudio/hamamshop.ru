<?php

/**
 * Default_Model_News
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Default_Model_News extends Default_Model_Base_News
{
    public function saveNew($data, $id = null)
    {
        if ($id) {
            $table = Doctrine_Core::getTable('Default_Model_News')->find($id);
        } else {
            $table = new Default_Model_News();
        }

        if ($data['slug']) {
            $table->slug = $data['slug'];
        } else {
            $table->setSlug($data['title']);
        }
        $table->title = $data['title'];
        $table->title_uk = $data['title_uk'];
        $table->title_en = $data['title_en'];
        $table->text = $data['text'];
        $table->text_uk = $data['text_uk'];
        $table->text_en = $data['text_en'];
        if (strlen($data['created_at'])) {
            $table->created_at = $data['created_at'];
        }

        $table->save();

        if (!$id) {
            $newId = $table->identifier();
            return $newId['id'];
        }
    }

    public function getNewById($id)
    {
        $q = Doctrine_Query::create()
                ->from('Default_Model_News')
                ->where('id = ?', $id);
        $row = $q->fetchArray();
        return $row[0];
    }

    public function getNewBySlug($page)
    {
        $q = Doctrine_Query::create()
                ->from('Default_Model_News')
                ->where('slug = ?', $page);
        $row = $q->fetchArray();
        return $row[0];
    }

    public function paginatorQuery()
    {
        
        $q = Doctrine_Query::create()
                ->from('Default_Model_News')                
                ->orderBy('created_at DESC');
        return $q;
    }

    public function all($limit = null, $prefix = null)
    {
        $title = "title".$prefix;
        $q = Doctrine_Query::create()
                ->from('Default_Model_News')
                ->orderBy('created_at DESC');
        if ($prefix) {
            $q->where($title. ' <>""');
        }
        if ($limit) {
            $q->limit($limit);
        }
        $rows = $q->fetchArray();
        if ($rows) {
            return $rows;
        }
    }

    public function setSlug($slug)
    {
        $this->slug = My_Translate_Slug::slugify($slug);
    }
}