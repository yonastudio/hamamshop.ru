<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopBrandFilter extends Zend_View_Helper_Abstract
{
    public function shopBrandFilter()
    {
        if (Zend_Registry::isRegistered('filter_params_array')) {
            $params = Zend_Registry::get('filter_params_array');
        } else {
            return;
        }
        $brand = new Default_Model_ShopBrand();
        $rows = $brand->filter($params);
        $html = $this->view->partial('shop/brand-filter.phtml',array('entries' => $rows, 'params' => $params));
        return $html;
    }
}
