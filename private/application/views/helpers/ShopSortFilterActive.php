<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopSortFilterActive extends Zend_View_Helper_Abstract
{
    public function shopSortFilterActive($params, $callback_type = 'select')
    {
        if (Zend_Registry::isRegistered('filter_params_array')) {
            $currents = Zend_Registry::get('filter_params_array');
        }
        switch ($callback_type) {
            case 'select' :
                if ($currents['sort'] == $params['sort']) {
                    if ($currents['by'] == $params['by']) {
                        return 'selected';
                    }
                }
                break;
            case 'a' :
                if ($currents['limit'] == $params['limit']) {
                   return 'class="active"';
                } elseif (!$currents['limit'] && $params['limit'] == 12) {
                   return 'class="active"';
                }
                break;
        }

    }
}
