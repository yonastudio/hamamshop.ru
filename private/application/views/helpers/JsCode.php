<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_JsCode extends Zend_View_Helper_Abstract
{

    public function jsCode()
    {
        $jsCode = new My_Service_JsCode();
        $data = $jsCode->read();
        return $data;
    }

}
