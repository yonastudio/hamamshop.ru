<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_BooleanType extends Zend_View_Helper_Abstract
{

    public function booleanType($val)
    {
        if ($val) {
            return "Да";
        } else {
            return "Нет";
        }
    }

}
