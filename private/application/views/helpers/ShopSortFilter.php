<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopSortFilter extends Zend_View_Helper_Abstract
{
    public function shopSortFilter()
    {
        $html = $this->view->partial('shop/sort-filter.phtml');
        return $html;
    }
}
