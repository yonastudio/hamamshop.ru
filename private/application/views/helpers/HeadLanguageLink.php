<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of headLanguageLink
 *
 * @author tos
 */
class Zend_View_Helper_HeadLanguageLink extends Zend_View_Helper_Abstract {

    public function headLanguageLink($lg)
    {  
        $lang = Zend_Registry::get('lang');
        
        $url = $this->view->url(array('lang' => $lg));
        
        switch ($lg) {
            case 'ru':
                $class = 'ru';
                break;
            case 'en':
                $class = 'en';
                break;
        }
        
        if ($lang == $lg) {
            return '<li class="active"><span class="'.$class.'"></span>';
        } else {
            $url = str_replace('%3D','=',$url);
            return '<li><a class="'.$class.'" href="'.$url.'"></a></li>';
        }
    }

}
?>
