<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImageView
 *
 * @author tos
 */
class Zend_View_Helper_ImageView extends Zend_View_Helper_Abstract {

    public function imageView($id, $imageId, $params, $type = null)
    {
        $image = new My_Image_Filter($type);
        return $image->ShowImageHtmlView($id, $imageId, $params);
    }

}
?>
