<?php

/**
 * Description of GetPage
 *
 * @author веб-студия "WeZoom" <web@wezoom.net> http://wezoom.net
 * @author ведущий разрабочик Торош Александр <webtorua@gmail.com>
 */
class Zend_View_Helper_GetCategory extends Zend_View_Helper_Abstract
{

    public function getCategory($name = null)
    {
        if (!$name) {
            return;
        }
        $prefix = Zend_Registry::get('prefix');

        $category = My_Category_Category::getInstance();
        $category_id = $category->getCategoryIdByName($name);

        $category_data = $category->getFullData($category_id);
        if ($category_data['meta_title' . $prefix]) {
            //$this->view->category_title = $category_data['meta_title' . $prefix];
			$this->view->category_title = $category_data['title' . $prefix];
            $this->view->headTitle($category_data['meta_title' . $prefix]);
        } elseif ($category_data['title' . $prefix]) {
            $this->view->category_title = $category_data['title' . $prefix];
            $this->view->headTitle($category_data['title' . $prefix]);
	}
        if ($category_data['description']) $this->view->headMeta()->setName('description', $category_data['description']);
        if ($category_data['keywords']) $this->view->headMeta()->setName('keywords', $category_data['keywords']);
		if ($category_data['text']) $this->view->text = $category_data['text'];

        return $category_data;
    }

}
