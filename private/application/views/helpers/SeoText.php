<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_SeoText extends Zend_View_Helper_Abstract
{

    public function seoText()
    {
        /* $seoText = new My_Service_SeoText();
          $data = $seoText->read();
          return $data; */

        $url = $_SERVER['REQUEST_URI'];
        $cachemanager = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');

        $html = $cachemanager->get('seotext_' . md5($url));
        if (!$html) {
            $seo_text = new Default_Model_SeoText();
            $data = $seo_text->getSeoTextByUrl($url);
            if (!$data) {
                return false;
            }
            $html = $this->view->partial('seotext/index.phtml', array(
                'seotext' => $data
                    ));
            $cachemanager->set($html, 'seotext_' . md5($url), 3600);
        }

        return $html;
    }

}
