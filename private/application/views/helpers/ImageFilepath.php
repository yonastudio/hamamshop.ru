<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImageView
 *
 * @author tos
 */
class Zend_View_Helper_ImageFilepath extends Zend_View_Helper_Abstract {

    public function imageFilepath($id, $imageId, $params, $type = null)
    {
        $image = new My_Image_Filter($type);
        $image->process($id, $imageId, $params);
        return $image->ImageFilepath($id, $imageId, $params);
    }

}
?>
