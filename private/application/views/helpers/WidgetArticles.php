<?php
/**
 * Created by JetBrains PhpStorm.
 * User: webtorua
 * Date: 19.11.11
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
 
class Zend_View_Helper_WidgetArticles extends Zend_View_Helper_Abstract
{
    public function widgetArticles()
    {
        $cacher = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');
        $data = $cacher->get('widget_articles');
        if (!$data) {
            $news = new Default_Model_Articles();
            $data = $news->all(3, Zend_Registry::get('prefix'));
            $cacher->set($data,'widget_articles',300);
        }
        $html = $this->view->partial('widget/articles.phtml',array('entries' => $data));
        return $html;
    }
}
