<?php
/**
 * Created by JetBrains PhpStorm.
 * User: webtorua
 * Date: 22.10.11
 * Time: 0:43
 * To change this template use File | Settings | File Templates.
 */

class Zend_View_Helper_UrlGET extends Zend_View_Helper_Abstract
{
    public function urlGET($page = array())
    {
        $url = $this->view->url();
        if (count($_GET) >= 0) {
            $url .= '?';
        }
        $i = 0;
        foreach($_GET as $el => $val) {
            if ($el == 'page') continue;
            if ($i != 0) {
                $url .= '&';
            }
            $url .= $el . '=' . $val;
            $i++;
        }
        $url .= '&page=' . $page['page'];
        return $url;
    }
}
