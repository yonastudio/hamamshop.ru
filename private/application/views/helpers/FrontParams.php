<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChAdd
 *
 * @author Александр
 */
class Zend_View_Helper_FrontParams extends Zend_View_Helper_Abstract
{

    public function frontParams()
    {
        $front = Zend_Controller_Front::getInstance();
        //$controller = $front->getParam('controller');
        $request = $front->getRequest();
        $controller = $request->getParam('controller');
        $action = $request->getParam('action');
        $id = $request->getParam('id');
        $lang = $request->getParam('lang');

        $str = '';
        $str .= $this->_input('controller', $controller);
        $str .= $this->_input('action', $action);
        $str .= $this->_input('id', $id);
        $str .= $this->_input('lang', $lang);
        $str .= $this->_input('uri', $_SERVER['REQUEST_URI']);
        $str .= $this->_input('currency', 'UAH');

        return $str;
    }

    private function _input($id, $val)
    {
        return '<input type="hidden" id="' . $id . '" value="' . $val . '" />';
    }

}

?>
