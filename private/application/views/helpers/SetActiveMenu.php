<?php

/**
 * Description of SetActiveMenu
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Болонин Андрей <pilotftzi@gmail.com>
 */
class Zend_View_Helper_SetActiveMenu extends Zend_View_Helper_Abstract
{

    public function setActiveMenu($nav, $label)
    {
        $container = $this->view->$nav;
        $menu = $this->view->navigation()->menu($container);
        $active = $menu->findByLabel($label);
        if ($active) {
            $active->setActive(true);
        }
    }

}