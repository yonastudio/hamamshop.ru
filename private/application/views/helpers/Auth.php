<?php
 /**
 * Description of Auth
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */
 
class Zend_View_Helper_Auth extends Zend_View_Helper_Abstract
{
    public function auth()
    {
        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if (!$storage->access_level) {
            if (!$storage) {
                return $this->view->partial('users/auth_out.phtml');
            } else {
                return $this->view->partial('users/auth_in.phtml', array('storage' => $storage));
            }
        } else {
            return '<a href="/admin">Админпанель</a>';
        }
    }
}
