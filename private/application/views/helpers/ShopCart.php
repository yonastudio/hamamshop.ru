<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopCart extends Zend_View_Helper_Abstract
{

    public function shopCart()
    {
        /*$cacher = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');

        if (!Zend_Session::namespaceIsset('Cart')) {
            new Zend_Auth_Storage_Session('Cart');
        }

        $session_id = Zend_Session::getId();

        $count = $cacher->get('Cart_count_' . $session_id);
        $sum = $cacher->get('Cart_sum_' . $session_id);

        if (!$count || !$sum) {
            $cart = new Default_Model_ShopCart();
            $sumAndCount = $cart->sumAndCount($session_id);
            $sum = $sumAndCount['sum'];
            $count = $sumAndCount['count'];

            $cacher->set($sumAndCount['sum'], 'Cart_sum_' . $session_id, 3600);
            $cacher->set($sumAndCount['count'], 'Cart_count_' . $session_id, 3600);
        }*/

        $session_id = Zend_Session::getId();

        $cart = new Default_Model_ShopCart();
        $sumAndCount = $cart->sumAndCount($session_id);

        $count = $sumAndCount['count'];

        $count = ($count) ? $count : '0';

        return '<div id="shop-cart-count">'.$count.'</div>';
    }

}
