<?php

/**
 * Description of GetPage
 *
 * @author веб-студия "WeZoom" <web@wezoom.net> http://wezoom.net
 * @author ведущий разрабочик Торош Александр <webtorua@gmail.com>
 */
class Zend_View_Helper_GetPage extends Zend_View_Helper_Abstract
{

    public function getPage($slug)
    {
        $pages = new Default_Model_Pages();
        $cacher = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');

        $data = $cacher->get(My_Cache_Memcached::MCkey('getPage', $slug));
        if (!$data) {
            $data = $pages->getPageBySlug($slug);
            $cacher->set($data, My_Cache_Memcached::MCkey('getPage', $slug), 3600);
        }

        if (!$data) {
            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $redirector->gotoUrl('/');
        }

        $prefix = '';
        if (Zend_Registry::isRegistered('prefix')) {
            $prefix = Zend_Registry::get('prefix');
        }

        if ($data['text' . $prefix])
            $this->view->content = $data['text' . $prefix];
        if ($data['title' . $prefix])
            $this->view->headTitle($data['title' . $prefix]);
        $this->view->title = $data['title' . $prefix];
        if ($data['description' . $prefix])
            $this->view->headMeta()->setName('description', $data['description' . $prefix]);
        if ($data['keywords' . $prefix])
            $this->view->headMeta()->setName('keywords', $data['keywords' . $prefix]);
        
        if ($this->view->cmsConfig('parent_page')) {
            $breadcrumbs = $pages->getPageBreadcrumbs($data['parent_id']);
            if ($breadcrumbs)
                $this->view->breadcrumbs = $breadcrumbs;
        }
    }

}