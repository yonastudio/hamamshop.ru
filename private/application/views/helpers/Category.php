<?php

/**
 * Description of Category
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_Category extends Zend_View_Helper_Abstract
{
    
    public function category()
    {
        return My_Registry::Category();
    }
    
}
