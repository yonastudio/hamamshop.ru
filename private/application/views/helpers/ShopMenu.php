<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopMenu extends Zend_View_Helper_Abstract
{
    public function shopMenu()
    {
        $container = $this->view->shopMenu;

        $menu = $this->view->navigation()->menu($container)->setPartial(array('shop/ShopMenu.phtml','default'));
        $menu->setUlClass('shop-menu');
        return $menu->render();
    }
}
