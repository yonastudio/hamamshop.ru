<?php

/**
 * Description of Category
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_Storage extends Zend_View_Helper_Abstract
{

    private static $_class = array();

    private static $_array = array(
        1 => 'My_Storage_Shop_FieldDataType',
        2 => 'My_Storage_Shop_ProductStatus',
        3 => 'My_Storage_Shop_Delivery',
        4 => 'My_Storage_Shop_Payment',
        5 => 'My_Storage_Shop_OrderStatus',
        6 => 'My_Storage_Shop_PaymentStatus',
    );

    private static function CalledClass($key)
    {
        if (!isset(self::$_class[$key])) {
            $class = self::$_array[$key];
            if ($class) {
                self::$_class[$key] = new $class;
            }
        }
        return self::$_class[$key];
    }

    public function storage($key)
    {
        return self::CalledClass($key);
    }

}
