<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChAdd
 *
 * @author Александр
 */
class Zend_View_Helper_ChAdd extends Zend_View_Helper_Abstract
{

    public function chAdd()
    {
        if ($this->view->chAddOn) {
            $front = Zend_Controller_Front::getInstance();
            $request = $front->getRequest();
            $controller = $request->getParam('controller');   
            
            if ($this->view->chAddType) {
                return '<a href="' . $this->view->url(array('controller' => $controller, 'action' => 'add', 'type' => $this->view->chAddType), 'add_page_type') . '" id="add">Добавить</a>';
            }

            if ($this->view->chAddOnGalleryimage) {
                return '<a href="' . $this->view->url(array('controller' => $controller, 'action' => 'add', 'galleryid' => $this->view->chAddOnGalleryimage), 'add') . '" id="add">Добавить</a>';
            }
            return '<a href="' . $this->view->url(array('controller' => $controller, 'action' => 'add'), 'add') . '" id="add">Добавить</a>';
        }
    }

}

?>
