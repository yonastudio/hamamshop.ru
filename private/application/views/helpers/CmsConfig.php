<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_CmsConfig extends Zend_View_Helper_Abstract
{
    
    private static $_cmsconfig = null;
    
    private function _instance()
    {
        if (!self::$_cmsconfig) {
            self::$_cmsconfig = Zend_Registry::get('cmsconfig');
        }
    }
    
    public function __construct()
    {
        self::_instance();
    }

    public function cmsConfig($key)
    {
        self::_instance();
        if (array_key_exists($key ,self::$_cmsconfig)) {
            return self::$_cmsconfig[$key];
        }
        
    }

}
