<?php
/**
 * Created by JetBrains PhpStorm.
 * User: webtorua
 * Date: 21.11.11
 * Time: 23:47
 * To change this template use File | Settings | File Templates.
 */
 
class Zend_View_Helper_ShopHomepageRandomProducts extends Zend_View_Helper_Abstract
{
    public function shopHomepageRandomProducts($limit = 5)
    {

        $prefix = Zend_Registry::get('prefix');
        $q = Doctrine_Query::create()
                ->select('sm.*,'
                . 'c.name AS c_name,'
                . 'c.title' . $prefix . ' AS c_title,'
                . 'sb.name AS sb_name,'
                . 'sb.title AS sb_title'
                )
                ->from('Default_Model_ShopModel sm')
                ->innerJoin('sm.Category c')
                ->innerJoin('sm.ShopBrand sb')
                ->innerJoin('sm.ShopModifications smd')
                ->where('smd.status = 1')
                ->andWhere('c.id <> 19')
                ->orderBy('RAND(NOW())')
                ->limit($limit);
        
        $rows = $q->fetchArray();
        $html = '';
        if (count($rows) > 0) {
            $html = $this->view->partial('shop/shop-homepage-random-products.phtml',array(
                'entries' => $rows
            ));
        }

        return $html;
    }
}
