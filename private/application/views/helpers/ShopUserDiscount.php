<?php
 /**
 * Description of ShopUserDiscount
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Zend_View_Helper_ShopUserDiscount extends Zend_View_Helper_Abstract
{

    public function shopUserDiscount($sumprice, $onlyvalue = false)
    {
        $storage = Zend_Auth::getInstance()->getStorage()->read();
        if ($storage && !$storage->access_level) {
            $user = new Default_Model_User();
            $data = $user->findOneWithDiscount($storage->id);
            $this->view->discount_title = $data['discount_title'];
        }
        $str = '';
        if ($data['discount_val']) {
            $sumprice = $sumprice - $sumprice * $data['discount_val'] / 100;
            $str .= '<span class="marked">'.$this->view->translate('discount').' -' . $data['discount_title'] . '</span><br />';
        } elseif ($sumprice >= DISCOUNT_BONUS_SUM) {
            /*$sumprice = $sumprice - $sumprice * DISCOUNT_BONUS_VAL / 100;
            $str .= '<span class="marked">'.$this->view->translate('discount').' -' . DISCOUNT_BONUS_VAL . '%</span><br />';*/
            return $sumprice;
        }
        $str .= number_format($sumprice, 2, '.', '');
        if ($onlyvalue) {
            return number_format($sumprice, 2, '.', '');
        }
        return $str;
    }

}
