<?php

/**
 * Description of GetPage
 *
 * @author веб-студия "WeZoom" <web@wezoom.net> http://wezoom.net
 * @author ведущий разрабочик Торош Александр <webtorua@gmail.com>
 */
class Zend_View_Helper_GetNew extends Zend_View_Helper_Abstract
{

    public function getNew($id)
    {
        $articles = new Default_Model_News();
        
        $data = $articles->getNewById($id);                
                
        if (!$data) {
            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $redirector->gotoUrl('/');
        }
        
        $prefix = '';
        if (Zend_Registry::isRegistered('prefix')) {
            $prefix = Zend_Registry::get('prefix');
        }

        if ($data['text' . $prefix])
            $this->view->content = $data['text' . $prefix];
        if ($data['title' . $prefix]) {
            $this->view->headTitle($data['title' . $prefix]);
            $this->view->title = $data['title' . $prefix];
            $this->view->headMeta()->setName('description', $data['title' . $prefix]);
            $this->view->headMeta()->setName('keywords', $data['title' . $prefix]);
        }
        if ($data['created_at'])
            $this->view->date = $data['created_at'];
    }

}