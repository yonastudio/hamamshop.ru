<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_TopMenu extends Zend_View_Helper_Abstract
{
    public function topMenu()
    {
        $container = $this->view->topMenu;
        $menu = $this->view->navigation()->menu($container);
        $menu->setUlClass('pills');

		$articles = $menu->findOneBy('title', 'Статьи');
		if ($articles) {
			if (Zend_Registry::get('lang') == 'en') {
				$menu->removePage($articles);
			}	
		}

        return $menu->render();
    }
}
