<?php
/**
 * Created by JetBrains PhpStorm.
 * User: webtorua
 * Date: 19.11.11
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
 
class Zend_View_Helper_WidgetNews extends Zend_View_Helper_Abstract
{
    public function widgetNews()
    {
        $cacher = Zend_Controller_Action_HelperBroker::getStaticHelper('Cacher');
        $data = $cacher->get('widget_news');
        if (!$data) {
            $news = new Default_Model_News();
            $data = $news->all(2, Zend_Registry::get('prefix'));
            $cacher->set($data,'widget_news',300);
        }
        $html = $this->view->partial('widget/news.phtml',array('entries' => $data));
        return $html;
    }
}
