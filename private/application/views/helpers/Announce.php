<?php

/**
 * Description of Announce
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_Announce extends Zend_View_Helper_Abstract
{

    public function announce($text, $number)
    {
        $text = strip_tags($text);
        if (!$text) {
            return;
        }
        $text = mb_substr($text, 0, $number, 'utf-8');
        $array_str = explode('.', $text);
        if (count($array_str) >= 2) {
            $array_str[count($array_str) - 1] = '...';
            $output = implode('.', $array_str);
        } else {
            $array_str = explode(' ', $text);
            $array_str[count($array_str) - 1] = '...';
            $output = implode(' ', $array_str);
        }
        return $output;
    }

}
