<?php

/**
 * Description of Category
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_ShopFilterParams extends Zend_View_Helper_Abstract
{

    public function shopFilterParams($params)
    {
        if (Zend_Registry::isRegistered('filter_params_array')) {
            $currents = Zend_Registry::get('filter_params_array');
        } else {
            return;
        }

        $new = array();

        if (count($currents) > 0) {
            foreach ($currents as $curr_el => $curr_val) {
                $new[$curr_el] = $curr_val;
                if (count($params) > 0) {
                    foreach ($params as $param => $param_key) {
                        $new[$param] = $param_key;
                    }
                }
            }
        } else {
            if (count($params) > 0) {
                foreach ($params as $param => $param_key) {
                    $new[$param] = $param_key;
                }
            }
        }
        return $this->encodeParams($new);
    }

    private function encodeParams($arr)
    {
        if (count($arr) > 0) {
            $str = '';
            foreach ($arr as $el => $val) {
                //Page определяется в роутере. Он в параметрах не нужен.
                if ($el != 'page') {
                    $str .= '&' . $el . '=' . $val;
                }
            }
            $str = substr($str, 1);
            return $str;
        }
    }

}
