<?php
/**
 * Description of MinifiedCss
 * @copyright  Copyright (c) 2011 Aleksandr Torosh (http://wezoom.net)
 * @author Торош Александр <webtorua@gmail.com>
 */

class Zend_View_Helper_Compress extends Zend_View_Helper_Abstract
{

    private $valid_extensions = array('js', 'css');
    private $file;
    private $fullFilePath;
    private $cachePath;
    private $cacheFile;
    private $extension;

    public function __construct($file = null)
    {
        if (!$file) {
            return;
        }
        $this->file = $file;
        $this->fullFilePath = DOCUMENT_ROOT . '/' . $file;
        $this->extension = $this->getExtension();
        if (!in_array($this->extension, $this->valid_extensions)) {
            return;
        }
        $this->setCachePath();
    }

    public function compress($file)
    {
        $this->__construct($file);

        $hash = $this->getHash();
        $this->cacheFile = $this->cachePath . $hash . '.' . $this->extension;
        if (!file_exists(DOCUMENT_ROOT . $this->cacheFile)) {
            $this->clean();
            $minimized = $this->getMinimized();
            $this->saveCache($minimized);
        }
        return $this->cacheFile;
    }

    private function getMinimized()
    {
        switch ($this->extension) {
            case 'js' :
                require_once 'JSMin.php';
                return JSMin::minify(file_get_contents($this->fullFilePath));
            case 'css' :
                require_once 'CssMin.php';
                return CssMin::minify(file_get_contents($this->fullFilePath));
            default :
                return;
        }
    }

    private function saveCache($data)
    {
        $f = fopen(DOCUMENT_ROOT . $this->cacheFile, 'w+');
        fputs($f, $data);
    }

    private function getExtension()
    {
        if ($this->file) {
            $exts = explode(".", $this->file);
            $n = count($exts) - 1;
            $exts = $exts[$n];
            return $exts;
        }
    }

    private function setCachePath()
    {
        $filepath = substr(md5($this->file),0,8);
        $this->cachePath = '/compressed/' . $filepath . '/';
        if (!is_dir(DOCUMENT_ROOT . $this->cachePath)) {
            mkdir(DOCUMENT_ROOT . $this->cachePath);
        }
    }

    private function lastModified()
    {
        if (file_exists($this->fullFilePath)) {
            return filemtime($this->fullFilePath);
        }
    }

    private function getHash()
    {
        return substr(md5(SECRET . $this->lastModified()), 5, 8);
    }

    private function clean()
    {
        $files = glob(DOCUMENT_ROOT . $this->cachePath . '*.' . $this->extension);
        if (count($files) > 0) {
            foreach ($files as $file) {
                unlink($file);
            }
        }
    }

}
