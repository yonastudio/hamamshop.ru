<?php

/**
 * Description of TopMenu
 *
 * @author Торош Олександр <webtorua@gmail.com>
 */
class Zend_View_Helper_LeftMenu extends Zend_View_Helper_Abstract
{
    public function leftMenu()
    {
        $container = $this->view->leftMenu;
        $menu = $this->view->navigation()->menu($container);
        $menu->setUlClass('left-menu');
        return $menu->render();
    }
}
