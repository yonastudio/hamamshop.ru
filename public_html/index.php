<?php

error_reporting(E_ALL ^E_NOTICE ^E_WARNING);

if (strpos($_SERVER['REQUEST_URI'], 'index.php')) {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: http://' . $_SERVER['HTTP_HOST'] . str_replace(array('index.php/', 'index.php'), array('', ''), str_replace('?', '', $_SERVER['REQUEST_URI'])));
    exit;
}

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../private/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

/**Project defines*/
require_once APPLICATION_PATH . '/configs/defines.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap()
            ->run();