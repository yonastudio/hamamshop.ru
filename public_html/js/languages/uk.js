translate = {
    UAH : "грн.",
    user_shopping_cart : "Кошик покупця",
    product_was_added_to_shopping_cart : "Товар додано у кошик покупця",
    added_to_cart : "У кошику",
    favorites : "Выбране",
    wrong_value : "Невірне значення",
    are_you_really_want_delete_from_cart : "Ви дійсьно бажаєте видалите цей товар з кошика?",
    are_you_really_want_delete_from_favorites : "Ви дійсьно бажаєте видалите цей товар з Вибраного?",
    passwords_not_match : "Паролі не співпадають",
    password_is_to_short : "Пароль надто короткий. Мінімальна довжина - 3 символа"
}