translate = {
    UAH : "UAH",
    user_shopping_cart : "Shoping Cart",
    product_was_added_to_shopping_cart : "Product added to Shoping Cart",
    added_to_cart : "In Cart",
    favorites : "Favorites",
    wrong_value : "Wrong value",
    are_you_really_want_delete_from_cart : "Are you sure you want to remove this item from the Cart?",
    are_you_really_want_delete_from_favorites : "Are you sure you want to remove this item from the Favorites?",
    passwords_not_match : "Passwords not match",
    password_is_to_short : "Password is to short. Make it more than 3 chars"
}