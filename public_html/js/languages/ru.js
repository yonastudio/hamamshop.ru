translate = {
    UAH : "грн.",
    user_shopping_cart : "Корзина покупателя",
    product_was_added_to_shopping_cart : "Товар добавлен в корзину покупателя",
    added_to_cart : "В корзине",
    favorites : "Избранное",
    wrong_value : "Неверное значение",
    are_you_really_want_delete_from_cart : "Вы действительно хотите удалить этот товар из корзины?",
    are_you_really_want_delete_from_favorites : "Вы действительно хотите удалить этот товар с Избранного?",
    passwords_not_match : "Пароли не совпадают",
    password_is_to_short : "Пароль слишком короткий. Минимальная длинна - 3 символа"
}