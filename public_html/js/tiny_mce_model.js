$(function() {
    $('#description, #description_uk, #description_en').tinymce({

        script_url : '/lib/tiny_mce/tiny_mce_gzip.php',

        theme : "advanced",
        width : "100%",
        height: '300',
        language: "ru",

        plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        
        relative_urls : false,
        convert_urls : true,
        dialog_type : "modal",
        
        theme_advanced_blockformats : "p,h1,h2,h3,h4",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        element_format : "xhtml",
        content_css : "/css/tinymce.css",

        paste_auto_cleanup_on_paste : true,
        paste_text_sticky : true,
        
    });
    
});


