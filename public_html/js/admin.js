Admin = {};

/**
 * Удаление. Универсальный deleter
 * @param controller
 * @param id
 */
Admin.deleteitem = function(controller, id) {
    if (controller && id) {
        $.ajax({
            url: "/" + controller + "/delete",
            data: ({
                id: id
            }),
            type: "POST",
            dataType: 'html',
            success: function(data) {
                if (data && data != 1) {
                    alert(data);
                }
                window.location.href = "/" + Admin.controller + "/list";
            }
        });
    }
}

var Category = {};

/**
 * Запрос дерева категорий
 * @param id
 * @param level
 */
Category.xhrprocess = function(id, level) {
    $.ajax({
        url: '/categories/xhrprocess',
        data: ({
            id: id
        }),
        type: "POST",
        dataType: 'html',
        success: function (data) {
            if (data) {

                if (id && level) {
                    for (var i = 10; i > level; i--) {
                        $('#category_panel_field ul[level$="' + i + '"]').remove();
                    }
                    var html = $("#category_panel_field").html();
                    $("#category_panel_field").html(html + data);
                } else {

                    $("#category_panel_field").html(data);
                }
            }
        }
    });
}

$(function() {

    Admin.controller = $("#controller").val();
    Admin.id = $("#id").val();
    /**
     * Закрытие модального диалога
     */
    $('.close').live('click', function() {
        $(".dialog").animate({
            opacity: "hide"
        }, "fast");
    })

    /**
     * Submit form в .content-head
     */
    $("#submit").click(function() {
        $(this).css('color', '#999');
        $(this).text('Сохранено');
        $('#form').submit();
    });

    /**
     * Универсальный deleter в .content-head
     */
    $("#delete").live('click', function() {
        var name = prompt('Для удаления записи, введите "delete"');
        if (name == 'delete') {
            var result = Admin.deleteitem(Admin.controller, Admin.id);
        }
    });

    /**
     * Универсальный deleter в таблицах
     */
    $(".delete").live('click', function() {
        var id = $(this).attr('id');
        if (confirm('Удалить запить? ID=' + id)) {
            Admin.deleteitem(Admin.controller, id);
            $(this).parent('tr').animate({
                opacity: "hide"
            }, "slow");
        }
    });

    /**
     * Диалог выбора категорий
     */
    $("#link").live('click', function() {
        var id = $(this).attr('category');
        $(function() {
            $("#dialog").dialog({
                height: 500,
                width: 770
            });
        });
        $("#category_panel").show();
        Category.xhrprocess(id);
    });

    /**
     * Панель категорий
     */
    $("#category_panel_field li").live('click', function() {
        var haschildren = $(this).attr('haschildren');
        var level = $(this).parent('ul').attr('level');
        var id = $(this).attr('id');
        if (haschildren == 1) {
            Category.xhrprocess(id, level);
        } else {
            $('#category_panel_field ul[level$="0"]').remove();
            $("#category_panel").hide();
            $("#category_id").val(id);
            var title = $(this).html();
            $("#category_title").html(title);
            $("#category_panel_field").html('');
            $(function() {
                $("#dialog").dialog('close');
            });

        }
    });

    /**
     * Загрузчик единичных изображений
     */
    $('#fileupload').fileupload({
        dataType: 'json',
        url: $('#fileupload').attr('act'),
        done: function (e, data) {
            if (data.result.error) {
                alert(data.result.error);
            } else {
                $('#image').html(data.result.image);
            }
        }
    });

    /**
     * Загрузчик множественных изображений
     * @todo Нужен рефакторинг в CatalogItemsController::editAction(), заменить на #fileupload_multi
     */
    $('#fileupload_prtf').fileupload({
        dataType: 'json',
        url: $('#fileupload_prtf').attr('act'),
        done: function (e, data) {
            var image = data.result.image;
            var id = data.result.id;
            var html = '<li class=\"gallery_image_item\" id=\"' + id + '\">' +
                image + '<br/>' +
                '</li>';
            $(".gallery_image_list").append(html);
        }
    });

    /**
     * Загрузчик множественных изображений
     */
    $('#fileupload_multi').fileupload({
        dataType: 'json',
        url: $('#fileupload_multi').attr('act'),
        done: function (e, data) {
            var image = data.result.image;
            var id = data.result.id;
            var html = '<li class=\"gallery_image_item\" id=\"' + id + '\">' +
                image + '<br/>' +
                '</li>';
            $(".gallery_image_list").append(html);
        }
    });

    /**
     * Удаление изображения из галереи
     */
    $(".delete-screen").click(function() {
        if (confirm("Удалить изображение?")) {
            var url = $(this).attr('act');
            var id = $(this).attr('id');
            $.ajax({url: url, data: ({id: id}), type: "POST", dataType: 'html',
                success: function(data) {
                    if (data) {
                        location.reload(true);
                    }
                }
            });
        }
    });

    /**
     * Изменяет порядок изображений
     */
    /*$(".gallery_image_item, .gallery_colors_item").draggable({
        drag: function(event, ui) {
            $(this).css('opacity', '0.7').css('z-index', '15000')
        },
        stop: function(event, ui) {
            if (!ui.droppable) {
                location.reload(true);
            }
        }
    });
    $(".gallery_image_item, .gallery_colors_item").droppable({
        drop: function(event, ui) {
            var first = $(this).attr('id');
            var second = ui.draggable.attr('id');
            var item_id = $("#id").val();
            var url = $(this).parent('ul').attr('act');
            $.ajax({
                url: url,
                data: ({
                    first: first,
                    second: second,
                    item_id: item_id
                }),
                type: "POST",
                success: function() {
                    setTimeout(location.reload(true), 200);
                }
            });
        }
    });*/

    /**
     * Инициализация сортировщика таблиц
     */
     $("table#sortable").tablesorter();

    /**
     * Окно добавления данных без модификаций
     */
    $("#modif-add-one").click(function() {
        var model_id = $("#id").val();
        $.post('/shop-modifications', {type:'one',model_id:model_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
    });

    /**
     * Окно добавления данных модификации
     */
    $("#modif-add-many").click(function() {
        var model_id = $("#id").val();
        $.post('/shop-modifications', {type:'many',model_id:model_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
    });

    /**
     * Окно редактирования данных модификации
     */
    $(".modif-edit").click(function(){
        var modif_id = $(this).attr('modif-id');
        $.post('/shop-modifications/edit', {modif_id:modif_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
        return false;
    });

    /**
     * Окно добавления цвета
     */
    $("#color-add").live('click',function(){
        var model_id = $("#id").val();
        $.post('/shop-colors', {model_id:model_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
    });

    /**
     * Закрытие модального окна. Очистка контейнера.
     */
    $('#modal .close').live('click', function () {
        $("#modal").html('<div class="modal-body"><img src="/images/admin/ajaxLoader.gif" /></div>');
    });

    /**
     * Сохранение данных модального окна
     */
    $('#modal-save').live('click', function() {
        var formData = $("#form-modal").serialize();
        var act = $("#modal .modal-header").attr('act');
        $.post(act, {formData:formData}, function(data) {
            if (data.error) {
                alert(data.error);
            }
            if (data.saved) {
                location.reload(true);
            }
        }, 'json');
        return false;
    });

    /**
     * Добавление/редактирование цвета к модификации
     */
    $(".modif-colors-edit").live('click', function(){
        var model_id = $("#id").val();
        var modif_id = $(this).attr('modif-id');
        $.post('/shop-colors/modifs', {model_id:model_id, modif_id:modif_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
        return false;
    });

});

/**
     * Добавление/редактирование цвета к фото
     */
    function imageColorEdit(image_id)
    {
        $.post('/shop-colors/images', {image_id:image_id}, function(data) {
            if (data.html) {
                $("#modal").html(data.html);
                $("#modal").modal('show');
            }
        }, 'json');
    }