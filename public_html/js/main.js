$(function () {

  $(document).pngFix();

  var height = $(window).attr('innerHeight');
  var width = $(window).attr('innerWidth');

  var nheight = width / 1.415 + 30;

  if (nheight > height) {
    $("body").css('min-height', height);
  }

  var lang = $("#lang").val();
  var currency = $("#currency").val();

  $(".spoiler a").click(function () {
    $('#spoiler').slideToggle(200);
    return false;
  });

  $(".shop-menu li").each(function () {
    if ($(this).hasClass('active')) {
      $(this).children('a').addClass('actived');
    }
  });

  var modal_html = $('#modal').html();
  var cart_modal_html = $('#cart-modal').html();
  var modal_loader = $('#modal .modal-body').html();

  /**
   * Menu
   */
  $("#top-menu_button").click(function () {
    $(this).toggleClass('active');
    $('.top-menu').toggleClass('active');
  });

  $("#top-menu_button").hover(function () {
    $(this).addClass('hover');
  }, function () {
    $(this).removeClass('hover');
  });

  /**
   * Поиск
   */
  $("#search > .search-submit").click(function () {
    $("#search > .search-input").toggle();
  });

  /**
   * Добавить в корзину
   */
  $(".add-to-cart").live('click', function () {
    ga('send', 'event', 'Cart', 'Add');

    if (typeof yaCounter26829522 !== 'undefined') {
      yaCounter26829522.reachGoal('ADD_TO_CART');
    }

    scrollTop();

    var modif_id = $(this).attr('modif-id');
    var count = $(this).prev('.cart-count').val();
    var color_id = $(this).parent('td').prev('td').prev('td').children('select').children('option:selected').val();
    if (parseInt(count) <= 0) {
      alert(translate.wrong_value);
      $(this).prev('.cart-count').val('1');
      return false;
    }
    var price = parseFloat($(this).parent('td').prev('td').prev('td').prev('td').html());
    $.post('/shop-order/add-to-cart', {
      modif_id: modif_id,
      count: count,
      price: price,
      color_id: color_id
    }, function (data) {
      if (data.error) {
        alert(data.error);
        $('#cart-modal').modal('hide');
      }
      if (data.success) {
        $.get('/' + lang + '/shop-order', function (data) {
          if (data) {
            $("#cart-modal").html(data);
          }
        });

        $("#shop-cart-count").html(data.count);

        //$("#shop-cart-count").html(data.count).animate({color: "lime"}, 350).animate({color: "black"}, 350);
        //$("#shop-cart-sum").animate().html(data.sum).animate({color: "lime"}, 350).animate({color: "black"}, 350);
      }
    }, 'json');
    return false;
  });

  /**
   * Обновление данных в корзине покупателя в шапке сайта
   */
  function cartHeadRefresh() {
    $.get('/shop-order/refresh-cart', function (data) {
      if (data.success) {
        $("#shop-cart-count").html(data.count);
        //$("#shop-cart-count").html(data.count).animate({color: "lime"}, 350).animate({color: "black"}, 350);
        //$("#shop-cart-sum").animate().html(data.sum).animate({color: "lime"}, 350).animate({color: "black"}, 350);
      }
    }, 'json');
  }

  /**
   * Добавить в избранное в таблице модификаций товара
   */
  $(".add-to-fav").live('click', function () {
    scrollTop();
    var modif_id = $(this).attr('modif-id');
    var color_id = $(this).parent('td').prev('td').prev('td').children('select').children('option:selected').val();
    $.post('/shop-favorites/add', {modif_id: modif_id, color_id: color_id}, function (data) {
      if (data.error) {
        alert(data.error);
        $('#cart-modal').modal('hide');
      }
      if (data.needauth) {
        $('#cart-modal').modal('hide');
        $("#favorites").click();
      }
      if (data.success) {
        $.get('/' + lang + '/shop-favorites', function (data) {
          if (data) {
            $("#cart-modal").html(data);
          }
        });
      }
    }, 'json');
    return false;
  });

  /**
   * Добавление товара в корзину с избранного
   */
  $(".add-to-cart_from-fav").live('click', function () {
    scrollTop();
    var modif_id = $(this).attr('modif-id');
    var color_id = $(this).attr('color-id');
    $("#cart-modal").html(cart_modal_html);
    $.post('/shop-order/add-to-cart', {modif_id: modif_id, count: 1, color_id: color_id}, function (data) {
      if (data) {
        $("#cart-modal").modal('hide');
        $("#shop-cart").click();
      }
    });
  });

  /**
   * Закрытие модального окна
   */
  $('#modal .close').live('click', function () {
    $("#modal").html(modal_html);
    //$("#cart-modal").html(modal_html);
  });
  $('#cart-modal .close').live('click', function () {
    $("#cart-modal").html(modal_html);
  });

  $().piroBox({
    my_speed: 300, //animation speed
    bg_alpha: 0.5, //background opacity
    slideShow: 'true', // true == slideshow on, false == slideshow off
    slideSpeed: 3, //slideshow
    close_all: '.piro_close' // add class .piro_overlay(with comma)if you want overlay click close piroBox
  });

  /**
   * Корзина покупателя
   */
  $("#shop-cart, #cart-back").live('click', function () {
    $("#cart-modal .modal-body").html(modal_loader);
    $.get('/' + lang + '/shop-order', function (data) {
      if (data) {
        $("#cart-modal").html(data);
      }
    });
    return false;
  });

  /**
   * "Пересчитать" в корзине
   */
  $("#cart-refresh").live('click', function () {
    scrollTop();
    var counts = '';
    $(".cart-count-value").each(function () {
      if (parseInt($(this).val()) < 0) {
        alert(translate.wrong_value);
        return false;
      }
      counts += '&' + $(this).attr('cart-id') + '=' + $(this).val();
    });
    $("#cart-modal .modal-body").html(modal_loader);
    counts = counts.substr(1);
    $.post('/' + lang + '/shop-order', {counts: counts}, function (data) {
      if (data) {
        cartHeadRefresh();
        $("#cart-modal").html(data);
      }
    });
    return false;
  });

  /**
   * Кнопка "Оформить заказ в корзине".
   */
  $("#cart-order").live('click', function () {
    scrollTop();
    ga('send', 'event', 'Cart', 'Order');

    if (typeof yaCounter26829522 !== 'undefined') {
      yaCounter26829522.reachGoal('OPEN_ORDER_FROM_CART');
    }

    $("#cart-modal .modal-body").html(modal_loader);
    $.get('/' + lang + '/shop-order/order', function (data) {
      if (data) {
        $("#cart-modal").html(data);
      }
    });
    return false;
  });

  /**
   * Подтерждение введеных даных при оформлении заказа
   */
  $("#cart-submit").live('click', function () {
    var payment = $("#payment option:selected").val();
    var formData = $("#form-checkout").serialize();
    $("#cart-modal #form-checkout-container").html(modal_loader);

    $.post('/' + lang + "/shop-order/order-submit", {formData: formData}, function (data) {
      if (data) {

        toastr.clear();

        if (data.errors) {

          for(var element in data.errors) {
            var errorMessage = data.errors[element];

            if (errorMessage) {
              $("#form-checkout #" + element).addClass('error');
              toastr.error(element + ': ' + errorMessage);

            } else {
              $("#form-checkout #" + element).removeClass('error');
            }
          }

        }

        if (data.error) {
          toastr.error(data.error);

        }

        if (data.success) {
          if (data.redirect) {
            window.location.href = data.redirect;
          }
        }

      }
    }, 'json');

    return false;
  });


  /**
   * Удаление товара из корзины
   */
  $(".cart-delete").live('click', function () {
    var id = $(this).attr('cart-id');
    var title_elem = $(this).parent('td').prev('td').prev('td').prev('td');
    var title = title_elem.children('span.title').html();
    var count = $(this).parent('td').prev('td').prev('td').children('input').val();
    if (confirm(translate.are_you_really_want_delete_from_cart + '\n ' + title)) {
      $("#cart-modal").html(cart_modal_html);
      $.post('/' + lang + '/shop-order', {
        del: '1',
        id: id
      }, function (data) {
        if (data) {
          cartHeadRefresh();
          $("#cart-modal").html(data);
        }
      });
    }
    return false;
  });


  $(".favorites-delete").live('click', function () {
    var id = $(this).attr('cart-id');
    if (confirm(translate.are_you_really_want_delete_from_favorites)) {
      $("#cart-modal").html(cart_modal_html);
      $.post('/' + lang + '/shop-favorites', {
        del: '1',
        id: id
      }, function (data) {
        if (data) {
          $("#cart-modal").html(data);
        }
      });
    }
    return false;
  });

  /**
   * Вход для пользователей сайта
   */
  $("#user-auth-login").click(function () {
    $.get('/' + lang + '/users/login', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
  });

  /**
   * Забыли пароль?
   */
  $("#user-lost-password").live('click', function () {
    $("#modal").html(modal_html);
    $.get('/' + lang + '/users/lost-password', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
    return false;
  });

  /**
   * Авторизация при оформлении заказа в корзине
   */
  $("#cart-login").live('click', function () {
    $("#modal").html(modal_html);
    $.get('/' + lang + '/users/login?iscart=1', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
    return false;
  });

  /**
   * Авторизация. Кнопка Логин с любого модального окна.
   */
  $("#login").live('click', function () {
    $("#modal").html(modal_html);
    $.get('/' + lang + '/users/login', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
    return false;
  });

  /**
   * Регистрация нового пользователя
   */
  $("#user-auth-registration").click(function () {
    $.get('/' + lang + '/users/registration', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
  });

  /**
   * Личный кабинет
   */
  $("#user-auth-title").click(function () {
    $.get('/' + lang + '/users/profile', function (data) {
      if (data) {
        $("#modal").html(data);
      }
    });
  });

  /**
   * Избранное
   */
  $("#favorites").live('click', function () {
    $("#cart_modal").html(cart_modal_html);
    $.get('/' + lang + '/shop-favorites', function (data) {
      if (data) {
        $("#cart-modal").html(data);
      }
    });
    return false;
  });

  /**
   * Смена типа оплаты при оформлении заказа
   */
  $("#delivery").live('change', function () {
    var sel = $(this).children('option:selected').val();
    switch (sel) {
      case '1' :
        $("#adress").attr('disabled', 'disabled');
        break;
      case '2' :
        $("#adress").removeAttr('disabled');
        break;
    }
  });

  $('#form-modal input').live('keypress', function (e) {
    if (e.which == 13) {
      $(this).bind();
      $('#modal-save').focus().click();
    }
  });

  $("#user-orders-list").live('click', function () {
    $("#cart_modal").html(cart_modal_html);
    $.get('/' + lang + '/users/orders', function (data) {
      if (data) {
        $("#cart-modal").html(data);
      }
    });
  });

  /**
   * Отправка формы из модального окна. Проверки данных, состояний.
   */
  $('#modal-save').live('click', function () {

    //Пароль при регистрации
    var p1 = $("#password_1").val();
    var p2 = $("#password_2").val();
    if (p1 && p2) {
      p1 = p1.toString();
      p2 = p2.toString();
      if (p1 != p2) {
        alert(translate.passwords_not_match);
        return false;
      } else {
        if (p1.length < 3) {
          alert(translate.password_is_to_short);
          return false;
        }
      }
    }

    var formData = $("#form-modal").serialize();
    var act = $("#modal .modal-header").attr('act');
    var iscart = $("#iscart").html();
    if (iscart == 'iscart') {
      act += '?iscart=1';
    }

    $("#modal").html(modal_html);
    $("#modal .modal-footer").remove();
    $("#modal .modal-body").html(modal_loader);

    $.post(act, {formData: formData}, function (data) {
      if (data) {

        //Проверка на перезагрузку страницы
        var pattern = new RegExp('<div class=\"hidden\">success</div>');
        if (data.match(pattern)) {

          //Проверка на авторизацию из корзины
          var pattern_iscart = new RegExp('<div id=\"iscart\" class=\"hidden\">iscart</div>');
          if (data.match(pattern_iscart)) {

            $('#modal').modal('hide');
            $("#cart-modal .modal-body").html(modal_loader);
            $.get('/' + lang + '/shop-order/order', function (data) {
              if (data) {
                $("#cart-modal").html(data);
              }
            });
            return;
          }

          location.reload(true);
          return;
        }

        $("#modal").html(data);
      }
    });
    return false;
  });

  /*Выбор сортировки*/
  $("#shop-sort-filter select").change(function () {
    var href = $("#shop-sort-filter select option:selected").val();
    location.href = href;
  });

  /**
   * Галерея внутри товара
   */
  $('#product-gallery-small > li > a').click(function () {
    $('#product-gallery-small > li').removeClass('active');
    $(this).parent().addClass('active');

    var image_id = $(this).data('image');
    $('#product-gallery > li').hide();
    $('#product-gallery > li[data-image="' + image_id + '"]').show();
  });


  // HomepageSlider init
  var homepageSlider = new HomepageSlider();
  homepageSlider.init();

  $('#slider-nav > .circle').click(function () {
    var position = $(this).data('position');
    console.log(position);
    homepageSlider.pickPosition(position);
  });

});

var HomepageSlider = function () {
  var self = this;

  self.position = 0;
  self.length = 3;
  self.interval = null;

  self.init = function() {
    self.interval = setInterval(function() { self.nextPosition() }, 10000);
  };

  self.nextPosition = function () {
    if (self.position === (self.length - 1)) {
      self.position = 0;
    } else {
      self.position = self.position + 1;
    }
    console.log('net position:', self.position);
    self.switchPositon(self.position);
  };

  self.prevPosition = function () {
    if (self.position === 0) {
      self.position = (self.length - 1);
    } else {
      self.position = self.position - 1;
    }
    self.switchPositon(self.position);
  };

  self.switchPositon = function (position) {
    $('#slider-nav > .circle').removeClass('active');
    $('#slider-nav > .circle[data-position="' + position + '"]').addClass('active');

    $('#slider-home > .wrap > .item').removeClass('active');
    $('#slider-home > .wrap > .item[data-position="' + position + '"]').addClass('active');

    $('#slider-product > .product').removeClass('active');
    $('#slider-product > .product[data-position="' + position + '"]').addClass('active');
  };

  self.pickPosition = function(position) {
    clearInterval(self.interval);
    self.switchPositon(position);
  };

};

/**
 * Валидация формы обратной связи
 */
function checkCallbackForm() {
  var name = $("#name").val();
  var email = $("#email").val();
  var phone = $("#phone").val();
  var message = $("#message").val();
  var error = false;

  $(".help-inline").remove();
  $(".clearfix").removeClass('error');

  if (name.length < 1) {
    $("#name").parent('div').parent('.clearfix').addClass('error');
    $("#name").parent('div').append('<span class="help-inline">Введите имя</span>');
    error = true;
  }

  if (!validateEmail(email)) {
    $("#email").parent('div').parent('.clearfix').addClass('error');
    $("#email").parent('div').append('<span class="help-inline">Введите правильный email</span>');
    error = true;
  }

  if (phone.length > 100) {
    $("#phone").parent('div').parent('.clearfix').addClass('error');
    $("#phone").parent('div').append('<span class="help-inline">Слишком длинная строка</span>');
    error = true;
  }

  if (message.length > 2000) {
    $("#message").parent('div').parent('.clearfix').addClass('error');
    $("#message").parent('div').append('<span class="help-inline">Слишком длинная строка</span>');
    error = true;
  }

  if (message.length < 1) {
    $("#message").parent('div').parent('.clearfix').addClass('error');
    $("#message").parent('div').append('<span class="help-inline">Введите сообщение</span>');
    error = true;
  }

  if (error) {
    return false;
  } else {
    return true;
  }

}

function scrollTop() {

  var body = $("html, body");
  body.animate({scrollTop: 0}, '200', 'swing');

}

/**
 * Валидация Email
 * @param email
 */
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
